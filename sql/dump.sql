-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: forge
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `achievements`
--

DROP TABLE IF EXISTS `achievements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_uk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achievements`
--

LOCK TABLES `achievements` WRITE;
/*!40000 ALTER TABLE `achievements` DISABLE KEYS */;
INSERT INTO `achievements` VALUES (1,'Більше десяти років досвіду команди лікарів','','','10','0000-00-00 00:00:00','2016-01-16 15:41:03',NULL),(2,'Більше ста професійних послуг','','','100','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL),(3,'Більше однієї тисяч задоволених клієнтів','','','1000','0000-00-00 00:00:00','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `achievements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_groups`
--

DROP TABLE IF EXISTS `acl_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_groups`
--

LOCK TABLES `acl_groups` WRITE;
/*!40000 ALTER TABLE `acl_groups` DISABLE KEYS */;
INSERT INTO `acl_groups` VALUES (1,'Администратор','{\"backend.*\":1}','2016-01-08 15:19:59','2016-01-08 15:19:59',NULL);
/*!40000 ALTER TABLE `acl_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_groups_users`
--

DROP TABLE IF EXISTS `acl_groups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_groups_users` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `acl_groups_users_group_id_foreign` (`group_id`),
  CONSTRAINT `acl_groups_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `acl_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `acl_groups_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `acl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_groups_users`
--

LOCK TABLES `acl_groups_users` WRITE;
/*!40000 ALTER TABLE `acl_groups_users` DISABLE KEYS */;
INSERT INTO `acl_groups_users` VALUES (1,1);
/*!40000 ALTER TABLE `acl_groups_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_users`
--

DROP TABLE IF EXISTS `acl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acl_users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_users`
--

LOCK TABLES `acl_users` WRITE;
/*!40000 ALTER TABLE `acl_users` DISABLE KEYS */;
INSERT INTO `acl_users` VALUES (1,'Super User','admin@domain.com','$2y$10$4vvfrVgotjhe4dOt3PeYWOSTpP9N27ar3TjnA1iwdyesrBhrqT9g6','B55M34iCGXHOqCoqZmu39WMrUbrMvSKsN9yTchRqg6XT1mXBXLSxufUntXIE','[]',1,'2016-01-08 15:19:59','2016-03-12 14:02:08',NULL);
/*!40000 ALTER TABLE `acl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `media_id` int(10) unsigned DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `media_id` (`media_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,21,''),(2,22,'http://zaycevstudio.com/');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (7,'kotedzhi',1,0,'2016-03-12 14:15:05','2016-03-12 14:15:05');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_texts`
--

DROP TABLE IF EXISTS `categories_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_texts`
--

LOCK TABLES `categories_texts` WRITE;
/*!40000 ALTER TABLE `categories_texts` DISABLE KEYS */;
INSERT INTO `categories_texts` VALUES (1,1,'uk','Декоративна косметика','Декоративна косметика','',''),(2,2,'uk','Подологія','подологія','',''),(3,3,'uk','Догляд за волоссям','Догляд за волоссям','',''),(4,1,'ru','Мейкап','Мейкап','',''),(5,1,'en','Мейкап','Мейкап','',''),(6,2,'ru','Нігті','Нігті','',''),(7,2,'en','Нігті','Нігті','',''),(8,3,'ru','Волосся','Волосся','',''),(9,3,'en','Волосся','Волосся','',''),(10,4,'uk','Догляд за обличчям','Догляд за обличчям','',''),(11,4,'ru','','','',''),(12,4,'en','','','',''),(13,5,'uk','Догляд за тілом','Догляд за тілом','',''),(14,5,'ru','','','',''),(15,5,'en','','','',''),(16,6,'uk','Догляд за руками','Догляд за руками','',''),(17,6,'ru','','','',''),(18,6,'en','','','',''),(19,7,'uk','Котеджі','','',''),(20,7,'ru','Котеджи','','',''),(21,7,'en','Котеджи','','','');
/*!40000 ALTER TABLE `categories_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('card','certificate') NOT NULL,
  `media_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates`
--

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
INSERT INTO `certificates` VALUES (1,'card',NULL),(2,'certificate',NULL),(3,'certificate',NULL),(4,'certificate',NULL);
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates_texts`
--

DROP TABLE IF EXISTS `certificates_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates_texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certificate_id` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates_texts`
--

LOCK TABLES `certificates_texts` WRITE;
/*!40000 ALTER TABLE `certificates_texts` DISABLE KEYS */;
INSERT INTO `certificates_texts` VALUES (1,1,'uk','Дисконтна накопичувальна картка','','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n'),(2,2,'uk','500','6 процедур',''),(3,3,'uk','1200','10 процедур',''),(4,4,'uk','2500','12 процедур',''),(5,1,'ru','Дисконтна накопичувальна картка','','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n'),(6,1,'en','Дисконтна накопичувальна картка','','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n'),(7,2,'ru','500','6 процедур',''),(8,2,'en','500','6 процедур',''),(9,3,'ru','1200','10 процедур',''),(10,3,'en','1200','10 процедур',''),(11,4,'ru','2500','12 процедур',''),(12,4,'en','2500','12 процедур','');
/*!40000 ALTER TABLE `certificates_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` VALUES (1,'instagram','Інстаграм'),(2,'certificates','Сертифікати'),(3,'main','Головна');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_id` (`gallery_id`,`media_id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_images`
--

LOCK TABLES `gallery_images` WRITE;
/*!40000 ALTER TABLE `gallery_images` DISABLE KEYS */;
INSERT INTO `gallery_images` VALUES (52,1,68),(51,1,67),(49,3,50),(48,3,17),(46,3,47),(47,3,48),(45,3,51),(50,1,66);
/*!40000 ALTER TABLE `gallery_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lang`
--

DROP TABLE IF EXISTS `lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(2) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `caption` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lang`
--

LOCK TABLES `lang` WRITE;
/*!40000 ALTER TABLE `lang` DISABLE KEYS */;
INSERT INTO `lang` VALUES (1,'uk',1,1,'Українська','UA'),(2,'ru',1,0,'Русский','RU'),(3,'en',1,0,'English','EN');
/*!40000 ALTER TABLE `lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `form` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('callback','feedback','order') COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ttn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads`
--

LOCK TABLES `leads` WRITE;
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
INSERT INTO `leads` VALUES (1,2,'Головна','Оформлення замовлення','callback','0000-00-00 00:00:00','Діма','+38 (089) 087-77-89','546','',0,'2016-01-17 13:01:38','2016-01-25 13:58:06',NULL),(2,2,'Головна','Запис на прийом','callback','0000-00-00 00:00:00','Діма','+38 (985) 858-84-87','іваіва','',0,'2016-01-17 13:06:22','2016-01-23 09:29:00',NULL),(3,4,'Головна','Запис на прийом','callback','0000-00-00 00:00:00','Tastjkjj','+38 (066) 002-02-93','otajic@gmail.com','',0,'2016-01-22 14:13:56','2016-01-22 15:08:13','2016-01-22 15:08:13'),(4,6,'Головна','Оформлення замовлення','callback','0000-00-00 00:00:00','bbvcb','+38 (145) 545-45-45','hjhgjghjgjghjgh','',0,'2016-01-22 15:05:29','2016-01-22 15:08:24','2016-01-22 15:08:24'),(5,1,'мсиисми','Оформлення замовлення. Послуга - смиси','callback','0000-00-00 00:00:00','Тест','+38 (000) 000-00-00','аааа','',0,'2016-02-01 12:11:46','2016-02-01 19:17:01','2016-02-01 19:17:01'),(6,1,'мсиисми','Оформлення замовлення. Послуга - смиси','callback','0000-00-00 00:00:00','Тест','+38 (000) 000-00-00','аааа','',0,'2016-02-01 12:11:49','2016-02-01 12:11:49',NULL),(7,1,'мсиисми','Оформлення замовлення. Послуга - смиси','callback','0000-00-00 00:00:00','Тест','+38 (000) 000-00-00','аааа','',0,'2016-02-01 12:11:52','2016-02-01 19:17:09','2016-02-01 19:17:09'),(8,1,'мсиисми','Оформлення замовлення. Послуга - смиси','callback','0000-00-00 00:00:00','Тест','+38 (000) 000-00-00','аааа@maul.ua','',0,'2016-02-01 12:12:17','2016-02-01 12:12:17',NULL),(9,1,'мсиисми','Оформлення замовлення. Послуга - смиси','callback','0000-00-00 00:00:00','Тест','+38 (000) 000-00-00','аааа@maul.ua','',0,'2016-02-01 12:12:20','2016-02-01 19:17:14','2016-02-01 19:17:14'),(10,1,'мсиисми','Оформлення замовлення. Послуга - смиси','callback','0000-00-00 00:00:00','Тест','+38 (000) 000-00-00','аааа@maul.ua','',0,'2016-02-01 12:12:22','2016-02-01 12:12:22',NULL),(11,2,'мсиисми','Оформлення замовлення. Послуга - смиси','callback','0000-00-00 00:00:00','Тест','+38 (000) 000-00-00','dfdfdf@mail.ua','',0,'2016-02-01 12:13:33','2016-02-01 18:58:02',NULL),(12,1,'Головна','Запис на прийом','callback','0000-00-00 00:00:00','ertert','+38 (345) 345-34-53','435@dfg.rt','',0,'2016-02-01 12:18:35','2016-02-01 12:18:35',NULL),(13,2,'Головна','Запис на прийом','callback','0000-00-00 00:00:00','Ольга','+38 (095) 493-24-63','ozhytar@gmail.com','',0,'2016-02-06 16:41:06','2016-02-09 11:41:55',NULL);
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_comments`
--

DROP TABLE IF EXISTS `leads_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lead_id` int(10) unsigned NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_comments`
--

LOCK TABLES `leads_comments` WRITE;
/*!40000 ALTER TABLE `leads_comments` DISABLE KEYS */;
INSERT INTO `leads_comments` VALUES (1,3,'облыччя\n9.00'),(2,1,'вавава'),(3,1,'hghgh'),(4,11,'400');
/*!40000 ALTER TABLE `leads_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `owner_id` int(10) unsigned DEFAULT NULL,
  `hash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ext` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `width` mediumint(8) unsigned NOT NULL,
  `height` mediumint(8) unsigned NOT NULL,
  `type` enum('file','dir') COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `options` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_parent_id_index` (`parent_id`),
  KEY `media_owner_id_index` (`owner_id`),
  KEY `media_hash_index` (`hash`),
  CONSTRAINT `media_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `acl_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `media_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `media` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (2,NULL,1,'','',0,0,'dir','Послуги','','2016-01-15 13:12:49','2016-01-22 14:35:09',NULL),(3,2,1,'3310bd263ade8d67f37b6996b47e35ba','png',0,0,'file','dogliad','{\"size\":0,\"mime\":\"\"}','2016-01-15 13:26:00','2016-01-15 22:17:04','2016-01-15 22:17:04'),(4,2,1,'267d9aadebe8926ee8bc409a9e868a85','png',0,0,'file','obraz','{\"size\":0,\"mime\":\"\"}','2016-01-15 13:26:00','2016-03-20 18:10:28','2016-03-20 18:10:28'),(5,2,1,'234536819adb1cb39c73ee99fc098328','png',0,0,'file','potologia','{\"size\":0,\"mime\":\"\"}','2016-01-15 13:26:01','2016-03-20 18:10:28','2016-03-20 18:10:28'),(6,2,1,'5a4ca75fd6366c8c341f182d22132355','png',0,0,'file','spa','{\"size\":0,\"mime\":\"\"}','2016-01-15 13:26:01','2016-03-20 18:10:28','2016-03-20 18:10:28'),(7,2,1,'2dbf3038531e4851b69dd7505d40c661','png',0,0,'file','oblychia','{\"size\":0,\"mime\":\"\"}','2016-01-15 13:26:01','2016-03-20 18:10:28','2016-03-20 18:10:28'),(8,2,1,'c8280c20218c5b68bb3f6d460cba7f41','jpeg',0,0,'file','b34al1NA','{\"size\":0,\"mime\":\"\"}','2016-01-15 20:16:06','2016-03-20 18:10:28','2016-03-20 18:10:28'),(9,2,1,'30151bf16650dcd1da364754691b9973','jpg',0,0,'file','3','{\"size\":0,\"mime\":\"\"}','2016-01-15 22:17:04','2016-03-20 18:10:28','2016-03-20 18:10:28'),(10,2,1,'08146adf99842dbae493241651308b69','png',0,0,'file','bitmap','{\"size\":0,\"mime\":\"\"}','2016-01-15 22:17:56','2016-03-20 18:10:28','2016-03-20 18:10:28'),(11,2,1,'9460ac2b5f32d7056af9dc88c3aff6df','png',0,0,'file','dogliad','{\"size\":0,\"mime\":\"\"}','2016-01-15 22:59:22','2016-03-20 18:10:28','2016-03-20 18:10:28'),(12,NULL,1,'3b4900caf3fa8b822b63ac0946c7c1a4','png',0,0,'file','cerivnyc','{\"size\":0,\"mime\":\"\"}','2016-01-16 14:43:05','2016-01-16 14:44:01',NULL),(13,NULL,1,'','',0,0,'dir','Відгуки','','2016-01-16 20:20:32','2016-01-16 20:20:32',NULL),(14,13,1,'b0373dd994fe5cc7c0f34832c109e625','png',0,0,'file','kyle','{\"size\":0,\"mime\":\"\"}','2016-01-16 20:20:49','2016-01-16 20:20:49',NULL),(15,NULL,1,'','',0,0,'dir','Новини','','2016-01-16 21:59:09','2016-01-16 21:59:09',NULL),(16,15,1,'57e0580751d705acbc2bff334fd34ff3','jpg',0,0,'file','792A4371','{\"size\":0,\"mime\":\"\"}','2016-01-16 22:01:26','2016-01-16 22:01:26',NULL),(17,15,1,'a55c858c29568d0c81ac17c7b8838e3d','jpg',0,0,'file','as','{\"size\":0,\"mime\":\"\"}','2016-01-16 22:01:33','2016-01-16 22:01:33',NULL),(18,15,1,'05f3bf72024d7413a0c36b4cbe1be4cb','jpg',0,0,'file','2','{\"size\":0,\"mime\":\"\"}','2016-01-16 22:01:40','2016-01-16 22:01:40',NULL),(19,15,1,'1fbf3cb202e8caa01d78a5eb8b07cd15','jpg',0,0,'file','3','{\"size\":0,\"mime\":\"\"}','2016-01-16 22:01:46','2016-01-16 22:01:46',NULL),(20,NULL,1,'','',0,0,'dir','Банери','','2016-01-16 23:09:47','2016-01-16 23:09:47',NULL),(21,20,1,'97ff2352b9f97800e1baf6cf6d22bf50','png',0,0,'file','akcia-1','{\"size\":0,\"mime\":\"\"}','2016-01-16 23:10:11','2016-01-16 23:10:12',NULL),(22,20,1,'5730efeb52d2fc2193ca4d4e831189d4','png',0,0,'file','akcia-2','{\"size\":0,\"mime\":\"\"}','2016-01-16 23:10:12','2016-01-16 23:10:12',NULL),(23,NULL,1,'','',0,0,'dir','Партнери','','2016-01-16 23:58:38','2016-01-16 23:58:38',NULL),(24,23,1,'5334f150ae22aa44226105ee946d0ade','png',0,0,'file','dmk-copy','{\"size\":0,\"mime\":\"\"}','2016-01-16 23:59:03','2016-01-16 23:59:03',NULL),(25,23,1,'0603e8d5d63d6a3ddb71a5d892ffdb5b','png',0,0,'file','ericson-copy','{\"size\":0,\"mime\":\"\"}','2016-01-16 23:59:04','2016-01-16 23:59:04',NULL),(26,23,1,'ab3c00441c0fc9aa1bae103b59a19e04','png',0,0,'file','sesderma-copy','{\"size\":0,\"mime\":\"\"}','2016-01-16 23:59:04','2016-01-16 23:59:04',NULL),(27,NULL,1,'','',0,0,'dir','Каталог','','2016-01-17 01:30:37','2016-01-17 01:30:37',NULL),(28,27,1,'3c7d30776584363407d8f21de4c9209c','png',0,0,'file','img-1','{\"size\":0,\"mime\":\"\"}','2016-01-17 01:31:08','2016-01-17 01:31:09',NULL),(29,27,1,'db762a74e3cba4e2160a4e1ec5f6ec60','png',0,0,'file','img-2','{\"size\":0,\"mime\":\"\"}','2016-01-17 01:31:16','2016-01-17 01:31:17',NULL),(30,27,1,'a7f1ffd3f6c06f138a66e1389cc65ca2','png',0,0,'file','img-3','{\"size\":0,\"mime\":\"\"}','2016-01-17 01:31:17','2016-01-17 01:31:17',NULL),(31,15,1,'','',0,0,'dir','неназвана папка','','2016-01-22 14:24:50','2016-01-22 14:24:58','2016-01-22 14:24:58'),(32,15,1,'ea335ab9b3aa110ba4fc50d750474d5c','jpg',0,0,'file','21','{\"size\":0,\"mime\":\"\"}','2016-01-22 14:25:54','2016-01-22 14:25:54',NULL),(33,NULL,1,'','',0,0,'dir','Інстаграм','','2016-01-22 14:50:15','2016-01-22 14:50:15',NULL),(34,33,1,'77cb814263b7ac408c4dbf6cfdfea762','png',0,0,'file','insta-1','{\"size\":0,\"mime\":\"\"}','2016-01-22 14:50:33','2016-01-22 14:50:33',NULL),(35,33,1,'dffd1f301ed7826c1cdb85457cc38fa6','png',0,0,'file','insta-2','{\"size\":0,\"mime\":\"\"}','2016-01-22 14:50:33','2016-01-22 14:50:33',NULL),(36,33,1,'1cd82c02749b1b1523e0215b8120ec5a','png',0,0,'file','insta-3','{\"size\":0,\"mime\":\"\"}','2016-01-22 14:50:33','2016-01-22 14:50:33',NULL),(37,33,1,'fcd1f45d1dfb36c7124ed3645c8a34ff','png',0,0,'file','insta-4','{\"size\":0,\"mime\":\"\"}','2016-01-22 14:50:33','2016-01-22 14:50:33',NULL),(38,NULL,1,'0bf76abfff4f8cfff9e91b089c7558ef','jpg',0,0,'file','Artboard','{\"size\":0,\"mime\":\"\"}','2016-02-01 11:24:10','2016-02-01 11:24:14',NULL),(39,NULL,1,'011fa9852ff00f4b8a324d47373190a4','jpg',0,0,'file','img_1','{\"size\":0,\"mime\":\"\"}','2016-02-01 11:25:00','2016-02-01 11:25:00',NULL),(40,NULL,1,'','',0,0,'dir','Галерея','','2016-02-02 13:10:03','2016-02-02 13:10:03',NULL),(41,40,1,'27e2078c7578196619ae130bdd92ce37','png',0,0,'file','792-a-4297-copy','{\"size\":0,\"mime\":\"\"}','2016-02-02 13:10:18','2016-02-02 13:10:18',NULL),(42,40,1,'138933584738fad137b4e5762cf786f2','png',0,0,'file','792-a-4484-copy','{\"size\":0,\"mime\":\"\"}','2016-02-02 13:11:00','2016-02-03 13:49:37','2016-02-03 13:49:37'),(43,40,1,'b3af76696eca328b2acd54c04b246f50','png',0,0,'file','792-a-4385-copy','{\"size\":0,\"mime\":\"\"}','2016-02-02 13:11:06','2016-02-03 13:49:42','2016-02-03 13:49:42'),(44,40,1,'ce3e95d67a5ae569cac2d98e4200ef7f','png',0,0,'file','792-a-4468-copy','{\"size\":0,\"mime\":\"\"}','2016-02-02 13:11:16','2016-02-03 13:49:29','2016-02-03 13:49:29'),(45,40,1,'2cbd06cb47dc2d2ee38d25f162bcafca','jpg',0,0,'file','792A4392','{\"size\":0,\"mime\":\"\"}','2016-02-02 13:11:29','2016-02-03 13:47:48','2016-02-03 13:47:48'),(46,NULL,1,'','',0,0,'dir','неназвана папка','','2016-02-03 13:43:48','2016-02-03 13:44:23','2016-02-03 13:44:23'),(47,40,1,'4bb99b7ca6783071b0342f06871d056a','jpg',0,0,'file','792A4484','{\"size\":0,\"mime\":\"\"}','2016-02-03 13:44:08','2016-02-03 13:45:53',NULL),(48,40,1,'23ecf86d7672fa1e7c290ce5bfbb2b68','jpg',0,0,'file','792A4385','{\"size\":0,\"mime\":\"\"}','2016-02-03 13:46:06','2016-02-03 13:46:07',NULL),(49,40,1,'ad62f00c09a280d792fb2b044b497f46','jpg',0,0,'file','792A4392','{\"size\":0,\"mime\":\"\"}','2016-02-03 13:47:33','2016-02-03 13:47:33',NULL),(50,40,1,'b802c393ade746c7175b8faab545c717','jpg',0,0,'file','792A4468','{\"size\":0,\"mime\":\"\"}','2016-02-03 13:49:23','2016-02-03 13:49:23',NULL),(51,40,1,'1c91c624457343acec269b3da2ac7b7a','jpg',0,0,'file','792A4297','{\"size\":0,\"mime\":\"\"}','2016-02-03 13:50:00','2016-02-03 13:50:00',NULL),(52,40,1,'b091463b79641233549a42dc36f543dc','jpg',0,0,'file','Закупорка пор','{\"size\":0,\"mime\":\"\"}','2016-02-09 11:48:43','2016-02-09 12:20:12',NULL),(53,40,1,'713ce4b1382f07757703665d13d16eb3','jpg',0,0,'file','Розширення пор','{\"size\":0,\"mime\":\"\"}','2016-02-09 12:32:16','2016-02-09 12:32:16',NULL),(54,40,1,'f496eff173556926e8536d7687725815','jpg',0,0,'file','Купероз','{\"size\":0,\"mime\":\"\"}','2016-02-09 12:41:46','2016-02-09 12:41:46',NULL),(55,40,1,'20fa7f230be273c272a46844b514aa1f','jpg',0,0,'file','Пігментація','{\"size\":0,\"mime\":\"\"}','2016-02-09 13:08:03','2016-02-09 13:08:03',NULL),(56,40,1,'83478e5ec783b418aa00566c50dba1b7','jpg',0,0,'file','Рубці','{\"size\":0,\"mime\":\"\"}','2016-02-09 13:20:12','2016-02-09 13:20:12',NULL),(57,40,1,'fe00289d40744bc04d062243f402d3db','jpg',0,0,'file','Набряки','{\"size\":0,\"mime\":\"\"}','2016-02-09 13:33:47','2016-02-09 13:33:47',NULL),(58,40,1,'b7f9c99df9c76295de90abe29ed2dabc','jpg',0,0,'file','Темні кола навколо очей','{\"size\":0,\"mime\":\"\"}','2016-02-09 13:42:16','2016-02-09 13:42:16',NULL),(59,40,1,'ba56f9562f08234eafc1f5c27af6545e','jpg',0,0,'file','Колагенова помпа','{\"size\":0,\"mime\":\"\"}','2016-02-09 13:46:49','2016-02-09 13:46:49',NULL),(60,40,1,'7c2fdf65091c57b0723c4c5ab2cabf96','jpg',0,0,'file','Зморшки','{\"size\":0,\"mime\":\"\"}','2016-02-09 13:48:28','2016-02-09 13:48:28',NULL),(61,40,1,'e53ccca908c85ef80e02642e49c8a97a','jpg',0,0,'file','Потрійна маска','{\"size\":0,\"mime\":\"\"}','2016-02-09 14:05:09','2016-02-09 14:05:09',NULL),(62,40,1,'986f5b454e8e48327a537344252a5fe2','jpg',0,0,'file','Судини','{\"size\":0,\"mime\":\"\"}','2016-02-09 14:05:40','2016-02-09 14:05:40',NULL),(63,27,1,'7ba78be4607dde85c773a123c5baa5ee','jpg',0,0,'file','E','{\"size\":0,\"mime\":\"\"}','2016-02-10 10:10:25','2016-02-10 10:10:25',NULL),(64,33,1,'beeefb9c4017d7626701005381ef728d','png',0,0,'file','1455331093_Neon_Line_Social_Circles-Instagram_50','{\"size\":0,\"mime\":\"\"}','2016-02-12 18:42:30','2016-02-12 18:42:30',NULL),(65,33,1,'77da8c507da2e0df563d40e139775f8c','png',0,0,'file','1455331401_Neon_Line_Social_Circles_50Icon_10px_grid-13','{\"size\":0,\"mime\":\"\"}','2016-02-12 18:44:22','2016-02-12 18:44:22',NULL),(66,27,1,'e6298bb12660c3fa0bbd75416e67d1d1','png',0,0,'file','cot1','{\"size\":0,\"mime\":\"\"}','2016-03-20 12:22:07','2016-03-20 12:22:07',NULL),(67,27,1,'489d4bcae4724a9446d2df7a0d968f52','png',0,0,'file','cot2','{\"size\":0,\"mime\":\"\"}','2016-03-20 12:22:17','2016-03-20 12:22:17',NULL),(68,27,1,'6b0717ca8182c12a7e1804ac892bbcb6','png',0,0,'file','cot3','{\"size\":0,\"mime\":\"\"}','2016-03-20 12:22:17','2016-03-20 12:22:17',NULL),(69,2,1,'3b87779809fcbe3853fdecc57b303fb7','png',0,0,'file','bike','{\"size\":0,\"mime\":\"\"}','2016-03-20 18:10:38','2016-03-20 18:10:38',NULL),(70,2,1,'89a4239656caf90e0c34828a40660f0f','png',0,0,'file','serv4','{\"size\":0,\"mime\":\"\"}','2016-03-20 18:14:40','2016-03-20 18:14:40',NULL),(71,2,1,'b22c837803a5018d63e777751622d902','jpg',0,0,'file','4H-3_0mZJUY-1024x764','{\"size\":0,\"mime\":\"\"}','2016-03-21 16:16:06','2016-03-21 16:16:06',NULL),(72,2,1,'89d7e0581c5010fee48d42ed4b97fe7c','jpg',0,0,'file','298634_284525124981300_1632300073_n','{\"size\":0,\"mime\":\"\"}','2016-03-21 16:17:32','2016-03-21 16:17:32',NULL);
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_01_08_170933_create_acl_users_table',1),('2016_01_08_171008_create_acl_groups_table',1),('2016_01_08_171019_create_acl_groups_users_table',1),('2016_01_10_131346_create_pages_table',2),('2016_01_10_131552_create_pages_texts_table',2),('2016_01_12_150510_create_media_table',3),('2016_01_15_130723_create_services_table',4),('2016_01_15_130746_create_services_texts_table',4),('2016_01_15_133250_create_services_categories_table',4),('2016_01_15_133303_create_services_prices_table',5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  `media_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'velosipedi',1,69,'2016-01-16 22:11:04','2016-03-24 15:36:20',NULL);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_gallery`
--

DROP TABLE IF EXISTS `news_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_gallery` (
  `news_id` int(10) unsigned NOT NULL,
  `media_id` int(10) unsigned NOT NULL,
  KEY `service_id` (`news_id`),
  KEY `media_id` (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_gallery`
--

LOCK TABLES `news_gallery` WRITE;
/*!40000 ALTER TABLE `news_gallery` DISABLE KEYS */;
INSERT INTO `news_gallery` VALUES (1,69);
/*!40000 ALTER TABLE `news_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_texts`
--

DROP TABLE IF EXISTS `news_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `text` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `news_id_lang` (`news_id`,`lang`),
  KEY `news_id` (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_texts`
--

LOCK TABLES `news_texts` WRITE;
/*!40000 ALTER TABLE `news_texts` DISABLE KEYS */;
INSERT INTO `news_texts` VALUES (1,1,'uk','Велосипеди','','<div class=\"text\">Любители экстремального спорта могут покататься по горным дорогам на велосипеде. Это весёлый и увлекательный способ транспортировки на открытом воздухе, при этом Вы сполна сможете насладиться чудесными пейзажами Карпат.</div>\r\n','Велосипеди','',''),(4,1,'ru','Велосипеди','','<div class=\"text\">Любители экстремального спорта могут покататься по горным дорогам на велосипеде. Это весёлый и увлекательный способ транспортировки на открытом воздухе, при этом Вы сполна сможете насладиться чудесными пейзажами Карпат.</div>\r\n','Велосипеди','',''),(5,1,'en','Велосипеди','','<div class=\"text\">Любители экстремального спорта могут покататься по горным дорогам на велосипеде. Это весёлый и увлекательный способ транспортировки на открытом воздухе, при этом Вы сполна сможете насладиться чудесными пейзажами Карпат.</div>\r\n','Велосипеди','','');
/*!40000 ALTER TABLE `news_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `additional` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'screen_1','screen','{\"video\":\"<iframe src=\\\"https:\\/\\/www.youtube.com\\/embed\\/mi0D-ooVh48\\\" frameborder=\\\"0\\\" allowfullscreen><\\/iframe>\"}',1,'2016-12-01 12:07:45','2016-01-15 10:19:28'),(2,'main','static','',1,'2016-12-01 12:07:45','2016-12-01 12:07:45'),(3,'news','static','',1,'2016-12-01 12:07:45','2016-12-01 12:07:45'),(4,'contacts','screen','[]',1,'2016-12-01 12:07:45','2016-12-01 12:07:45'),(5,'catalog','static','',1,'2016-12-01 12:07:45','2016-12-01 12:07:45'),(6,'services','static','',1,'2016-12-01 12:07:45','2016-12-01 12:07:45'),(7,'certificates','static','',1,'2016-12-01 12:07:45','2016-12-01 12:07:45'),(8,'gallery','static','',1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_texts`
--

DROP TABLE IF EXISTS `pages_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_texts`
--

LOCK TABLES `pages_texts` WRITE;
/*!40000 ALTER TABLE `pages_texts` DISABLE KEYS */;
INSERT INTO `pages_texts` VALUES (1,1,'uk','Косметологічний центр «Naturel» - косметологія обличчя та тіла','«Шукати красу всюди, де тільки можна її знайти і дарувати її тим, хто поруч з тобою»','','','',''),(2,2,'uk','Головна','','','Долина Миколая','Долина Миколая',''),(3,3,'uk','Блог','','','Блог','',''),(4,4,'uk','Контакти','','<p class=\"adress\">м. Чернівці,&nbsp;<bold>вул. Запорізька, 1-Б,</bold> на розі&nbsp;вул. Сторожинецької (навпроти автобусної зупинки)</p>\r\n\r\n<p class=\"tel\">(0372) 51 31 33 (095) 067 37 26 (097) 977 66 06</p>\r\n\r\n<p class=\"mail\"><bold>Email:</bold> beautyskin1@gmail.com<br />\r\n<bold>Час роботи:</bold> 9:00-21:00</p>\r\n\r\n<p class=\"mail\"><a href=\"https://www.instagram.com/naturel_cosmetology_center/\" target=\"_blank\"><img alt=\"\" src=\"/media/be/ee/64-beeefb9c4017d7626701005381ef728d.png\" style=\"width: 30px; height: 30px;\" /></a>&nbsp;&nbsp; &nbsp;<img alt=\"\" src=\"/media/77/da/65-77da8c507da2e0df563d40e139775f8c.png\" style=\"width: 30px; height: 30px;\" /></p>\r\n','Контакти','',''),(5,5,'uk','Каталог','','','Каталог','',''),(6,6,'uk','Послуги','','','Послуги','',''),(7,7,'uk','Сертифікати','','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n','Сертифікати','',''),(8,1,'ru','Косметологічний центр «Naturel» - косметологія обличчя та тіла','«Шукати красу всюди, де тільки можна її знайти і дарувати її тим, хто поруч з тобою»','','','',''),(9,1,'en','Косметологічний центр «Naturel» - косметологія обличчя та тіла','«Шукати красу всюди, де тільки можна її знайти і дарувати її тим, хто поруч з тобою»','','','',''),(10,2,'ru','Головна','','','Долина Миколая','Долина Миколая',''),(11,2,'en','Головна','','','Долина Миколая','Долина Миколая',''),(12,3,'ru','Блог','','','Блог','',''),(13,3,'en','Блог','','','Блог','',''),(14,4,'ru','Контакты','','<p class=\"adress\">м. Чернівці<br />\r\n<bold>вул. Запорізька, 1-Б,</bold> на розі&nbsp;вул. Сторожинецької (навпроти автобусної зупинки)</p>\r\n\r\n<p class=\"tel\">(0372) 51 31 33 (095) 067 37 26 (097) 977 66 06</p>\r\n\r\n<p class=\"mail\"><bold>Email:</bold> beautyskin1@gmail.com<br />\r\n<bold>Час роботи:</bold> 9:00-21:00</p>\r\n\r\n<p class=\"mail\"><a href=\"https://www.instagram.com/naturel_cosmetology_center/\" target=\"_blank\"><img alt=\"\" src=\"/media/be/ee/64-beeefb9c4017d7626701005381ef728d.png\" style=\"width: 30px; height: 30px;\" /></a>&nbsp;&nbsp; &nbsp;<img alt=\"\" src=\"/media/77/da/65-77da8c507da2e0df563d40e139775f8c.png\" style=\"width: 30px; height: 30px;\" /></p>\r\n','Контакти','',''),(15,4,'en','Контакти','','<p class=\"adress\">м. Чернівці<br />\r\n<bold>вул. Запорізька, 1-Б,</bold> на розі&nbsp;вул. Сторожинецької (навпроти автобусної зупинки)</p>\r\n\r\n<p class=\"tel\">(0372) 51 31 33 (095) 067 37 26 (097) 977 66 06</p>\r\n\r\n<p class=\"mail\"><bold>Email:</bold> beautyskin1@gmail.com<br />\r\n<bold>Час роботи:</bold> 9:00-21:00</p>\r\n\r\n<p class=\"mail\"><a href=\"https://www.instagram.com/naturel_cosmetology_center/\" target=\"_blank\"><img alt=\"\" src=\"/media/be/ee/64-beeefb9c4017d7626701005381ef728d.png\" style=\"width: 30px; height: 30px;\" /></a>&nbsp;&nbsp; &nbsp;<img alt=\"\" src=\"/media/77/da/65-77da8c507da2e0df563d40e139775f8c.png\" style=\"width: 30px; height: 30px;\" /></p>\r\n','Контакти','',''),(16,5,'ru','Каталог','','','Каталог','',''),(17,5,'en','Каталог','','','Каталог','',''),(18,6,'ru','Послуги','','','Послуги','',''),(19,6,'en','Послуги','','','Послуги','',''),(20,7,'ru','Сертифікати','','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n','Сертифікати','',''),(21,7,'en','Сертифікати','','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n','Сертифікати','',''),(22,8,'en','Галерея','Галерея','Галерея','Галерея','Галерея','Галерея'),(23,8,'uk','Галерея','Галерея','Галерея','Галерея','Галерея','Галерея'),(24,8,'ru','Галерея','Галерея','Галерея','Галерея','Галерея','Галерея');
/*!40000 ALTER TABLE `pages_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `media_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (1,'DMK','http://dannemking.com/',24,'2016-01-17 00:04:25','2016-01-23 13:16:33','2016-01-23 13:16:33'),(2,'Ericson Laboratoire','http://www.ericsonlab.ru/',25,'2016-01-17 00:06:50','2016-01-17 00:06:50',NULL),(3,'SESDERMA','http://sesderma.com.ua/',26,'2016-01-17 00:07:59','2016-01-17 00:07:59',NULL),(4,'DMK','',24,'2016-02-01 19:40:35','2016-02-01 19:40:35',NULL);
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `media_id` int(10) unsigned NOT NULL,
  `slug` varchar(255) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `price` varchar(255) NOT NULL,
  `article` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,7,67,'kotedzh-2',1,'1500','2','2016-01-17 01:41:11','2016-03-20 17:01:19'),(2,7,66,'kotedzh',1,'1500','1','2016-01-17 01:47:03','2016-03-20 12:23:19'),(3,7,68,'kotedzh-3',1,'1300','3','2016-03-20 18:03:45','2016-03-20 18:03:45');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_gallery`
--

DROP TABLE IF EXISTS `products_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_gallery` (
  `product_id` int(10) NOT NULL,
  `media_id` int(10) NOT NULL,
  KEY `product_id` (`product_id`,`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_gallery`
--

LOCK TABLES `products_gallery` WRITE;
/*!40000 ALTER TABLE `products_gallery` DISABLE KEYS */;
INSERT INTO `products_gallery` VALUES (1,67),(2,66),(2,67),(2,68),(3,67),(3,68);
/*!40000 ALTER TABLE `products_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_texts`
--

DROP TABLE IF EXISTS `products_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `lang` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `description_detail` text NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_id_lang` (`product_id`,`lang`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_texts`
--

LOCK TABLES `products_texts` WRITE;
/*!40000 ALTER TABLE `products_texts` DISABLE KEYS */;
INSERT INTO `products_texts` VALUES (1,1,'uk','Котедж 2','Котедж 2','<p>текст</p>\r\n','<ul>\r\n	<li>Wifi</li>\r\n	<li>Душ</li>\r\n</ul>\r\n'),(2,2,'uk','Котедж','Просторная и уютная гостиная.','<p>Просторная и уютная гостиная, это идеальное место для уютного семейного праздника или вечеринки в компании друзей. Наш котедж предлагает гостям такие самые современные удобства, как бесплатный доступ к сети Интернет и жидкокристаллические панели телевизор</p>\r\n','<ul>\r\n	<li>Кухня</li>\r\n	<li>WIFI</li>\r\n	<li>LCD панель</li>\r\n	<li>Тераса</li>\r\n	<li>Парковка</li>\r\n	<li>Бесідка і мангал</li>\r\n</ul>\r\n'),(3,1,'ru','Котедж 2','Котедж 2','',''),(4,1,'en','Котедж 2','Котедж 2','',''),(5,2,'ru','Котедж','','<p>Котедж 1</p>\r\n','<p>Котедж 1</p>\r\n'),(6,2,'en','Котедж','','',''),(7,3,'uk','Котедж 3','Новий комфорт','','<ul>\r\n	<li>Телевізор</li>\r\n	<li>Тепловізор</li>\r\n	<li>Телескоп</li>\r\n</ul>\r\n'),(8,3,'ru','','','',''),(9,3,'en','','','','');
/*!40000 ALTER TABLE `products_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `main` tinyint(1) unsigned DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `media_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,1,1,12,'2016-01-16 14:44:06','2016-01-16 14:44:06',NULL),(7,NULL,1,14,'2016-01-16 20:43:33','2016-01-16 20:43:33',NULL),(8,NULL,1,14,'2016-01-16 20:46:24','2016-01-16 20:46:24',NULL),(9,NULL,1,NULL,'2016-01-16 20:48:48','2016-01-16 20:48:52','2016-01-16 20:48:52');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews_texts`
--

DROP TABLE IF EXISTS `reviews_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `review_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews_texts`
--

LOCK TABLES `reviews_texts` WRITE;
/*!40000 ALTER TABLE `reviews_texts` DISABLE KEYS */;
INSERT INTO `reviews_texts` VALUES (1,1,'uk','Іван Іванович','Менеджер',''),(25,7,'uk','Kyle Brown','','Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент – написание символов на кириллице значительно.'),(26,8,'uk','Вася Пупкін','','Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент – написание символов на кириллице значительно.'),(27,9,'uk','вапвап','','вапвап'),(28,1,'ru','Іван Іванович','',''),(29,1,'en','Іван Іванович','',''),(30,7,'ru','Kyle Brown','','Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент – написание символов на кириллице значительно.'),(31,7,'en','Kyle Brown','','Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент – написание символов на кириллице значительно.'),(32,8,'ru','Вася Пупкін','','Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент – написание символов на кириллице значительно.'),(33,8,'en','Вася Пупкін','','Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент – написание символов на кириллице значительно.'),(34,9,'ru','вапвап','','вапвап'),(35,9,'en','вапвап','','вапвап');
/*!40000 ALTER TABLE `reviews_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL,
  `media_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (5,21,NULL,'zakuporka-por','',0,1,62,'2016-01-15 23:15:23','2016-03-20 18:07:59','2016-03-20 18:07:59'),(6,21,NULL,'msiismi','',0,1,53,'2016-01-22 14:40:58','2016-03-20 18:08:01','2016-03-20 18:08:01'),(7,21,NULL,'zakuporka-por','',0,1,52,'2016-01-25 14:47:53','2016-03-20 18:08:04','2016-03-20 18:08:04'),(8,21,NULL,'pigmentatsiya','',0,1,55,'2016-02-09 13:11:00','2016-03-20 18:08:06','2016-03-20 18:08:06'),(9,21,NULL,'rubtsi-postakne','',0,1,56,'2016-02-09 13:20:20','2016-03-20 18:08:08','2016-03-20 18:08:08'),(10,21,NULL,'nabryaki','',0,1,57,'2016-02-09 13:33:56','2016-03-20 18:08:09','2016-03-20 18:08:09'),(11,21,NULL,'temni-kola-navkolo-ochey','',0,1,58,'2016-02-09 13:42:24','2016-03-20 18:08:11','2016-03-20 18:08:11'),(12,21,NULL,'velosipedi','',0,1,69,'2016-02-09 13:48:38','2016-03-20 18:11:00',NULL),(13,NULL,12,'rishennya-problemi','200',0,1,57,'2016-02-12 12:51:54','2016-03-20 18:08:13','2016-03-20 18:08:13'),(14,NULL,5,'exilite','300',0,1,53,'2016-02-12 13:01:21','2016-03-20 18:07:57','2016-03-20 18:07:57'),(15,NULL,5,'profesiyna-kosmetika-dmk','300',0,1,61,'2016-02-12 13:05:41','2016-03-20 18:07:55','2016-03-20 18:07:55'),(16,NULL,5,'gidromekhanopiling','400',0,1,47,'2016-02-12 13:07:25','2016-03-20 18:07:50','2016-03-20 18:07:50');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_categories`
--

DROP TABLE IF EXISTS `services_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL,
  `media_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_categories`
--

LOCK TABLES `services_categories` WRITE;
/*!40000 ALTER TABLE `services_categories` DISABLE KEYS */;
INSERT INTO `services_categories` VALUES (17,'zavershennya-obrazu',4,1,4,'2016-01-15 23:11:07','2016-03-20 18:13:00','2016-03-20 18:13:00'),(18,'eksklyuzivni-spa-programi',3,1,6,'2016-01-15 23:12:01','2016-03-20 18:12:57','2016-03-20 18:12:57'),(19,'podologiya-doglyad-za-stopami-i-gomilkami',2,1,5,'2016-01-15 23:13:25','2016-03-20 18:12:54','2016-03-20 18:12:54'),(20,'doglyad-za-tilom',1,1,11,'2016-01-15 23:13:51','2016-03-20 18:12:51','2016-03-20 18:12:51'),(21,'velosipedi',0,1,69,'2016-01-15 23:14:05','2016-03-20 18:13:44',NULL),(22,'lyzhi-i-snoubord',0,1,70,'2016-03-20 18:14:47','2016-03-20 18:14:47',NULL),(23,'banya',0,1,71,'2016-03-21 16:16:13','2016-03-21 16:16:13',NULL),(24,'baseyn',0,1,72,'2016-03-21 16:17:37','2016-03-21 16:17:37',NULL);
/*!40000 ALTER TABLE `services_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_gallery`
--

DROP TABLE IF EXISTS `services_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_gallery` (
  `service_id` int(10) unsigned NOT NULL,
  `media_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_gallery`
--

LOCK TABLES `services_gallery` WRITE;
/*!40000 ALTER TABLE `services_gallery` DISABLE KEYS */;
INSERT INTO `services_gallery` VALUES (7,52),(8,55),(9,56),(10,57),(11,58),(11,59),(13,60),(13,52),(5,54),(5,62),(14,53),(15,61),(16,47),(6,53),(12,69);
/*!40000 ALTER TABLE `services_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_prices`
--

DROP TABLE IF EXISTS `services_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned NOT NULL,
  `title_uk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_prices`
--

LOCK TABLES `services_prices` WRITE;
/*!40000 ALTER TABLE `services_prices` DISABLE KEYS */;
INSERT INTO `services_prices` VALUES (71,7,'Гідромеханопілінг ','','','580','2016-02-09 12:27:34','2016-02-09 12:27:34',NULL),(72,7,'Авторське очищення шкіри','','','360','2016-02-09 12:27:34','2016-02-09 12:27:34',NULL),(135,8,'Лазерна процедура Exilit. Ліквідація Пігментації','','','400','2016-02-09 13:14:13','2016-02-09 13:14:13',NULL),(136,8,'Процедури DMK - Melanoplex (лікування гіперпігментації)','','','965','2016-02-09 13:14:13','2016-02-09 13:14:13',NULL),(137,8,'Гідромеханопілінг','','','580','2016-02-09 13:14:13','2016-02-09 13:14:13',NULL),(144,9,'Лазерна процедура Exilit','','','100','2016-02-09 13:23:44','2016-02-09 13:23:44',NULL),(145,9,'Гідромеханопілінг','','','580','2016-02-09 13:23:44','2016-02-09 13:23:44',NULL),(162,10,'Ендермологія (лімфодренажний масаж)','','','225','2016-02-09 13:40:33','2016-02-09 13:40:33',NULL),(163,10,'Гідромеханопілінг','','','580','2016-02-09 13:40:33','2016-02-09 13:40:33',NULL),(164,10,'RF - процедура Радіо-ліфтингу','','','225','2016-02-09 13:40:33','2016-02-09 13:40:33',NULL),(165,10,'Ericson Laboratoire - BI-PATCH лімфодренажні маски','','','1020','2016-02-09 13:40:33','2016-02-09 13:40:33',NULL),(174,11,'Ендермологія (лімфодренажний масаж)','','','225','2016-02-09 13:46:56','2016-02-09 13:46:56',NULL),(175,11,'Процедури DMK - Колагенова помпа','','','140','2016-02-09 13:46:56','2016-02-09 13:46:56',NULL);
/*!40000 ALTER TABLE `services_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_texts`
--

DROP TABLE IF EXISTS `services_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) unsigned DEFAULT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_texts`
--

LOCK TABLES `services_texts` WRITE;
/*!40000 ALTER TABLE `services_texts` DISABLE KEYS */;
INSERT INTO `services_texts` VALUES (18,17,NULL,'uk','Завершення образу','','','Завершення образу','',''),(19,18,NULL,'uk','Ексклюзивні СПА-програми','','','Ексклюзивні СПА-програми','',''),(20,19,NULL,'uk','Подологія — догляд за стопами і гомілками','','','Подологія — догляд за стопами і гомілками','',''),(21,20,NULL,'uk','Догляд за тілом','','','Догляд за тілом','',''),(22,21,NULL,'uk','Велосипеди','','','','Любители экстремального спорта могут покататься по горным дорогам на велосипеде. Это весёлый и увлекательный способ транспортировки на открытом воздухе, при этом Вы сполна сможете насладиться чудесными пейзажами Карпат.',''),(23,NULL,5,'uk','Купероз (розширення судин)','','<p>Купероз &ndash; зниження еластичності стінок підшкірних капілярів, із стійким локальним їх розширенням, порушенням мікроциркуляції крові та живлення шкіри.</p>\r\n\r\n<p>Купероз визначається появою специфічного судинного малюнку у вигляді сітки (частіше всього на обличчі), або у &nbsp;вигляді судинних зірочок.</p>\r\n\r\n<p><u>​</u></p>\r\n','Закупорка пор','',''),(25,NULL,1,'uk','Єсипчук Оксана Василівна','Керівник та головний лікар','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n','','',''),(26,NULL,6,'uk','Розширення пор','В залежності від збільшення пор','<ul>\r\n	<li><u>Авторське очищення шкіри</u>.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">В Косметологічному центрі Naturel представлена авторська методика, яка відбувається за рахунок гідруючої системи, що допомагає підготувати шкіру до нетравматичної ліквідації закупорки, та використовується ексклюзивна техніка очищення, яка підбирається індивідуально в залежності від потреби: інструментальна чи апаратна. &nbsp;Після виконаної методики шкіра обличчя очищена, без пошкоджень з відчуттям легкості та комфорту. Тривалість процедури 1,5 години. Рекомендоване проведення процедури 1 раз в місяць або по необхідності.</p>\r\n\r\n<ul>\r\n	<li><u>Використання пілінга Джеснера. </u></li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">Косметологічна процедура, що представлена в Косметологічному центрі Naturel має на меті поліпшити стан шкіри та її &nbsp;зовнішній вигляд, за допомогою спеціальних хімічних складників.</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">Можливі варіанти виконання пілінгу: поверхневий, поверхнево-серединний, серединний, глибокий.</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">Рекомендуються по показам, якщо потрібно поліпшити колір шкіри, відновити структуру &ndash; використовують поверхневі пілінги, які не провокують сухість, злущення почервоніння - це аргеніновий, азелоїновий, мигдальний.</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">Якщо є проблем на обличчі такі як запальні елементи (червони прищі), закриті комедони (закупорка яка не має отвору), рубці (розрив в глибокому шарі сполучної тканини), тобто дерма &ndash; тому результативніше використовувати поверхнево &ndash; сединні, серединні пілінги, такі як Джеснера і реакція шкіри після пілінга обов&rsquo;язково така, як почервоніння, відчуття тепла, на наступний день стянутість, і через два три дні злущення дрібно пластичне або більш крупними пластинами, в залежності від глибини проблеми, яка є в глибинних шарах шкіри. Пілінг це косметологічна процедура косметологічного центру Naturel, що має на меті покращити стан шкіри та її зовнішній вигляд, за допомогою спеціальних хімічних складників.</p>\r\n\r\n<p>Молочна кислота в свою чергу має відлущуючу та відбілюючу дію, вона також стимулює відновлення колагену &ndash; певного білка, що відповідає за еластичність та пружність шкіри та найголовніша перевага її в унікальній властивості з&rsquo;єднувати, утримувати та рівномірно розприділяти вологу, направляючи її в глибинні шари шкіри.</p>\r\n\r\n<p>Саліцилова кислота &ndash; виконує антибактеріальну дію та підсилює дію молочної кислоти.</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">Резорцин &ndash; ефективний ексфоліант, саме він відповідає за відшелушення та видалення верхнього шару епідермісу після пілінгу. Примножує дію попередніх двох кислот.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Процедури DMK</u>. Ксклюзивні методики Косметологічного центру Naturel.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:35.4pt;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">&ndash; Професійна косметика DMK, ексклюзив Косметологічного центру &nbsp;Naturel (Danne) існує на ринку вже 45 років і є світовим лідером в корекції проблем шкіри, як косметологічних так і ряду дерматологічних.</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">концепцію роботи зі шкірою, яка містить наступні чотири кроки: ліквідувати, відновити, захистити та підтримати.</p>\r\n\r\n<p style=\"margin-left:35.4pt;\">Унікальна процедура технології лікування розширених пор, представлена в Косметологічному центрі, а саме &ldquo;Рідкий лазер&rdquo;, оскільки по ефективності його порівнюють з лазерною шліфовкою, а також вона позбавлена ризику побічних ефектів.</p>\r\n\r\n<p>Дана процедура вирішує проблему рознирених пор. Триває вона 1 годину 30 хвилин.</p>\r\n\r\n<p>Курс складає 8 процедур, які виконуються 1 раз в 3 тижні.</p>\r\n\r\n<p>Розширені пори часто приносять багато неприємних відчуттів людям які їх мають - обличчя виглядає, як апельсинова кірка, що є дуже не естетичним, а декоративна косметика та засоби для догляду, що наносяться на таку шкіру - підкреслюють проблему.</p>\r\n\r\n<p>Дане Монтегю Кінг - засновник лінії ДМК (США) - розробив процедуру, яка не має аналогів в світі та дав назву їй - Лікування носу, дл боротьби з розширеними порами.</p>\r\n\r\n<p>Дана процедура може використовуватись не тільки на ділянці носу, але і на чолі, підборідді, щічках - на будь-яких зонах, де присутні розширені пори.</p>\r\n\r\n<p>Під час процедури на шкіру обличчя наноситься незвичайний пілінг з лужною а потім кислою фазами, що сприяє розчепленню білків поверхневого шару шкіри, та розгладжує края кожної пори, зменшуючи її глибину.</p>\r\n\r\n<p>Після такої взаємодії шкіра інтенсивно охолоджується кубиками льоду та відновлюється з допомогою всесвітньо відомих сировиток та ферментних масок Данне.</p>\r\n\r\n<p>Процедура яку представляє Косметологічний центр з допомогою прописаних протоколів Данне - &ldquo;видалення розширених пор по методиці Данне&rdquo; дає швидкий результат після першого проведення, та тривалий видимий ефект після першого використання, стійкий ефект шліфовки після курсу з 8-ми процедур.</p>\r\n\r\n<p>Якщо раніше подібного роду естетичні проблеми могли бути вирішені тільки з домогою глубокої лазерної шліфовки, що виконується під наркозом, і з реабілітаційним періодом не менше місяця, то тепер це - комфортна терапевтична процедура в Косметологічному центрі, без ризику випадіння із соціального життя.</p>\r\n\r\n<p>Цю процедуру не даром називають &ldquo;Рідкий лазер&rdquo; оскільки вона повністю порівнюється з лазерною шліфовкою, та не має ризику побічних ефектів, негативних наслідків, таких як рубці та пігментні плями.</p>\r\n\r\n<p>Лікування розширених пор по методу Данне - вибір в тих випадках, коли результат дійсно необхідний!</p>\r\n\r\n<p>Після проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому &nbsp;знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівших клітиночок, які заважали шкірі дихати. Після чого зявляється &nbsp;відчуття легкості, сяяння та бархатистості шкіри.</p>\r\n\r\n<p style=\"margin-left:35.4pt;\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:35.4pt;\">Під час процедури на очищену шкіру обличчя наноситься спеціальний пілінг із лужною речовиною а потім з кислою фазами, що сприяє розчепленню білків поверхневого шару шкіри та розгладженню країв кожної пори із зменшенням глибини. Після такої дії шкіра інтенсивно охолоджується кубиками льоду та відновлюється з допомогою всесвітньо відомих сироваток та ферментних масок Данне.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Гідромеханопілінг</u> &ndash; (улюблений апарат Мадонни) &ndash; дана процедура є дуже популярною в Косметологічному центрі Naturel &nbsp;Апаратна процедура гідромеханопілінгу, полягає в одночасній шліфовці шкіри з поданням &nbsp;на неї спеціальної сировитки.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">Даний апарат представляє собою герметичну вакуумно-проточну систему з робочою абразивною насадкою, виконаною із алмазу. В апарат під&rsquo;єднується спеціальна лікувальна сироватка, яка під час мікрошліфування поверхні шкіри вводится з допомогою тиску у верхні шари шкіри. Сироватка підбирається в залежності від проблеми, а саме:</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація зморшок</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація пігментації</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація запальних елементів на обличчі</p>\r\n\r\n<p>По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, розгладження зморшок, очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, &nbsp;шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.</p>\r\n\r\n<p>Після проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому &nbsp;знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівших клітиночок, які заважали шкірі дихати. Після чого зявляється &nbsp;відчуття легкості, сяяння та бархатистості шкіри.</p>\r\n','','',''),(27,NULL,7,'uk','Закупорка пор','','<ul>\r\n	<li><u>Авторське очищення шкіри</u>.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">В Косметологічному центрі Naturel представлена авторська методика, яка відбувається за рахунок гідруючої системи, що допомагає підготувати шкіру до нетравматичної ліквідації закупорки, та використовується ексклюзивна техніка очищення, яка підбирається індивідуально в залежності від потреби: інструментальна чи апаратна. &nbsp;Після виконаної методики шкіра обличчя очищена, без пошкоджень з відчуттям легкості та комфорту. Тривалість процедури 1,5 години. Рекомендоване проведення процедури 1 раз в місяць або по необхідності.</p>\r\n\r\n<ul>\r\n	<li><u>Гідромеханопілінг</u> &ndash; (улюблений апарат Мадонни) представлений в Косметологічному центрі Naturel саме апаратною процедурою, яка полягає в одночасній шліфовці шкіри з поданням з на неї спеціальної сировитки. Даний апарат представляє собою герметичну вакуумно-проточну систему з робочою абразивною насадкою, виконаною із алмазу. В апарат під&rsquo;єднується спеціальна лікувальна сироватка, яка під час мікрошліфування поверхні шкіри вводится з допомогою тиску у верхні шари шкіри. Сироватка підбирається в залежності від проблеми, а саме:</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація зморшок</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація пігментації</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація запальних елементів на обличчі</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, розгладження зморшок, очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, &nbsp;шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">Після проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому &nbsp;знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівши клітиночок, які заважають шкірі дихати і ваша шкіра відчуває легкість, сяяння та бархатистість.</p>\r\n','','',''),(28,17,NULL,'ru','Завершення образу','','','Завершення образу','',''),(29,17,NULL,'en','Завершення образу','','','Завершення образу','',''),(30,18,NULL,'ru','Ексклюзивні СПА-програми','','','Ексклюзивні СПА-програми','',''),(31,18,NULL,'en','Ексклюзивні СПА-програми','','','Ексклюзивні СПА-програми','',''),(32,19,NULL,'ru','Подологія — догляд за стопами і гомілками','','','Подологія — догляд за стопами і гомілками','',''),(33,19,NULL,'en','Подологія — догляд за стопами і гомілками','','','Подологія — догляд за стопами і гомілками','',''),(34,20,NULL,'ru','Догляд за тілом','','','Догляд за тілом','',''),(35,20,NULL,'en','Догляд за тілом','','','Догляд за тілом','',''),(36,21,NULL,'ru','Велосипеди','','','','',''),(37,21,NULL,'en','Велосипеди','','','','',''),(38,NULL,5,'ru','Закупорка пор','','<h3>Авторське очищення шкіри.</h3>\r\n\r\n<p>В Косметологічному центрі Naturel представлена авторська методика, яка відбувається за рахунок гідруючої системи, що допомагає підготувати шкіру до нетравматичної ліквідації закупорки, та використовується ексклюзивна техніка очищення, яка підбирається індивідуально в залежності від потреби: інструментальна чи апаратна. Після виконаної методики шкіра обличчя очищена, без пошкоджень з відчуттям легкості та комфорту. Тривалість процедури 1,5 години. Рекомендоване проведення процедури 1 раз в місяць або по необхідності.</p>\r\n\r\n<h3>Гідромеханопілінг</h3>\r\n\r\n<p>(Улюблений апарат Мадонни) представлений в Косметологічному центрі Naturel саме апаратною процедурою, яка полягає в одночасній шліфовці шкіри з поданням з на неї спеціальної сировитки. Даний апарат представляє собою герметичну вакуумно-проточну систему з робочою абразивною насадкою, виконаною із алмазу. В апарат під&rsquo;єднується спеціальна лікувальна сироватка, яка під час мікрошліфування поверхні шкіри вводится з допомогою тиску у верхні шари шкіри. Сироватка підбирається в залежності від проблеми, а саме:</p>\r\n\r\n<ul>\r\n	<li>Ліквідація зморшок</li>\r\n	<li>Ліквідація пігментації</li>\r\n	<li>Ліквідація запальних елементів на обличчі</li>\r\n</ul>\r\n\r\n<p>По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, розгладження зморшок, очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.<br />\r\nПісля проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівши клітиночок, які заважають шкірі дихати і ваша шкіра відчуває легкість, сяяння та бархатистість.</p>\r\n','Закупорка пор','',''),(39,NULL,5,'en','Закупорка пор','','<h3>Авторське очищення шкіри.</h3>\r\n\r\n<p>В Косметологічному центрі Naturel представлена авторська методика, яка відбувається за рахунок гідруючої системи, що допомагає підготувати шкіру до нетравматичної ліквідації закупорки, та використовується ексклюзивна техніка очищення, яка підбирається індивідуально в залежності від потреби: інструментальна чи апаратна. Після виконаної методики шкіра обличчя очищена, без пошкоджень з відчуттям легкості та комфорту. Тривалість процедури 1,5 години. Рекомендоване проведення процедури 1 раз в місяць або по необхідності.</p>\r\n\r\n<h3>Гідромеханопілінг</h3>\r\n\r\n<p>(Улюблений апарат Мадонни) представлений в Косметологічному центрі Naturel саме апаратною процедурою, яка полягає в одночасній шліфовці шкіри з поданням з на неї спеціальної сировитки. Даний апарат представляє собою герметичну вакуумно-проточну систему з робочою абразивною насадкою, виконаною із алмазу. В апарат під&rsquo;єднується спеціальна лікувальна сироватка, яка під час мікрошліфування поверхні шкіри вводится з допомогою тиску у верхні шари шкіри. Сироватка підбирається в залежності від проблеми, а саме:</p>\r\n\r\n<ul>\r\n	<li>Ліквідація зморшок</li>\r\n	<li>Ліквідація пігментації</li>\r\n	<li>Ліквідація запальних елементів на обличчі</li>\r\n</ul>\r\n\r\n<p>По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, розгладження зморшок, очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.<br />\r\nПісля проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівши клітиночок, які заважають шкірі дихати і ваша шкіра відчуває легкість, сяяння та бархатистість.</p>\r\n','Закупорка пор','',''),(40,NULL,1,'ru','Єсипчук Оксана Василівна','Керівник та головний лікар','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n','','',''),(41,NULL,1,'en','Єсипчук Оксана Василівна','Керівник та головний лікар','<p>Демонстрационная, то и смысловую нагрузку. Фразы и по сей день впервые. Книгопечатании еще в книгопечатании еще в. Значительно различается языке, который планируется использовать при оценке качества восприятия макета того. Пределах добра и проектах, ориентированных на. Известным рыбным текстом является знаменитый lorem является знаменитый lorem ipsum обязан. Для вставки на кириллический контент &ndash; написание символов на кириллице значительно.</p>\r\n\r\n<p>Интернет-страницы и по сей день кириллице значительно различается нечитабельность текста. Его применили в качестве рыбы текст. Встречаются с разной частотой, имеется разница в качестве рыбы текст. Впервые его применили в xvi веке ipsum кроме. Напрашивается вывод, что впервые его применили в различных языках.</p>\r\n','','',''),(42,NULL,6,'ru','мсиисми','','<p>24/09/2015 до Косметологічного центру Naturel завітали представники мужньої статі. Амбіційні, рішучі та відомі в нашу місті гравці Футбольного клубу Буковина на чолі з головним тренером Мглинцем Віктором Івановичем!</p>\r\n\r\n<p>Керівництво Футбольного клубу Буковина піклується про успішні результати гри і здоров&rsquo;я своїх майстрів спорту, тому візити в Naturel є невід&rsquo;ємною частиною побудови правильної стратегії відновлюю чого процесу Футбольної команди, адже самі найскладніші методики релаксу та процедур що повертають втрачені сили - в Naturel перевтілюються у легку і комфортну &laquo;гру&raquo; із своїми правилами і переможними моментами!</p>\r\n\r\n<p>В Косметологічному центрі Naturel гравці Футбольного клубу Буковина проходили ряд процедур по відновленню м&rsquo;язів після складних футбольних навантажень, оздоровленню проблемних стоп а також усунення втоми як фізичного так і морального характеру.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Для кожного гравця була обрана індивідуальна методика по усуненню необхідних проблем розроблена на основі кваліфікованої діагностики лікарів Косметологічного центру Naturel.</p>\r\n\r\n<p>Проблеми зі стопами, які виникають у футболістів під час напруженої гри допоміг вирішити професійний подолог. Було усунуто глибокі мозолі, які здійснювали високий тиск та несли н</p>\r\n','смисмис','',''),(43,NULL,6,'en','мсиисми','','<p>24/09/2015 до Косметологічного центру Naturel завітали представники мужньої статі. Амбіційні, рішучі та відомі в нашу місті гравці Футбольного клубу Буковина на чолі з головним тренером Мглинцем Віктором Івановичем!</p>\r\n\r\n<p>Керівництво Футбольного клубу Буковина піклується про успішні результати гри і здоров&rsquo;я своїх майстрів спорту, тому візити в Naturel є невід&rsquo;ємною частиною побудови правильної стратегії відновлюю чого процесу Футбольної команди, адже самі найскладніші методики релаксу та процедур що повертають втрачені сили - в Naturel перевтілюються у легку і комфортну &laquo;гру&raquo; із своїми правилами і переможними моментами!</p>\r\n\r\n<p>В Косметологічному центрі Naturel гравці Футбольного клубу Буковина проходили ряд процедур по відновленню м&rsquo;язів після складних футбольних навантажень, оздоровленню проблемних стоп а також усунення втоми як фізичного так і морального характеру.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Для кожного гравця була обрана індивідуальна методика по усуненню необхідних проблем розроблена на основі кваліфікованої діагностики лікарів Косметологічного центру Naturel.</p>\r\n\r\n<p>Проблеми зі стопами, які виникають у футболістів під час напруженої гри допоміг вирішити професійний подолог. Було усунуто глибокі мозолі, які здійснювали високий тиск та несли н</p>\r\n','смисмис','',''),(44,NULL,7,'ru','Закупорка пор','','','','',''),(45,NULL,7,'en','Blockage cf.','','','','',''),(46,NULL,8,'uk','Пігментація','','<ul>\r\n	<li><u>Exilite</u> &ndash; це лазерна система, яка основана на дії імпульсів високоефективного випромінювання, що потрапляє на поверхню шкіри, руйнуючи меланін. Дана апаратна методика є єдиною в Укарїні та представлена лише в Косметологічному центрі Naturel . Завдяки цьому процесу, ділянки з темними плямами спочатку темніють а потім відлущуються природнім шляхом. Дана процедура не висвітлює плями та не перетворює їх в білі, вони просто зникають. Вже після першого сеансу пігментація починає зникати, і після чого ви бачите очищену сяючу чисту шкіру, без будь-яких дефектів.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>DMK</u> &ndash; Професійна косметика DMK, ексклюзив Косметологічного центру &nbsp;Naturel (Danne) існує на ринку вже 45 років і є світовим лідером в корекції проблем шкіри, як косметологічних так і ряду дерматологічних.</li>\r\n</ul>\r\n\r\n<p>Її засновник Данне Монтегю-Кінг (<em>Dann</em><em>&eacute; </em><em>Montague</em><em>-</em><em>King</em>)доктор біохімік запропонував чітку концепцію роботи зі шкірою, яка містить наступні чотири кроки: ліквідувати, відновити, захистити та підтримати.</p>\r\n\r\n<p>Процедура Melanoplex (Proezym і Enzyme для лікування гіперпігментації) &ndash; концепція лікувальної методики DMK дає можливість ліквідувати старий надлишковий меланін, який знаходиться в верхніх шарах епідерміса.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Гідромеханопілінг</u> &ndash; (улюблений апарат Мадонни) &ndash; дана процедура є дуже популярною в Косметологічному центрі Naturel &nbsp;Апаратна процедура гідромеханопілінгу, полягає в одночасній шліфовці шкіри з поданням &nbsp;на неї спеціальної сировитки.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">Даний апарат представляє собою герметичну вакуумно-проточну систему з робочою абразивною насадкою, виконаною із алмазу. В апарат під&rsquo;єднується спеціальна лікувальна сироватка, яка під час мікрошліфування поверхні шкіри вводится з допомогою тиску у верхні шари шкіри. Сироватка підбирається в залежності від проблеми, а саме:</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація зморшок</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація пігментації</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація запальних елементів на обличчі</p>\r\n\r\n<p>По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, розгладження зморшок, очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, &nbsp;шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.</p>\r\n\r\n<p>Після проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому &nbsp;знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівших клітиночок, які заважали шкірі дихати. Після чого зявляється &nbsp;відчуття легкості, сяяння та бархатистості шкіри.</p>\r\n','','',''),(47,NULL,8,'ru','','','','','',''),(48,NULL,8,'en','','','','','',''),(49,NULL,9,'uk','Рубці, постакне','','<p style=\"margin-left:18.0pt;\">&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Exilite</u> &ndash; це лазерна система, яка основана на дії імпульсів високоефективного випромінювання по лукуванню рубців від акне. Дана апаратна методика є єдиною в Укарїні та представлена лише в Косметологічному центрі Naturel Світлова енергія поглинає продукти метаболізму піогенних бактерій ,які &nbsp;спонукають розвитку акне на шкірі і внаслідок потрапляння потужної енергії відбувається руйнування бактерій ,які гинуть щоб не провокують запалення і джерело інфекції ліквідовано <strong>Процес відбувається </strong>&nbsp;<strong>без порушення поверхні шкіри </strong>.Результат виражається в зменшенні виділення підшкірного жиру ,ліквідації запалення і звуження пор .В подальшому ліквідовується постзапальна пігментація ,червоні плями,рубці, що завершує процес лікування вугрових висипів. Не потребується додаткох процедур по вирівнюванню кольору шкіри після хвороби акне.Обличчя набуває однорідний здоровий колір, шкіра стає більш м&rsquo;якою, ніжною. Під час лікування апаратною системою EXILITE немає потреби використовувати додаткові процедури по вирівнюванню кольору шкіри після захворювання акне .</li>\r\n</ul>\r\n\r\n<p align=\"center\">&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Гідромеханопілінг</u> &ndash; (улюблений апарат Мадонни) &ndash; дана процедура є дуже популярною в Косметологічному центрі Naturel &nbsp;Апаратна процедура гідромеханопілінгу, полягає в одночасній шліфовці шкіри з поданням &nbsp;на неї спеціальної сировитки.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">Даний апарат представляє собою герметичну вакуумно-проточну систему з робочою абразивною насадкою, виконаною із алмазу. В апарат під&rsquo;єднується спеціальна лікувальна сироватка, яка під час мікрошліфування поверхні шкіри вводится з допомогою тиску у верхні шари шкіри. Сироватка підбирається в залежності від проблеми, а саме:</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація зморшок</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація пігментації</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація запальних елементів на обличчі</p>\r\n\r\n<p>По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, розгладження зморшок, очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, &nbsp;шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.</p>\r\n\r\n<p>Після проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому &nbsp;знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівших клітиночок, які заважали шкірі дихати. Після чого зявляється &nbsp;відчуття легкості, сяяння та бархатистості шкіри.</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">Ліквідація запальних елементів на обличчі</p>\r\n\r\n<p>По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, &nbsp;очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, &nbsp;шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.</p>\r\n\r\n<p>Після проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому &nbsp;знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівши клітиночок, які заважають шкірі дихати і ваша шкіра відчуває легкість, сяяння та бархатистість.</p>\r\n','','',''),(50,NULL,9,'ru','','','','','',''),(51,NULL,9,'en','','','','','',''),(52,NULL,10,'uk','Набряки','','<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Ендермологія (Лімфодренажний масаж) </u></li>\r\n</ul>\r\n\r\n<p>Апаратна методика, що представлена в Косметологічному центрі Naturel, яка направлена на позбавлення набряків &nbsp;на обличчі, під очима ,над верхньою повікою ,зменьшення другого підборіддя, &nbsp;відновлення пружності.</p>\r\n\r\n<p>Погіршення зовнішнього вигляду шкіри з віком пов&#39;язано з тим, що в її капілярах порушується мікроциркуляція крові і синтез еластину і колагену - білків, що відповідають за пружність. У результаті з&#39;являється в&#39;ялість, зморшки, втрачається здатність до відновлення після деформації.</p>\r\n\r\n<p>Ліфт-масаж обличчя надає механічний вплив на глибокі шари шкіри і жирової клітковини, що робить позитивний вплив на її стан і зовнішній вигляд.</p>\r\n\r\n<p>Під час процедури &nbsp;шкіра проробляється, так званою, маніпулою- спеціальною насадкою. За допомогою вакууму ділянка шкіри злегка всмоктуюється і пропрацьовується кожний міліметр на обличчі.</p>\r\n\r\n<p>Така процедура сприяє виведенню надлишку рідини з шкіри і стимулює обмін речовин. У результаті навіть після одного сеансу зникають набряки, провисання і дрібні зморшки. Зазвичай такий апаратний масаж обличчя проводять із 10 процедур, повторюючи їх по два рази на тиждень. Його неможливо проводить в домашніх умовах, і для проходження процедури необхідно звернутися в Косметологічний центр Naturel, де проводять дану процедуру.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Гідромеханопілінг</u> &ndash; (улюблений апарат Мадонни) &ndash; дана процедура є дуже популярною в Косметологічному центрі Naturel &nbsp;Апаратна процедура гідромеханопілінгу, полягає в одночасній шліфовці шкіри з поданням &nbsp;на неї спеціальної сировитки.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">Даний апарат представляє собою герметичну вакуумно-проточну систему з робочою абразивною насадкою, виконаною із алмазу. В апарат під&rsquo;єднується спеціальна лікувальна сироватка, яка під час мікрошліфування поверхні шкіри вводится з допомогою тиску у верхні шари шкіри. Сироватка підбирається в залежності від проблеми, а саме:</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація зморшок</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація пігментації</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">- Ліквідація запальних елементів на обличчі</p>\r\n\r\n<p>По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, розгладження зморшок, очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, &nbsp;шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.</p>\r\n\r\n<p>Після проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому &nbsp;знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівших клітиночок, які заважали шкірі дихати. Після чого зявляється &nbsp;відчуття легкості, сяяння та бархатистості шкіри.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>RF &ndash; процедура Радіо-ліфтингу Косметологічного центру Naturel.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\">Апаратна методика яка відновлює пружність і сприяє активізації природних процесів, в основі яких лежить вироблення колагену і еластину. Ліфтинг сприяє прогріванню верхніх шарів шкіри, під час якого відбувається скорочення переростянуного гологену і відновлюється пружність і за рахунок такої діі колагенові волокна здавлюють грижові мішки в якіх застоюється рідина і тим самим не має випячування грижового мішка тому що волагенові валокна його стримують.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Ericson &nbsp;- Ексклюзивна косметика Косметологічного центру Naturel.</li>\r\n</ul>\r\n\r\n<p style=\"margin-left:18.0pt;\"><u>BI</u><u>-</u><u>PATCH</u><u> лімфодренажні маски</u>.Маска бі-петчі для очей стимулює венозну кров ,покращеє мікроциркуляцью .Ліквідовує набряки ,мішки і темні кола під очима.Зморшки навколо очей стають менші помітні, активно протидіє старінню, зовнішній вигляд стає свіжим ,насичуються тканини киснем ,відновлюється нормальний рівень гидратаціі .</p>\r\n','','',''),(53,NULL,10,'ru','','','','','',''),(54,NULL,10,'en','','','','','',''),(55,NULL,11,'uk','Темні кола навколо очей','','<p align=\"center\" style=\"margin-left:18.0pt;\">&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Ендермологія (Лімфодренажний масаж) </u></li>\r\n</ul>\r\n\r\n<p>Ендермологія -&nbsp;&nbsp;&nbsp; (Лімфодренажний масаж) Апаратна методика яка представлена в Косметологічному центрі Naturel та направлена на позбавлення набряків &nbsp;на обличчі, під очима ,над верхньою повікою ,зменьшення другого підборіддя, &nbsp;відновлення пружності.</p>\r\n\r\n<p>Погіршення зовнішнього вигляду шкіри з віком пов&#39;язано з тим, що в її капілярах порушується мікроциркуляція крові і синтез еластину і колагену - білків, що відповідають за пружність. У результаті з&#39;являється в&#39;ялість, зморшки, втрачається здатність до відновлення після деформації, ліфт-масаж обличчя надає механічний вплив на глибокі шари шкіри і жирової клітковини, що робить позитивний вплив на її стан і зовнішній вигляд.</p>\r\n\r\n<p>Під час процедури &nbsp;шкіра проробляється, так званою, маніпулою- спеціальною насадкою. За допомогою вакууму ділянка шкіри злегка всмоктуюється і пропрацьовується кожний міліметр на обличчі.</p>\r\n\r\n<p>Така процедура сприяє виведенню надлишку рідини з шкіри і стимулює обміну речовин. У результаті навіть після одного сеансу зникають набряки, провисання і дрібні зморшки. Зазвичай такий апаратний масаж обличчя проводять із 10 процедур, повторюючи їх по два рази на тиждень. Його неможливо проводить в домашніх умовах, і для проходження процедури необхідно звернутися в Косметологічному центрі Naturel.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Колагенова помпа</u></li>\r\n</ul>\r\n\r\n<p>Колагенова попма &ndash; нова процеда від ДМК для підсилення вироблення колагену і відновленню пружності. Колагенова помпа основана на насиченні шкіри максимальною кількості вітаміна &nbsp;С і запуску процесів активного виробництва волокон ,які забезпечують здоровий , молодий і підтягнутий вигляд .</p>\r\n\r\n<p>Вітамін С &ldquo;Бос фібробластів&rdquo; як його називає засновник марки Данне Монтегю Кінг. Без максимальної концентрації в шкірі вітаміну С процеси утворень колагену замітно знижуються. Крім цього вітамін с робить пружніми судни, покращує колір обличчя і є сильним антиоксидантом, який продидіє фотостарінню (старінню під дією ультрафіолету сонячного чи солярію).</p>\r\n\r\n<p>Такий високий концентрат, який реально знаходиться в препараті, не вдавалося отримати нікому з провідних виробників косметики. При нанаесенні на шкіру аплікатором кремнієві оболонки розтоплюються і лікувальний вітамін С 20% проникає в шкіру без втрати своєї активності. Таку високу концентрацію вітаміну С неможливо ввести в склад кремоподібних препаратів, за архунок того, що він денотує крем.</p>\r\n\r\n<p>Данне знайшов вихід - він створив препарат 20%-ого вітаміну С FiberBlast C &nbsp;у вигляді порошку, та вмістив його в кремнієву оболонку для захисту від окиснення.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"margin-left:18.0pt;\">Колагенова помпа - процедура Косметологічного центру Naturel, світової марки DANNE, для вироблення колагену та підтяжки.</p>\r\n\r\n<p style=\"margin-left:53.25pt;\">FiberBlast C &nbsp;процедура Косметологічного центру Naturel- ефективна для -</p>\r\n\r\n<ul>\r\n	<li>пдвищення пружності шкіри</li>\r\n	<li>покращення кольору - розширення судин</li>\r\n	<li>лікування куперозу - розширення судин</li>\r\n</ul>\r\n\r\n<p>Результат помітний одразу - підтянута, сяюча та молода шкіра.</p>\r\n\r\n<p>Після курсу Колагенової помпи результат буде ще більш виразним та довготривалим, вражаючим навіть для пластичних хірургів.</p>\r\n','','',''),(56,NULL,11,'ru','','','','','',''),(57,NULL,11,'en','','','','','',''),(58,NULL,12,'uk','Велосипеди ','','<p>Любители экстремального спорта могут покататься по горным дорогам на велосипеде. Это весёлый и увлекательный способ транспортировки на открытом воздухе, при этом Вы сполна сможете насладиться чудесными пейзажами Карпат.</p>\r\n','','',''),(59,NULL,12,'ru','Велосипеди ','','<p>Любители экстремального спорта могут покататься по горным дорогам на велосипеде. Это весёлый и увлекательный способ транспортировки на открытом воздухе, при этом Вы сполна сможете насладиться чудесными пейзажами Карпат.</p>\r\n','','',''),(60,NULL,12,'en','Велосипеди ','','','','',''),(61,NULL,13,'uk','Рішення проблеми','Опис','<p>Тарара</p>\r\n','','',''),(62,NULL,13,'ru','','','','','',''),(63,NULL,13,'en','','','','','',''),(64,NULL,14,'uk','Exilite','це лазерна система, яка основана на дії імпульсів високоефективного випромінювання.','<p>Exilite &ndash; це лазерна система, яка основана на дії імпульсів високоефективного випромінювання. Дана апаратна методика є єдиною в Укарїні та представлена лише в Косметологічному центрі Naturel . За допомогою потужної світлової енергії відбувається нагрів та склеювання судин, в результаті чого судини стають прозорими і абсолютно не помітними під шкірою.</p>\r\n','','',''),(65,NULL,14,'ru','','','','','',''),(66,NULL,14,'en','','','','','',''),(67,NULL,15,'uk','Професійна косметика DMK','ексклюзив Косметологічного центру  Naturel','<p>Професійна косметика DMK, ексклюзив Косметологічного центру &nbsp;Naturel (Danne) існує на ринку вже 45 років і є світовим лідером в корекції проблем шкіри, як косметологічних так і ряду дерматологічних.<br />\r\nЇї засновник Данне Монтегю-Кінг (Dann&eacute; Montague-King)доктор біохімік запропонував чітку концепцію роботи зі шкірою, яка містить наступні чотири кроки: ліквідувати, відновити, захистити та підтримати.</p>\r\n\r\n<p>Процедури Danne &nbsp;направлені на тренування судин, покращення кровообігу, проліферацію клітин та потовщення верхнього шару шкіри.</p>\r\n\r\n<p>&laquo;Тренування судин, покращення кровообігу та потовщення верхнього шару шкіри&raquo;</p>\r\n','','',''),(68,NULL,15,'ru','','','','','',''),(69,NULL,15,'en','','','','','',''),(70,NULL,16,'uk','Гідромеханопілінг','','<p>Гідромеханопілінг &ndash; (улюблений апарат Мадонни) &ndash; дана процедура є дуже популярною в Косметологічному центрі Naturel &nbsp;Апаратна процедура гідромеханопілінгу, полягає в одночасній шліфовці шкіри з поданням &nbsp;на неї спеціальної сировитки.<br />\r\nДаний апарат представляє собою герметичну вакуумно-проточну систему з робочою абразивною насадкою, виконаною із алмазу. В апарат під&rsquo;єднується спеціальна лікувальна сироватка, яка під час мікрошліфування поверхні шкіри вводится з допомогою тиску у верхні шари шкіри. Сироватка підбирається в залежності від проблеми, а саме:</p>\r\n\r\n<p>- Ліквідація зморшок</p>\r\n\r\n<p>- Ліквідація пігментації</p>\r\n\r\n<p>- Ліквідація запальних елементів на обличчі</p>\r\n\r\n<p>По ефективності процедура має наступні переваги: помітний результат вже після першої процедури, розгладження зморшок, очищення шкіри і пор на ній, ліквідація запальних процесів та висипань на шкірі, відновлення водного балансу у внутрішніх шарах шкіри, &nbsp;шліфовка шкіри, покращення кольору шкіри, та набуття шовковистого стану.</p>\r\n\r\n<p>Після проведення процедури, кожен клієнт може звернути увагу на прозорий резервуар, який знаходиться в апараті, в якому &nbsp;знаходяться всі забруднення, які були на шкірі, комедони, сухі лусочки ороговівших клітиночок, які заважали шкірі дихати. Після чого зявляється &nbsp;відчуття легкості, сяяння та бархатистості шкіри.</p>\r\n','','',''),(71,NULL,16,'ru','','','','','',''),(72,NULL,16,'en','','','','','',''),(73,22,NULL,'uk',' Лыжи и сноуборд','','','',' В нашем комплексе «Долина Николая» есть лыжная мини – трасса, длиною 150 метров на которой вы получите массу удовольствия, катаясь на лыжах, сноубордах, сноутюбах и санках. Для продвинутых лыжников в 10 минутах от нашего комплекса ДН расположен горнолыжный комплекс Мигово. Тут прекрасные лыжные трассы для разной сложности. Для начинающих есть лыжная школа, и тренировочные горки. Снег тут выпадает рано, и лежит он долго. Кстати, убедиться, что на склонах достаточно снега, можно через веб-камеру Мигово, установленную возле лыжной трассы.',''),(74,22,NULL,'ru',' Лыжи и сноуборд','','','','',''),(75,22,NULL,'en',' Лыжи и сноуборд','','','','',''),(76,23,NULL,'uk','Баня','','','','Рекомендуем Вам посетить нашу Баню на Дровах в коттеджном комплексе Долина Николая. Она сделана с натуральной горной липы по Финской технологии, которая придает неповторимый и натуральный аромат. Баня помогает отвлечься от всех проблем, снять длительною усталость и при этом дает мощный импульс положительных эмоций, прекрасного самочувствия, хорошего настроения. А это неиссякаемый источник здоровья',''),(77,23,NULL,'ru','Баня','','','','',''),(78,23,NULL,'en','Баня','','','','',''),(79,24,NULL,'uk','Басейн','','','','Индивидуальный открытий бассейн для тех, кто проживает в нашем коттедже в Мигово. Вы получите массу эмоций и удовольствия. (7.3 на 3.6 м.)',''),(80,24,NULL,'ru','Басейн','','','','',''),(81,24,NULL,'en','Басейн','','','','','');
/*!40000 ALTER TABLE `services_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `weight` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `media_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` VALUES (1,1,1,39),(2,0,1,38),(3,2,0,NULL),(4,3,1,NULL),(5,4,1,NULL);
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_texts`
--

DROP TABLE IF EXISTS `slider_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slide_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_texts`
--

LOCK TABLES `slider_texts` WRITE;
/*!40000 ALTER TABLE `slider_texts` DISABLE KEYS */;
INSERT INTO `slider_texts` VALUES (1,2,'uk','Косметологічний центр «Naturel» — косметологія обличчя та тіла','«Шукати красу всюди, де тільки можна її знайти і дарувати її тим, хто поруч з тобою»'),(2,2,'ru','Косметологический центр «Naturel» - косметология лица и тела','\"Искать красоту везде, где только можно ее найти и дарить ее тем, кто рядом с тобой»'),(3,2,'en','Beauty Center «Naturel» - cosmetology Face and body','\"Find the beauty wherever one can find it and give it to those who are with you\"'),(4,3,'uk','',''),(5,3,'ru','',''),(6,3,'en','',''),(7,1,'uk','Косметологічний центр «Naturel» — косметологія обличчя та тіла','«Шукати красу всюди, де тільки можна її знайти і дарувати її тим, хто поруч з тобою»'),(8,1,'ru','',''),(9,1,'en','','');
/*!40000 ALTER TABLE `slider_texts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

