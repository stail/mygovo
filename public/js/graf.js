var seen = false;
$(".dial").knob();
$(".dial2").knob();
$(".dial3").knob();

$(window).scroll(function () {
    var offsetRow = jQuery('.row-achievement').offset();
    var row = offsetRow.top;

    if ((seen == false) && ($(this).scrollTop() >= row - 500)) {

        $(".dial").knob();
        $({
            animatedVal: 0
        }).animate({
            animatedVal: 25
        }, {
            duration: 2000,
            easing: "swing",
            step: function () {
                $(".dial").val(Math.ceil(this.animatedVal)).trigger("change");
            }
        });
        $(".dial2").knob();
        $({
            animatedVal: 0
        }).animate({
            animatedVal: 75
        }, {
            duration: 2000,
            easing: "swing",
            step: function () {
                $(".dial2").val(Math.ceil(this.animatedVal)).trigger("change");
            }
        });
        $(".dial3").knob();
        $({
            animatedVal: 0
        }).animate({
            animatedVal: 35
        }, {
            duration: 2000,
            easing: "swing",
            step: function () {
                $(".dial3").val(Math.ceil(this.animatedVal)).trigger("change");
            }
        });
        $(".item-num").fadeIn(1500);
        seen = true;
    }
});