function initialize() {
    var mapCanvas = document.getElementById('map');
    var mapOptions = {
        center: new google.maps.LatLng(48.2797000, 25.9311111),
        zoom: 19,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);

    setMarker(map);
}

function setMarker(map) {
    var latlngbounds = new google.maps.LatLngBounds();
    var image = new google.maps.MarkerImage('../img/icons/mitka.png',
        new google.maps.Size(39, 53),
        new google.maps.Point(0, 0),
        new google.maps.Point(0, 53));
    var myLatLng = new google.maps.LatLng(48.279630, 25.930250);
    latlngbounds.extend(myLatLng);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image,
        title: '"Naturel"\n вул. Запорізька 1',
    });
};
google.maps.event.addDomListener(window, 'load', initialize);