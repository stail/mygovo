jQuery(function ($) {

    $('.phone-mask').mask('+38 (999) 999-99-99');

    var callMe1 = $('#popup-form-1');
    var callMe2 = $('#popup-form-2');
    var callMe3 = $('#popup-form-3');

    renderList(callMe1);
    renderList(callMe2);
    renderList(callMe3);

    callMe1.validate({
        ignore: "",
        focusInvalid: false,
        rules: {
            "popup[name]": {
                required: true
            },
            "popup[phone]": {
                required: true
            },
            "popup[email]": {
                required: true
            }
        },
        highlight: function (element) {
            renderList(callMe1);
        },
        unhighlight: function (element) {
            renderList(callMe1);
        },
        errorPlacement: function (error, element) {
            return true;
        },
        submitHandler: function (form) {
            $.ajax({
                url: $(form).attr('action'),
                method: 'POST',
                data: $(form).serialize(),
                success: function (data) {
                    $(form).parents('.popup-send-form').find('.popup-thanks').show();
                    $(form)[0].reset();
                }
            });

            return false;
        }
    });

    callMe2.validate({
        ignore: "",
        focusInvalid: false,
        rules: {
            "popup[name]": {
                required: true
            },
            "popup[phone]": {
                required: true
            },
            "popup[email]": {
                required: true
            }
        },
        highlight: function (element) {
            renderList(callMe2);
        },
        unhighlight: function (element) {
            renderList(callMe2);
        },
        errorPlacement: function (error, element) {
            return true;
        },
        submitHandler: function (form) {
            $.ajax({
                url: $(form).attr('action'),
                method: 'POST',
                data: $(form).serialize(),
                success: function (data) {
                    $(form).parents('.popup-send-form').find('.popup-thanks').show();
                    $(form)[0].reset();
                }
            });

            return false;
        }
    });

    callMe3.validate({
        ignore: "",
        focusInvalid: false,
        rules: {
            "popup[name]": {
                required: true
            },
            "popup[phone]": {
                required: true
            },
            "popup[email]": {
                required: true
            }
        },
        highlight: function (element) {
            renderList(callMe3);
        },
        unhighlight: function (element) {
            renderList(callMe3);
        },
        errorPlacement: function (error, element) {
            return true;
        },
        submitHandler: function (form) {
            $.ajax({
                url: $(form).attr('action'),
                method: 'POST',
                data: $(form).serialize(),
                success: function (data) {
                    $(form).parents('.popup-send-form').find('.popup-thanks').show();
                    $(form)[0].reset();
                }
            });

            return false;
        }
    });

    callMe1.on('keyup change', '.required', function () {
        renderList(callMe1);
    });
    callMe2.on('keyup change', '.required', function () {
        renderList(callMe2);
    });
    callMe3.on('keyup change', '.required', function () {
        renderList(callMe3);
    });


    /*$('.required', callMe2).keyup(function () {
        renderList(callMe2);
    });

    $('.required', callMe3).keyup(function () {
        renderList(callMe3);
    });*/

    function renderList(_Form) {
        var _Input = $('.required', _Form),
            _Data = [],
            _Modal = _Form.parents('.popup-send-form'),
            _Container = $('.help .list', _Modal),
            _Valid = true;

        _Input.each(function () {
            var _Value = $.trim($(this).val());

            if (_Value == '' || _Value.indexOf('_') >= 0 || ($(this).attr('name') == 'popup[email]' && !isValidEmail(_Value))) {
                var _Text = '<span class="dummies" data-target="' + $(this).attr('name') + '">' + $(this).data('name') + '</span>';
                _Data.push(_Text);
                _Valid = false;
            }
        });

        if (_Valid) {
            $('[type="submit"]', _Form).attr('disabled', false);
            $('[type="submit"]', _Form).removeClass('no-active');
            _Container.parent('.help').hide();
        } else {
            $('[type="submit"]', _Form).attr('disabled', true);
            $('[type="submit"]', _Form).addClass('no-active');
            _Container.parent('.help').show();
        }

        _Container.html(_Data.join('<span>,</span>'));
    }

    $('.show-modal').click(function () {
        var modal = $(this).data('target'),
            text = $(this).data('text'),
            $modal = $(modal);
        $modal.find('.content').html(text);

        $('#wrap').fadeIn(200);
        $modal.fadeIn(200);
        disableScroll();

        return false;
    });

    $('.close').click(function () {
        $('.popup-send-form').fadeOut(200);
        $('#wrap').fadeOut(200);
        enableScroll();

        return false;
    });

    $('.show-order').click(function () {
        var _title = $(this).data('title');

        $('[name="popup[title]"]').val(_title);

        $("#wrap").fadeIn(200);
        $("#popup-send-form-order").fadeIn(200);
    });

    $('.popup-send-form').on('click', 'span.dummies', function () {
        var _target = $(this).data('target');

        console.log(_target);

        $('[name="' + _target + '"]').focus();
    });
});

function isValidEmail(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
}