$(document).ready(function () {
    var tel = false;
    $('#tel_id').focus(function () {
        if (this.value == false) {
            this.value = '+(380) ';
            tel = true;
        }
    });
});

function checkPoints(value) {
    var point_show_1 = '#point1';
    var point_show_2 = '#point2';

    if (value == "000") {
        $(point_show_1).css("display", "");
        $(point_show_2).css("display", "");
    }
    if (value == "001") {
        $(point_show_1).css("display", "");
        $(point_show_2).css("display", "none");
    }
    if (value == "010") {
        $('#point1').css("display", "");
        $('#point2').css("display", "none");
    }
    if (value == "011") {
        $(point_show_1).css("display", "none");
        $(point_show_2).css("display", "none");
    }
    if (value == "100") {
        $(point_show_1).css("display", "none");
        $(point_show_2).css("display", "");
    }
    if (value == "101") {
        $(point_show_1).css("display", "none");
        $(point_show_2).css("display", "none");
    }
    if (value == "110") {
        $(point_show_1).css("display", "none");
        $(point_show_2).css("display", "none");
    }
    if (value == "111") {
        $(point_show_1).css("display", "none");
        $(point_show_2).css("display", "none");
    }
}

function checkFields(num) {
    if (num == '1') {
        $('#fio_id').focus();
    } else
    if (num == '2') {
        $('#tel_id').focus();
    } else
    if (num == '3') {
        $('#mail_id').focus();
    }
}

function CountFio(item) {
    var item_show = '#dummies_fio';
    if (document.getElementById(item).value.length >= 4) {
        $(item_show).css("display", "none");
        document.getElementById('check_fio').value = 1;
    } else {
        $(item_show).css("display", "");
        document.getElementById('check_fio').value = 0;
    }
    checkAll();
}


function CountTel(item) {
    var item_show = '#dummies_tel';
    if (document.getElementById(item).value.length >= 15) {
        $(item_show).css("display", "none");
        document.getElementById('check_tel').value = 1;
    } else {
        $(item_show).css("display", "");
        document.getElementById('check_tel').value = 0;
    }
    checkAll();
}

function isEmail(item) {
    var at = "@"
    var dot = "."
    var lat = item.indexOf(at)
    var litem = item.length
    var ldot = item.indexOf(dot)
    if (item.indexOf(at) == -1) return false;
    if (item.indexOf(at) == -1 || item.indexOf(at) == 0 || item.indexOf(at) == litem) return false;
    if (item.indexOf(dot) == -1 || item.indexOf(dot) == 0 || item.indexOf(dot) >= litem - 2) return false;
    if (item.indexOf(at, (lat + 1)) != -1) return false;
    if (item.substring(lat - 1, lat) == dot || item.substring(lat + 1, lat + 2) == dot) return false;
    if (item.indexOf(dot, (lat + 2)) == -1) return false;
    if (item.indexOf(" ") != -1) return false;
    return true
}

function CorrectEmail(item) {
    var item_show = '#dummies_mail';
    if (isEmail(item.value) == true) {
        $(item_show).css("display", "none");
        document.getElementById('check_mail').value = 1;
    } else {
        $(item_show).css("display", "");
        document.getElementById('check_mail').value = 0;
    }
    checkAll();
}

function checkAll() {
    var x;
    var item_show = '#for_dummies';
    var check_fio = document.getElementById('check_fio').value;
    var check_tel = document.getElementById('check_tel').value;
    var check_mail = document.getElementById('check_mail').value;
    x = check_fio + check_tel + check_mail;
    document.getElementById('check_all').value = x;
    checkPoints(x.toString());
    if (document.getElementById('check_all').value == 111) {
        document.getElementById('submit_id').disabled = false;
        document.getElementById('submit_id').classList.remove('no-active');
        $(item_show).css("display", "none");
    } else {
        document.getElementById('submit_id').disabled = true;
        document.getElementById('submit_id').classList.add('no-active');
        $(item_show).css("display", "-webkit-inline-box");
    }
}

$('#tab1').bind('click', changeTabsBack);
$('#tab2').bind('click', changeTabs);

function changeTabs() {
    $(".tab-container-1").fadeOut(300);
    $(".tab-container-2").delay(300).fadeIn(300);
    $('#tab1').removeClass("tab-active");
    $('#tab2').addClass("tab-active");
    
}
function changeTabsBack() {
    $(".tab-container-2").fadeOut(300);
    $(".tab-container-1").delay(300).fadeIn(300);
    $('#tab2').removeClass("tab-active");
    $('#tab1').addClass("tab-active");
    
}

$('.filter-btn').bind('click', openFilter);

var filterOpen = false;

function openFilter() {
    if (filterOpen == false) {
        $('.filter-btn').addClass("active");
        $('.filter-list').addClass("active");
        filterOpen = true;
    } 
    else {
        $('.filter-btn').removeClass("active");
        $('.filter-list').removeClass("active");
        filterOpen = false;        
    }    
}

jQuery(function($) {
    $('body').on('click', '.filter-link', function() {
        $('.filter-link').removeClass('active');
        $(this).addClass('active');        
        $('.catalog-row').fadeOut(700).fadeIn(700);
    });
});
