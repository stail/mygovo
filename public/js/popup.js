$('#close').bind('click', close);
$('.close').bind('click', close);
$('#close-order').bind('click', closeOrder);
$('.close-video').bind('click', closeVideo);
$('#wrap').bind('click', close);
$('.thanks-close').bind('click', closeThanks);
$('.nav-btn-open').bind('click', showNav);
$('.nav-btn-close').bind('click', closeNav);

function showNav() {
    $(".nav-field").fadeIn(200);
    $(".nav-btn-open").fadeOut(200);
    $(".nav-btn-close").delay(400).fadeIn(200);
    $(".nav-field").addClass("open");
    $(".row").addClass("blur");    
    //disableScroll();
};
function closeNav() {
    $(".nav-field").fadeOut(200);
    $(".nav-btn-close").fadeOut(200);
    $(".nav-btn-open").delay(200).fadeIn(200);
    $(".nav-field").removeClass("open");
    $(".row").removeClass("blur"); 
    //enableScroll();
};

function show() {
    $("#wrap").fadeIn(200);
    $("#popup-send-form").fadeIn(200);
    disableScroll();
};

function showOrder() {
    $("#wrap").fadeIn(200);
    $("#popup-send-form-order").fadeIn(200);
    disableScroll();
};

function showVideo() {
    $("#popup-video").fadeIn(200);
    disableScroll();
};

function close() {
    $("#popup-send-form").fadeOut(200);
    $("#wrap").fadeOut(200);
    enableScroll();
};
function closeOrder() {
    $("#popup-send-form-order").fadeOut(200);
    $("#wrap").fadeOut(200);
    enableScroll();
};

function closeThanks() {
    $(".popup-send-form").fadeOut(200);
    $("#wrap").fadeOut(200);
    $(".popup-thanks").fadeOut(200);
    clearFields();
    enableScroll();
};

function closeVideo() {
    $("#popup-video").fadeOut(200);
    $('iframe').attr('src', '');
    $('iframe').attr('src', 'https://www.youtube.com/embed/mi0D-ooVh48');
    enableScroll();
};

function clearFields() {
    $('#fio_id').val('');
    $('#tel_id').val('');
    $('#mail_id').val('');
    $('#dummies_fio').css("display", "");
    $('#dummies_tel').css("display", "");
    $('#dummies_mail').css("display", "");
    $("#point1").css("display", "");
    $("#point2").css("display", "");
    document.getElementById('submit_id').disabled = true;
    $('#submit_id').addClass("no-active");
    $("#for_dummies").css("display", "-webkit-inline-box");
    document.getElementById('check_fio').value = 0;
    document.getElementById('check_tel').value = 0;
    document.getElementById('check_mail').value = 0;
    document.getElementById('check_all').value = 0;
};

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
};

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}
