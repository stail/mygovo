<?php

$lang = null;
$locale = Request::segment(1);
if (in_array($locale, Config::get('app.locales'))) {
    \Lang::setLocale($locale);
    \Carbon\Carbon::setLocale($locale);
    $lang = $locale;
}

Route::group(
    ['prefix' => $lang, 'namespace' => 'Modules\\Mygovo\\Http\\Controllers'],
    function (\Illuminate\Routing\Router $router) {

        $router->group(
            [
                'prefix' => 'services',
                'where'  => [
                    'id'           => '\d+',
                    'service'      => '\d+',
                    'slug'         => '[a-z0-9-_]+',
                    'service_slug' => '[a-z0-9-_]+',
                    'category'     => '[a-z0-9-_]+'
                ]
            ],
            function (\Illuminate\Routing\Router $router) {
                $router->get('{category}/{service}{service_slug?}', 'MygovoController@service');
                $router->get('{id}{slug?}', 'MygovoController@services');
                $router->get('/', 'MygovoController@servicesMain');
            }
        );


        $router->group(
            [
                'prefix' => 'blog',
                'where'  => [
                    'id'   => '\d+',
                    'slug' => '[a-z0-9-_]+',
                ]
            ],
            function (\Illuminate\Routing\Router $router) {
                $router->get('{id}{slug?}', 'MygovoController@newsItem');
                $router->get('', 'MygovoController@news');
            }
        );

        $router->group(
            [
                'prefix' => 'catalog',
                'where'  => [
                    'id'           => '\d+',
                    'slug'         => '[a-z0-9-_]+',
                    'product_id'   => '\d+',
                    'product_slug' => '[a-z0-9-_]+'
                ]
            ],
            function (\Illuminate\Routing\Router $router) {
                $router->get('{id}{slug?}/{product_id}{product_slug?}', 'MygovoController@catalogProduct');
                $router->get('{id}{slug?}', 'MygovoController@catalogCategory');
                $router->get('', 'MygovoController@catalog');
            }
        );

        $router->get('certificates', ['as' => 'mygovo.certificates', 'uses' => 'MygovoController@certificates']);
        $router->get('contacts', ['as' => 'mygovo.contacts', 'uses' => 'MygovoController@contacts']);
        $router->post('submit', ['as' => 'mygovo.submit', 'uses' => 'MygovoController@submitForm']);
        $router->get('/', ['as' => 'mygovo.index', 'uses' => 'MygovoController@index']);
        //0$router->get('gallery', ['as' => 'mygovo.gallery', 'uses' => 'MygovoController@gallery']);
        $router->get('gallery', 'MygovoController@gallery');
    }
);