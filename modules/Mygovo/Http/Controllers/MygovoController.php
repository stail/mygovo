<?php namespace Modules\Mygovo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Modules\Backend\Models\Language;
use Modules\Mygovo\Instagram\Instagram;
use Modules\Mygovo\Models\Achievement;
use Modules\Mygovo\Models\Banner;
use Modules\Mygovo\Models\Category;
use Modules\Mygovo\Models\Certificate;
use Modules\Mygovo\Models\Gallery;
use Modules\Mygovo\Models\Lead;
use Modules\Mygovo\Models\Product;
use Modules\Mygovo\Models\Review;
use Modules\Mygovo\Models\Service as ServiceModel;
use Modules\Mygovo\Models\Service\Category as ServiceCategoryModel;
use Modules\Mygovo\Models\Slide;
use Modules\News\Models\News;
use Modules\Pages\Models\Page as PageModel;
use Illuminate\Routing\Controller;
use Modules\Partners\Models\Partner;

class MygovoController extends Controller
{
    public function __construct()
    {
        $languages = Language::query()
            ->where('active', '=', 1)
            ->get();

        $services = ServiceCategoryModel::query()
            ->with(['text', 'media'])
            ->where('active', '=', 1)
            ->orderBy('weight', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        \View::share('services', $services);
        \View::share('languages', $languages);
    }

    public function index()
    {
        $slider = Slide::query()
            ->whereNotNull('media_id')
            ->where('active', '=', 1)
            ->orderBy('weight', 'asc')
            ->get();

        $data = PageModel::query()
            ->where('key', '=', 'main')
            ->first();

        $screen_1 = PageModel::query()
            ->where('key', '=', 'screen_1')
            ->first();

        $contacts = PageModel::query()
            ->where('key', '=', 'contacts')
            ->first();

        $manager = Review::query()
            ->where('main', '=', 1)
            ->firstOrFail();

        $achievements = Achievement::query()
            ->orderBy('id', 'asc')
            ->get();

        $card = Certificate::query()
            ->where('type', '=', 'card')
            ->first();

        $certificates = Certificate::query()
            ->where('type', '=', 'certificate')
            ->get();

        $reviews = Review::query()
            ->where('main', '=', null)
            ->get();

        $news = News::query()
            ->where('active', '=', 1)
            ->orderBy('created_at', 'desc')
            ->limit(5)
            ->get();

        $banners = Banner::query()
            ->where('media_id', '<>', 0)
            ->orderBy('id', 'asc')
            ->get();

        $partners = Partner::query()
            ->orderBy('created_at', 'desc')
            ->get();

        $products = Product::query()
            ->orderByRaw("RAND()")
            ->limit(4)
            ->get();

        $instagram = Gallery::query()
            ->where('key', '=', 'instagram')
            ->first();

        $mainGallery = Gallery::query()
            ->where('key', '=', 'main')
            ->first();

        $mainGalleryImages = $mainGallery->images()->orderBy('id', 'asc')->get();

        return \View::make(
            'mygovo::index',
            [
                'data'         => $data,
                'screen_1'     => $screen_1,
                'manager'      => $manager,
                'achievements' => $achievements,
                'card'         => $card,
                'certificates' => $certificates,
                'reviews'      => $reviews,
                'news'         => $news,
                'banners'      => $banners,
                'partners'     => $partners,
                'contacts'     => $contacts,
                'products'     => $products,
                'instagram'    => $instagram,
                'slider' => $slider,
                'mainGalleryImages' => $mainGalleryImages
            ]
        );
    }

    public function servicesMain()
    {
        $data = PageModel::query()
            ->where('key', '=', 'services')
            ->first();

        return \View::make('mygovo::services-main', ['data' => $data]);
    }

    public function services(Route $route)
    {
        $data = ServiceCategoryModel::query()
            ->findOrFail($route->parameter('id'));

        return \View::make('mygovo::services', ['data' => $data]);
    }

    public function service(Route $route)
    {
        $data = ServiceModel::query()
            ->findOrFail($route->parameter('service'));

        $category = ($data->parent) ? $data->parent->category->id : $data->category->id;

        $services = ServiceModel::query()
            ->where('parent_id', '=', $category)
            ->orderByRaw('RAND()')
            ->get();

        return \View::make('mygovo::service', ['data' => $data, 'items' => $services]);
    }

    public function news()
    {
        $data = PageModel::query()
            ->where('key', '=', 'news')
            ->first();

        $news = News::query()
            ->where('active', '=', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return \View::make('mygovo::news', ['data' => $data, 'news' => $news]);
    }

    public function newsItem(Route $route)
    {
        $data = News::query()
            ->findOrFail($route->parameter('id'));

        return \View::make('mygovo::news-item', ['data' => $data]);
    }

    public function catalog()
    {
        $data = PageModel::query()
            ->where('key', '=', 'catalog')
            ->first();

        $categories = Category::query()
            ->where('active', '=', 1)
            ->orderBy('weight', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        $products = Product::query()
            ->where('active', '=', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return \View::make('mygovo::catalog', ['data' => $data, 'categories' => $categories, 'products' => $products]);
    }

    public function catalogCategory(Route $route)
    {
        $data = Category::query()
            ->findOrFail($route->parameter('id'));

        $active = $data->id;

        $categories = Category::query()
            ->where('active', '=', 1)
            ->orderBy('weight', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        $products = Product::query()
            ->where('active', '=', 1)
            ->where('category_id', '=', $data->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return \View::make(
            'mygovo::catalog',
            ['data' => $data, 'categories' => $categories, 'products' => $products, 'active' => $active]
        );
    }

    public function catalogProduct(Route $route)
    {
        $data = Product::query()
            ->findOrFail($route->parameter('product_id'));

        $products = Product::query()
            ->where('active', '=', 1)
            ->where('category_id', '=', $data->category->id)
            ->orderByRaw('RAND()')
            ->limit(5)
            ->get();

        return \View::make('mygovo::product', ['data' => $data, 'products' => $products]);
    }

    public function contacts(Route $route)
    {
        $data = PageModel::query()
            ->where('key', '=', 'contacts')
            ->first();

        return \View::make('mygovo::contacts', ['data' => $data]);
    }

    public function gallery(Route $route)
    {
        $gallery = Gallery::query()
            ->where('key', '=', 'instagram')
            ->first();
        $data = PageModel::query()
            ->where('key', '=', 'gallery')
            ->first();
        return \View::make('mygovo::gallery', ['data' => $data, 'gallery' => $gallery]);
    }

    public function certificates()
    {
        $data = PageModel::query()
            ->where('key', '=', 'certificates')
            ->first();

        $certificates = Gallery::query()
            ->where('key', '=', 'certificates')
            ->first();

        return \View::make('mygovo::certificates', ['data' => $data, 'certificates' => $certificates]);
    }

    public function submitForm(Request $request)
    {
        $data = $request->input('popup');

        if (array_key_exists('title', $data) && !empty($data['title'])) {
            $data['form'] .= '. Послуга - '.$data['title'];
        }

        Lead::create($data);

        \Mail::send('emails.request', $data, function ($message) use ($data) {
            /**
             * @var \Illuminate\Mail\Message $message
             */
            $message->from('info@mygovo.in.ua', 'Mygovo');
            $message->to('info@mygovo.in.ua')->subject($data['form']);
        });
    }
}