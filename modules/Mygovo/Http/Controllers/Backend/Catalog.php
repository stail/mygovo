<?php namespace Modules\Mygovo\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Mygovo\Models\Category;
use Modules\Mygovo\Models\CategoryText;

class Catalog extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['datatable']);
        Metronic::menu('catalog', 'categories');
    }

    public function getIndex()
    {
        return \View::make('mygovo::backend.catalog.categories.index');
    }

    public function postIndex(Request $request)
    {
        $model = Category::query();
        $recordsTotal = $recordsFiltered = $model->count();

        $model->limit($request->input('length', 25))
            ->offset($request->input('start', 0));

        $model->orderBy('created_at', 'desc');

        $collection = $model->get();

        $data = [];
        foreach ($collection as $item) {
            $data[] = [
                $item->text->caption,
                '<a href="'.route('backend.mygovo.catalog.status', ['id' => $item->id]).'" rel="item-status" class="btn btn-sm '.($item->active ? 'green' : 'red').'"><i class="fa '.($item->active ? 'fa-check' : 'fa-ban').'"></i></a>',
                '<a href="'.route('backend.mygovo.catalog.edit', ['id' => $item->id]).'" class="btn btn-sm green" title="Редагувати"><i class="fa fa-pencil"></i></a> '.
                '<a href="'.route('backend.mygovo.catalog.destroy', ['id' => $item->id]).'" rel="item-destroy" class="btn btn-sm red" title="Видалит"><i class="fa fa-trash-o"></i></a>'
            ];
        }

        return [
            'data'            => $data,
            'recordsTotal'    => $recordsTotal,
            'recordsFiltered' => $recordsFiltered
        ];
    }

    public function getCreate()
    {
        return \View::make('mygovo::backend.catalog.categories.form');
    }

    public function postCreate(Request $request)
    {
        $model = new Category();

        return $this->save($model, $request);
    }

    public function getEdit($id)
    {
        $data = Category::query()
            ->findOrFail($id);

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('mygovo::backend.catalog.categories.form', ['data' => $data, 'text' => $text]);
    }

    public function postEdit($id, Request $request)
    {
        /**
         * @var Category $model
         */
        $model = Category::query()
            ->findOrFail($id);

        return $this->save($model, $request);
    }

    public function anyDestroy($id)
    {
        Category::query()
            ->findOrFail($id)
            ->delete();

        return \Redirect::route('backend.mygovo.catalog.index');
    }

    public function anyStatus($id)
    {
        $model = Category::query()
            ->findOrFail($id);

        $model->active = ($model->active == 1) ? 0 : 1;
        $model->save();

        return $model->active;
    }

    private function save(Category $model, Request $request)
    {
        return \DB::transaction(
            function () use ($model, $request) {
                $data = $request->input('content', []);

                $slug = empty($data['slug']) ? Str::slug($request->input('text.uk.caption')) : Str::slug($data['slug']);
                $data['slug'] = strtolower($slug);

                $model->fill($data);
                $model->save();

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = CategoryText::firstOrNew(['category_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                return \Redirect::route('backend.mygovo.catalog.edit', ['id' => $model->id])
                    ->with('message', 'success');
            }
        );
    }
}