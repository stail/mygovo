<?php namespace Modules\Mygovo\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Support\Metronic;
use Modules\Mygovo\Models\Lead;
use Modules\Mygovo\Models\LeadComment;

class Leads extends Controller
{
    public $status = [
        1 => 'Новий',
        2 => 'В роботі',
        3 => 'В очікуванні',
        4 => 'Виконано',
        6 => 'Відправлено',
        5 => 'Відмова'
    ];

    public function __construct()
    {
        Metronic::module(['datatable', 'datepicker']);
        Metronic::menu('leads');
    }

    public function getIndex()
    {
        $forms = Lead::query()
            ->distinct()
            ->get(['form']);

        return \View::make('mygovo::backend.leads.index', ['forms' => $forms]);
    }

    public function postIndex(Request $request)
    {
        $status = $request->get('status') ? $request->get('status') : 1;

        $model = Lead::query()
            ->where('status', '=', $status);
        $recordsTotal = $model->count();

        $filters = $request->input('filter', []);
        if (!empty($filters['page'])) {
            $model->where('page', '=', $filters['page']);
        }
        if (!empty($filters['form'])) {
            $model->where('form', '=', $filters['form']);
        }
        if (!empty($filters['date_from'])) {
            $date = date('Y-m-d', strtotime($filters['date_from']));
            $model->where('date', '>=', $date.' 00:00:00');
        }
        if (!empty($filters['date_to'])) {
            $date = date('Y-m-d', strtotime($filters['date_to']));
            $model->where('date', '<=', $date.' 23:59:59');
        }
        $recordsFiltered = $model->count();

        $model->orderBy('created_at', 'desc');
        $model->limit($request->input('length', 10))
            ->offset($request->input('start', 0));

        $collection = $model->get();

        $data = [];

        foreach ($collection as $item) {
            $options = [];
            foreach ($this->status as $status_id => $status_name) {
                $options[] = '<option value="'.$status_id.'" '.($status_id == $item->status ? 'selected' : '').'>'.$status_name.'</option>';
            }
            $options = implode('', $options);
            $comments = $item->comments()->orderBy('id', 'desc')->first();

            $data[] = [
                $item->id,
                $item->page,
                $item->form,
                $item->created_at->format('d.m.Y'),
                $item->name,
                $item->phone,
                ($comments) ? $comments->text : '',
                '<select class="status-change form-control input-sm" data-link="'.route('backend.mygovo.leads.status', ['id' => $item->id]).'">'.$options.'</select>',
                '<a class="btn transparent btn-sm default" href="'.route('backend.mygovo.leads.view', ['id' => $item->id]).'" rel="order-view"><i class="fa fa-search"></i></a> '.
                '<a class="btn green btn-sm" href="'.route('backend.mygovo.leads.edit', ['id' => $item->id]).'"><i class="fa fa-pencil"></i></a> '.
                '<a class="btn red btn-sm" href="'.route('backend.mygovo.leads.destroy', ['id' => $item->id]).'" rel="item-destroy"><i class="fa fa-trash"></i></a>'
            ];
        }

        return [
            'data'            => $data,
            'recordsTotal'    => $recordsTotal,
            'recordsFiltered' => $recordsFiltered
        ];
    }

    public function getEdit($id)
    {
        $data = Lead::query()
            ->findOrFail($id);

        return \View::make('mygovo::backend.leads.edit', ['data' => $data]);
    }

    public function postEdit($id, Request $request)
    {
        $item = Lead::query()
            ->findOrFail($id);

        $item->fill($request->input('content', []));
        $item->save();

        return \Redirect::route('backend.mygovo.leads.index');
    }

    public function getView($id)
    {
        $data = Lead::query()
            ->findOrFail($id);

        return \View::make('mygovo::backend.leads.view', ['data' => $data, 'status' => $this->status]);
    }

    public function anyDestroy($id)
    {
        Lead::query()
            ->findOrFail($id)
            ->delete();

        return \Redirect::route('backend.mygovo.leads.index');
    }

    public function anyStatus($id, Request $request)
    {
        $item = Lead::query()
            ->findOrFail($id);

        $item->status = $request->input('status', 1);
        $item->save();

        return [];
    }

    public function postComment(Request $request)
    {
        LeadComment::create(['lead_id' => $request->input('lead_id'), 'text' => $request->input('text')]);
    }

    /*public function getTtn($id)
    {
        $data = Lead::query()
            ->findOrFail($id);

        return \View::make(
            'backend.leads.ttn',
            [
                'data' => $data
            ]
        );
    }*/

    /*public function postTtn($id, Request $request)
    {
        $item = Lead::query()
            ->findOrFail($id);

        $item->fill($request->input('content', []));
        $item->save();

        Notify::sendSMS(
            'Vkomforti',
            $item->phone,
            trans('notify.sms.ttn_1').$request->input('content.ttn').trans('notify.sms.ttn_2')
        );
    }*/
}