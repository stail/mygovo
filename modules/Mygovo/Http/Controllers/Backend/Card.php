<?php namespace Modules\Mygovo\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\Mygovo\Models\Certificate;

class Card extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::menu('certificates', 'card');
        Metronic::module(['ckeditor', 'colorbox']);
    }

    public function getIndex()
    {
        $data = Certificate::query()
            ->where('type', '=', 'card')
            ->first();

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('mygovo::backend.card.index', ['data' => $data, 'text' => $text]);
    }

    public function postIndex(Request $request)
    {
        return \DB::transaction(
            function () use ($request) {
                $model = Certificate::query()
                    ->where('type', '=', 'card')
                    ->firstOrFail();

                $image = MediaModel::search($request->input('media_path'));
                $data['media_id'] = ($image) ? $image->id : null;

                $model->fill($data);
                $model->save();

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = Certificate\Text::firstOrNew(['certificate_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                return \Redirect::route('backend.mygovo.card.index')
                    ->with('message', 'success');
            }
        );
    }
}