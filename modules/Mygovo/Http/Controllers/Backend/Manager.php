<?php namespace Modules\Mygovo\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\Mygovo\Models\Review as ReviewModel;
use Modules\Mygovo\Models\Service as ServiceModel;

class Manager extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['datatable', 'ckeditor', 'colorbox']);
        Metronic::menu('services', 'manager');
    }

    public function getIndex()
    {
        $data = ReviewModel::query()
            ->where('main', '=', 1)
            ->firstOrFail();

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('mygovo::backend.manager.index', ['data' => $data, 'text' => $text]);
    }

    public function postIndex(Request $request)
    {
        return \DB::transaction(
            function () use ($request) {
                $model = ReviewModel::query()
                    ->where('main', '=', 1)
                    ->firstOrFail();

                $image = MediaModel::search($request->input('media_path'));
                $data['media_id'] = ($image) ? $image->id : null;

                $model->fill($data);
                $model->save();

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = ReviewModel\Text::firstOrNew(['review_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                return \Redirect::route('backend.mygovo.manager.index')
                    ->with('message', 'success');
            }
        );
    }
}