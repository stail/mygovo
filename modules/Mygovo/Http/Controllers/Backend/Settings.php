<?php namespace Modules\Mygovo\Http\Controllers\Backend;

use Illuminate\Routing\Controller;
use Modules\Backend\Models\Language;
use Modules\Backend\Support\Metronic;

class Settings extends Controller
{
    public function __construct()
    {
        Metronic::menu('settings');
    }

    public function getIndex()
    {
        $languages = Language::query()
            ->where('default', '=', 0)
            ->orderBy('id', 'asc')
            ->get();

        return \View::make('mygovo::backend.settings.index', ['languages' => $languages]);
    }

    public function anyStatus($id)
    {
        $model = Language::query()
            ->findOrFail($id);

        $model->active = ($model->active == 1) ? 0 : 1;
        $model->save();

        return $model->active;
    }
}