<?php namespace Modules\Mygovo\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Support\Metronic;
use Modules\Mygovo\Models\Gallery as GalleryModel;

class Gallery extends Controller
{
    public function __construct()
    {
        Metronic::module(['colorbox']);

        Metronic::menu('gallery');
    }

    public function getIndex()
    {
        $data = GalleryModel::query()
            ->get();

        return \View::make('mygovo::backend.gallery.index', ['data' => $data]);
    }

    public function getEdit($id)
    {
        $data = GalleryModel::query()
            ->findOrFail($id);

        $template = $data->key == 'main' ? 'main-gallery' : 'edit';

        return \View::make('mygovo::backend.gallery.'.$template, ['data' => $data]);
    }

    public function postEdit($id, Request $request)
    {
        /**
         * @var GalleryModel $model
         */
        $model = GalleryModel::query()
            ->findOrFail($id);

        $model->images()->delete();
        $model->images()->createMany($request->input('gallery', []));

        return \Redirect::route('backend.mygovo.gallery.edit', ['id' => $model->id])
            ->with('message', 'success');
    }
}