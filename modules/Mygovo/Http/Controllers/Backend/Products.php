<?php namespace Modules\Mygovo\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\Mygovo\Models\Category;
use Modules\Mygovo\Models\Product;
use Modules\Mygovo\Models\ProductText;

class Products extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['datatable', 'colorbox', 'ckeditor']);
        Metronic::menu('catalog', 'products');
    }

    public function getIndex()
    {
        return \View::make('mygovo::backend.catalog.products.index');
    }

    public function postIndex(Request $request)
    {
        $model = Product::query();
        $recordsTotal = $recordsFiltered = $model->count();

        $model->limit($request->input('length', 25))
            ->offset($request->input('start', 0));

        $model->orderBy('created_at', 'desc');

        $collection = $model->get();

        $data = [];
        foreach ($collection as $item) {
            $data[] = [
                $item->text->caption,
                $item->category->text->caption,
                '<a href="'.route('backend.mygovo.products.status', ['id' => $item->id]).'" rel="item-status" class="btn btn-sm '.($item->active ? 'green' : 'red').'"><i class="fa '.($item->active ? 'fa-check' : 'fa-ban').'"></i></a>',
                '<a href="'.route('backend.mygovo.products.edit', ['id' => $item->id]).'" class="btn btn-sm green" title="Редагувати"><i class="fa fa-pencil"></i></a> '.
                '<a href="'.route('backend.mygovo.products.destroy', ['id' => $item->id]).'" rel="item-destroy" class="btn btn-sm red" title="Видалит"><i class="fa fa-trash-o"></i></a>'
            ];
        }

        return [
            'data'            => $data,
            'recordsTotal'    => $recordsTotal,
            'recordsFiltered' => $recordsFiltered
        ];
    }

    public function getCreate()
    {
        $categories = Category::query()
            ->where('active', '=', 1)
            ->get();

        return \View::make('mygovo::backend.catalog.products.form', ['categories' => $categories]);
    }

    public function postCreate(Request $request)
    {
        $model = new Product();

        return $this->save($model, $request);
    }

    public function getEdit($id)
    {
        $data = Product::query()
            ->findOrFail($id);

        $categories = Category::query()
            ->where('active', '=', 1)
            ->get();

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('mygovo::backend.catalog.products.form', ['data' => $data, 'categories' => $categories, 'text' => $text]);
    }

    public function postEdit($id, Request $request)
    {
        /**
         * @var Product $model
         */
        $model = Product::query()
            ->findOrFail($id);

        return $this->save($model, $request);
    }

    public function anyDestroy($id)
    {
        Product::query()
            ->findOrFail($id)
            ->delete();

        return \Redirect::route('backend.mygovo.catalog.index');
    }

    public function anyStatus($id)
    {
        $model = Product::query()
            ->findOrFail($id);

        $model->active = ($model->active == 1) ? 0 : 1;
        $model->save();

        return $model->active;
    }

    private function save(Product $model, Request $request)
    {
        return \DB::transaction(
            function () use ($model, $request) {
                $data = $request->input('content', []);

                $slug = empty($data['slug']) ? Str::slug($request->input('text.uk.caption')) : Str::slug($data['slug']);
                $data['slug'] = strtolower($slug);

                $image = MediaModel::search($request->input('media_path'));
                $data['media_id'] = ($image) ? $image->id : null;

                $model->fill($data);
                $model->save();

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = ProductText::firstOrNew(['product_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                $model->gallery()->delete();
                $model->gallery()->createMany($request->input('gallery', []));

                return \Redirect::route('backend.mygovo.products.edit', ['id' => $model->id])
                    ->with('message', 'success');
            }
        );
    }
}