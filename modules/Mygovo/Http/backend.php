<?php

Route::group(
    ['namespace' => '\\Modules\\Mygovo\\Http\\Controllers\\Backend'],
    function (\Illuminate\Routing\Router $router) {
        $router->controller(
            'categories',
            'Categories',
            [
                'getIndex'   => 'categories.index',
                'getCreate'  => 'categories.create',
                'postCreate' => 'categories.store',
                'getEdit'    => 'categories.edit',
                'postEdit'   => 'categories.update',
                'anyDestroy' => 'categories.destroy',
                'anyStatus'  => 'categories.status',
                'anyWeight'  => 'categories.weight'
            ]
        );

        $router->controller(
            'services',
            'Services',
            [
                'getIndex'   => 'services.index',
                'getCreate'  => 'services.create',
                'postCreate' => 'services.store',
                'getEdit'    => 'services.edit',
                'postEdit'   => 'services.update',
                'anyDestroy' => 'services.destroy',
                'anyStatus'  => 'services.status',
                'anyWeight'  => 'services.weight'
            ]
        );

        $router->controller(
            'manager',
            'Manager',
            [
                'getIndex'  => 'manager.index',
                'postIndex' => 'manager.update'
            ]
        );

        $router->controller(
            'achievements',
            'Achievements',
            [
                'getIndex' => 'achievements.index',
                'getEdit'  => 'achievements.edit',
                'postEdit' => 'achievements.update',
            ]
        );

        $router->controller('card', 'Card', ['getIndex' => 'card.index']);

        $router->controller(
            'certificates',
            'Certificates',
            [
                'getIndex' => 'certificates.index',
                'getEdit'  => 'certificates.edit',
                'postEdit' => 'certificates.update'
            ]
        );

        $router->controller(
            'reviews',
            'Reviews',
            [
                'getIndex'   => 'reviews.index',
                'getCreate'  => 'reviews.create',
                'postCreate' => 'reviews.store',
                'getEdit'    => 'reviews.edit',
                'postEdit'   => 'reviews.update',
                'anyDestroy' => 'reviews.destroy'
            ]
        );

        $router->controller('banners', 'Banners', ['getIndex' => 'banners.index']);
        $router->controller('gallery', 'Gallery', ['getIndex' => 'gallery.index', 'getEdit' => 'gallery.edit']);

        $router->controller(
            'catalog',
            'Catalog',
            [
                'getIndex'   => 'catalog.index',
                'getCreate'  => 'catalog.create',
                'getEdit'    => 'catalog.edit',
                'anyDestroy' => 'catalog.destroy',
                'anyStatus'  => 'catalog.status'
            ]
        );

        $router->controller(
            'products',
            'Products',
            [
                'getIndex'   => 'products.index',
                'getCreate'  => 'products.create',
                'getEdit'    => 'products.edit',
                'anyDestroy' => 'products.destroy',
                'anyStatus'  => 'products.status'
            ]
        );

        $router->controller(
            'leads',
            'Leads',
            [
                'getIndex'    => 'leads.index',
                'getEdit'     => 'leads.edit',
                'getView'     => 'leads.view',
                'anyDestroy'  => 'leads.destroy',
                'anyStatus'   => 'leads.status',
                'postComment' => 'leads.comment'
            ]
        );

        $router->controller('settings', 'Settings', ['getIndex' => 'settings.index', 'anyStatus' => 'settings.status']);

        $router->controller(
            'slider',
            'Slider',
            [
                'getIndex'  => 'slider.index',
                'getEdit'   => 'slider.edit',
                'anyStatus' => 'slider.status',
                'anyWeight' => 'slider.weight'
            ]
        );
    }
);