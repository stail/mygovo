<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media as MediaModel;

class Slide extends Model
{
    protected $table = 'slider';
    protected $fillable = ['weight', 'active', 'media_id'];

    public $timestamps = false;

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(SlideText::class, 'id', 'slide_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(SlideText::class, 'slide_id', 'id');
    }

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}