<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media as MediaModel;

class ProductGallery extends Model
{
    protected $table = 'products_gallery';
    protected $fillable = ['product_id', 'media_id'];

    public $timestamps = false;

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}