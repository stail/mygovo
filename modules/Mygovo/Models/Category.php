<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['slug', 'weight', 'active'];
    protected $appends = ['link'];

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(CategoryText::class, 'id', 'category_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(CategoryText::class, 'category_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }

    public function getLinkAttribute()
    {
        return 'catalog/'.$this->id.'-'.$this->slug;
    }
}