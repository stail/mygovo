<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;

class ProductText extends Model
{
    protected $table = 'products_texts';
    protected $fillable = [
        'product_id',
        'lang',
        'caption',
        'description',
        'description_detail',
        'text'
    ];

    public $timestamps = false;
}