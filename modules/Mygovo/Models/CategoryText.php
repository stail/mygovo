<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryText extends Model
{
    protected $table = 'categories_texts';
    protected $fillable = [
        'category_id',
        'lang',
        'caption',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public $timestamps = false;
}