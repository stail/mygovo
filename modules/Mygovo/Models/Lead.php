<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    use SoftDeletes;

    protected $table = 'leads';
    protected $fillable = [
        'status',
        'page',
        'form',
        'type',
        'date',
        'name',
        'phone',
        'email',
        'ttn',
        'price'
    ];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public $dates = ['created_at'];

    public function comments()
    {
        return $this->hasMany(LeadComment::class, 'lead_id', 'id');
    }
}