<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;

class SlideText extends Model
{
    protected $table = 'slider_texts';
    protected $fillable = ['slide_id', 'lang', 'caption', 'description'];

    public $timestamps = false;
}