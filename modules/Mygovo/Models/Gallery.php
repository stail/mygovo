<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';
    protected $fillable = ['key', 'caption'];

    public $timestamps = false;

    public function images()
    {
        return $this->hasMany(GalleryImages::class, 'gallery_id', 'id');
    }
}