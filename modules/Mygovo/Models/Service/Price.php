<?php namespace Modules\Mygovo\Models\Service;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'services_prices';
    protected $fillable = ['service_id', 'title_uk', 'title_ru', 'title_en', 'price'];

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(Text::class, 'id', 'service_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(Text::class, 'service_id', 'id');
    }

}