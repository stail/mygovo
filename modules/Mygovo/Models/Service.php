<?php namespace Modules\Mygovo\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Media\Models\Media as MediaModel;
use Modules\Mygovo\Models\Service\Category;
use Modules\Mygovo\Models\Service\Gallery as GalleryModel;
use Modules\Mygovo\Models\Service\Price as PriceModel;
use Modules\Mygovo\Models\Service\Text as TextModel;

class Service extends Model
{
    use SoftDeletes;

    protected $table = 'services';
    protected $fillable = ['parent_id', 'service_id', 'slug', 'weight', 'active', 'media_id', 'price'];
    protected $appends = ['link'];

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(TextModel::class, 'id', 'service_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(TextModel::class, 'service_id', 'id');
    }

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }

    public function prices()
    {
        return $this->hasMany(PriceModel::class, 'service_id', 'id');
    }

    public function gallery()
    {
        return $this->hasMany(GalleryModel::class, 'service_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany(Service::class, 'service_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }

    public function getLinkAttribute()
    {
        $category = ($this->parent) ? $this->parent->category->link : $this->category->link;

        return $category.'/'.$this->id.'-'.$this->slug;
    }
}