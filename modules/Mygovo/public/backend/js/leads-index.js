jQuery(function ($) {
    'use strict';

    var _Table = new Datatable(),
        _Body = $('body');

    _Table.setAjaxParam('_token', _TOKEN);
    _Table.init({
        src: $('.data-table'),
        dataTable: {
            ordering: false
        }
    });

    _Body.on('change', '.form-filter', function () {
        _Table.submitFilter();
    });

    _Body.on('change', '.status-change', function () {
        var _Link = $(this).attr('data-link'),
            _Status = $(this).val();

        $.ajax({
            url: _Link,
            method: 'POST',
            data: {
                _token: _TOKEN,
                status: _Status
            },
            success: function (data) {
                /*if (_Status == 6) {
                    ShowAjaxModal({
                        url: '/leads/ttn/' + _ID,
                        success: function () {
                            $('#ttn-form').validate({
                                focusInvalid: false,
                                rules: {
                                    'content[ttn]': {
                                        required: true
                                    }
                                },
                                invalidHandler: function (event, validator) {
                                },
                                highlight: function (element) {
                                    $(element).closest('.form-group').addClass('has-error');
                                },
                                errorPlacement: function (error, element) {
                                    return true;
                                },
                                submitHandler: function (form) {
                                    SubmitAjaxModal({
                                        url: $(form).attr('action'),
                                        data: $(form).serialize(),
                                        success: function () {
                                            _Table.getDataTable().ajax.reload();
                                        }
                                    });
                                }
                            });
                        }
                    });
                }*/

                _Table.getDataTable().ajax.reload();
            }
        });
    });

    $('.date-picker').datepicker({
        language: 'ua',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true,
        format: 'dd.mm.yyyy',
        pickerPosition: 'bottom-left'
    });

    _Body.on('click', '[rel="order-view"]', function (e) {
        e.preventDefault();

        ShowAjaxModal({
            url: $(this).attr('href')
        });
    });
});