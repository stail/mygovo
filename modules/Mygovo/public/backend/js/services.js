jQuery(function ($) {
    'use strict';

    var _Body = $('body');

    $('.sortable').sortable({
        helper: function (event, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });
            return ui;
        },
        update: function () {
            var _Link = $(this).parents('table').data('url');

            $.ajax({
                url: _Link,
                method: 'POST',
                data: {
                    _token: _TOKEN,
                    list: $(this).sortable('toArray', {attribute: 'data-id'})
                }
            });
        }
    });

    _Body.on('click', '[rel="item-status"]', function (e) {
        e.preventDefault();
        var _Link = $(this),
            _Icon = $('i', this);

        UpdateStatus({
            url: _Link.attr('href'),
            success: function (data) {
                var btnColor = (data == 1) ? 'green' : 'red';
                var btnIcon = (data == 1) ? 'fa-check' : 'fa-ban';

                _Link.removeClass('green red');
                _Icon.removeClass('fa-check fa-ban');

                _Link.addClass(btnColor);
                _Icon.addClass(btnIcon);
            }
        });
    });
});
