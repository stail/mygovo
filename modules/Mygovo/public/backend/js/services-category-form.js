jQuery(function ($) {
    'use strict';

    var _Body = $('body');

    var filePicker = $('#file-picker');
    var _Input = filePicker.data('input');

    filePicker.on('click', '[rel="popup"]', function (e) {
        e.preventDefault();
        var elFinder = '/elfinder/popup/';
        var triggerUrl = elFinder + _Input;

        $.colorbox({
            href: triggerUrl,
            fastIframe: true,
            iframe: true,
            width: '70%',
            height: '580'
        });
    });

    filePicker.on('change', _Input, function () {
        if (!$(this).val()) {
            $('[rel="preview"]', filePicker).parent('.input-group-btn').hide();
            $('[rel="delete"]', filePicker).parent('.input-group-btn').hide();
        } else {
            $('[rel="preview"]', filePicker).parent('.input-group-btn').show();
            $('[rel="delete"]', filePicker).parent('.input-group-btn').show();
        }
    });
    $(_Input).change();

    filePicker.on('click', '[rel="delete"]', function () {
        if (confirm('Видалити?')) {
            $(_Input, filePicker).val('').change();
            $('.file-name', filePicker).val('');
        }
    });

    filePicker.on('click', '[rel="preview"]', function (e) {
        e.preventDefault();
        var _href = $(this).attr('href');

        $.colorbox({
            href: _href,
            photo: true,
            width: '70%',
            height: '580'
        });
    });

    function processSelectedFile(filePath, requestingField, file) {
        console.log(arguments);
        $('.file-name', filePicker).val(file.name ? file.name : ' ');
        $('[rel="preview"]', filePicker).attr('href', filePath);
        $(requestingField, filePicker).val(filePath).change();
    }

    window.processSelectedFile = processSelectedFile;
});