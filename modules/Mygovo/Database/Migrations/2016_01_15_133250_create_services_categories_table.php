<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesCategoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'services_categories',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('slug');
                $table->unsignedInteger('weight');
                $table->boolean('active');
                $table->unsignedInteger('media_id')->nullable();
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services_categories');
    }

}
