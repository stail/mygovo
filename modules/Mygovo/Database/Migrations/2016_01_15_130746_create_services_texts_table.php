<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTextsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'services_texts',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('service_id');
                $table->string('lang', 2);
                $table->string('caption');
                $table->text('description');
                $table->text('text');
                $table->string('meta_title');
                $table->text('meta_description');
                $table->text('meta_keywords');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services_texts');
    }

}
