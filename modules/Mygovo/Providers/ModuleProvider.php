<?php namespace Modules\Mygovo\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerConfig();
        $this->registerTranslations();
        $this->registerViews();

        //\BackendMenu::addMenu('leads', 'Ліди', route('backend.mygovo.leads.index'), 'lnr lnr-cart');

        \BackendMenu::addMenu('settings', 'Налаштування', route('backend.mygovo.settings.index'), 'icon-settings');

        \BackendMenu::addMenu('catalog', 'Каталог');
        //\BackendMenu::addItem('catalog', 'categories', 'Категорії', route('backend.mygovo.catalog.index'));
        \BackendMenu::addItem('catalog', 'products', 'Котеджі', route('backend.mygovo.products.index'));

        \BackendMenu::addMenu('services', 'Послуги');
        \BackendMenu::addItem('services', 'categories', 'Категорії', route('backend.mygovo.categories.index'));
        \BackendMenu::addItem('services', 'items', 'Послуги', route('backend.mygovo.services.index'));
        \BackendMenu::addItem('services', 'manager', 'Керівник', route('backend.mygovo.manager.index'));

        //\BackendMenu::addMenu('achievements', 'Досягнення', route('backend.mygovo.achievements.index'));

//        \BackendMenu::addMenu('certificates', 'Сертифікати');
//        \BackendMenu::addItem('certificates', 'card', 'Дисконтна картка', route('backend.mygovo.card.index'));
//        \BackendMenu::addItem('certificates', 'items', 'Сертифікати', route('backend.mygovo.certificates.index'));

        //\BackendMenu::addMenu('reviews', 'Відгуки', route('backend.mygovo.reviews.index'));
        \BackendMenu::addMenu('banners', 'Банери', route('backend.mygovo.banners.index'));

        \BackendMenu::addMenu('gallery', 'Галерея', route('backend.mygovo.gallery.index'));

        \BackendMenu::addMenu('slider', 'Слайдер', route('backend.mygovo.slider.index'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes(
            [
                __DIR__.'/../config/config.php' => config_path('modules/mygovo.php'),
            ]
        );
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php',
            'modules.mygovo'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/mygovo');

        $sourcePath = __DIR__.'/../resources/views';

        $this->publishes(
            [
                $sourcePath => $viewPath
            ]
        );

        $this->loadViewsFrom([$viewPath, $sourcePath], 'mygovo');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/mygovo');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'mygovo');
        } else {
            $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'mygovo');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
    }

}
