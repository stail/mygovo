@extends('mygovo::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    <header>
        <div class="content">
            <div class="preheader">
                <div class="head-phone">
                    <p>+3 8 (050) 536-88-25</p></div>
                <div class="head-position"><a href="assets_mygovo/html/contacts.html">Як нас знайти?</a></div>
                <div class="menu-bars"></div>
                <div class="lang">
                    <ul>
                        @foreach($languages as $lang)
                            <li>
                                <a {{$lang == Lang::getLocale() ? 'class="active"' : ''}} href="{{URL::to($lang->key)}}">{{$lang->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="weather">
                    <img src="assets_mygovo/img/cloud.png" alt="">

                    <div class="degrees">+30&deg;</div>
                </div>
            </div>

            @include('mygovo::templates.top-nav')

            <div class="main-title">
                <h1>На одинці з природою з домашнім комфортом</h1>
                <a href="//www.youtube.com/watch?v=sB6HY8r983c"
                   data-rel="lightcase:myCollection">
                    Переглянути відео</a>
            </div>
            <div class="book-block">
                <h3>Знайти вільні місця</h3>
                <img src="assets_mygovo/img/booking.png" alt="Booking.com">

                <form action="">
                    <input type="text" class="text" id="dpd1" placeholder="Дата заїзду">
                    <input type="text" class="text" id="dpd2" placeholder="Дата виїзду">
                    <input type="submit" value="Перевірити">
                </form>
            </div>
        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <!--
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>
            -->
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="overlay"></div>
                <div class="item active">
                    <div style="background:url(assets_mygovo/img/winter.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="background:url(assets_mygovo/img/smmmer.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left and right controls    -->
            <!--
                          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="lnr lnr-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
            -->
        </div>
    </header>
    <div id="container">
        <div class="cottages">
            <div class="content">
                <div class="title">
                    <h1>Котеджі та шале</h1>
                    <hr>
                    <p>Обирайте з найкращого</p>
                </div>
                <div id="owl-example" class="owl-carousel">
                    <?php $even = true; ?>
                    @foreach ($products->chunk(4) as $chunk_)
                        <?php $even_ = true; ?>
                        @foreach ($chunk_->chunk(2) as $chunk)
                            @foreach($chunk as $item)
                                <div class="cottage-box">
                                    <a class="cottage-inner" href="{{URL::to($item->link)}}">
                                        <div class="cottage-overlay">
                                            <div class="overlay-inner">
                                                <h1 class="overlay-title">{{$item->text->caption}}</h1>
                                                <hr>
                                                <div class="discript">
                                                    {{$item->text->description}}
                                                </div>
                                                <div class="price">{{$item->price}} <span>{{Lang::get('mygovo::common.catalog.currency')}}
                                                        /ніч</span></div>
                                            </div>
                                        </div>
                                        <!--todo: fix image-->
                                        <img src="{{$item->media ? $item->media->path : ''}}" alt="">
                                    </a>
                                </div>
                            @endforeach
                            <?php $even_ = false; ?>
                        @endforeach
                        <?php $even = false; ?>
                    @endforeach

                </div>

            </div>
        </div>
        <div class="services">
            <div class="content">
                <div class="title">
                    <h1>Послуги та розваги</h1>
                    <hr>
                    <p>Відпочивайте зі смаком</p>
                </div>
                <div id="owl-example1" class="owl-carousel">

                    {{--<div class="services-box">--}}
                        {{--<a class="services-inner" href="">--}}
                            {{--<div class="services-overlay">--}}
                                {{--<div class="overlay-inner">--}}
                                    {{--<h1 class="overlay-title">Котедж</h1>--}}
                                    {{--<hr>--}}
                                    {{--<div class="discript">3 Спальні.--}}
                                        {{--Королівські ліжка.--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<img src="assets_mygovo/img/bike.png" alt="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="services-box">--}}
                        {{--<a class="services-inner" href="">--}}
                            {{--<div class="services-overlay">--}}
                                {{--<div class="overlay-inner">--}}
                                    {{--<h1 class="overlay-title">Велосипеди</h1>--}}
                                    {{--<hr>--}}
                                    {{--<div class="discript">Прокат велосипедів</div>--}}

                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<img src="assets_mygovo/img/298634_284525124981300_1632300073_n.jpg" alt="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="services-box">--}}
                        {{--<a class="services-inner" href="">--}}
                            {{--<div class="services-overlay">--}}
                                {{--<div class="overlay-inner">--}}
                                    {{--<h1 class="overlay-title">Котедж</h1>--}}
                                    {{--<hr>--}}
                                    {{--<div class="discript">3 Спальні.--}}
                                        {{--Королівські ліжка.--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<img src="assets_mygovo/img/9.jpg" alt="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="services-box">--}}
                        {{--<a class="services-inner" href="">--}}
                            {{--<div class="services-overlay">--}}
                                {{--<div class="overlay-inner">--}}
                                    {{--<h1 class="overlay-title">Котедж</h1>--}}
                                    {{--<hr>--}}
                                    {{--<div class="discript">3 Спальні.--}}
                                        {{--Королівські ліжка.--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<img src="assets_mygovo/img/4H-3_0mZJUY-1024x764.jpg" alt="">--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    @foreach($services as $item)
                    <div class="services-box">
                        <a class="services-inner" href="{{$item->link}}">
                            <div class="services-overlay">
                                <div class="overlay-inner">
                                    <h1 class="overlay-title">{{$item->text->caption}}</h1>
                                    <hr>
                                    <div class="discript">
                                        {{$item->text->description}}
                                    </div>
                                </div>
                            </div>
                            <img src="{{$item->media ? $item->media->path : ''}}" alt="">
                        </a>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
        <div class="advantages">
            <div class="content">
                <div class="title">
                    <h1>Чому саме ми?</h1>
                    <hr>
                    <p>Відчуйте різницю</p>
                </div>
                <div class="left">
                    <div class="advant-box">
                        <img src="assets_mygovo/img/distance.png" alt="">

                        <div class="advant-title">60 км від Чернівців</div>
                    </div>
                    <div class="advant-box">
                        <img src="assets_mygovo/img/bus.png" alt="">

                        <div class="advant-title">Регулярні перевезення</div>
                    </div>
                    <div class="advant-box">
                        <img src="assets_mygovo/img/house.png" alt="">

                        <div class="advant-title">Власний котедж</div>
                    </div>
                    <div class="advant-box">
                        <img src="assets_mygovo/img/ring.png" alt="">

                        <div class="advant-title">Всі зручності</div>
                    </div>

                </div>
                <div class="right">
                    <img src="assets_mygovo/img/advant.png" alt="">
                </div>

            </div>
        </div>

        <div class="discription">
            <div class="content">
                <div class="title">
                    <h1>Відпочинок в комплексі "Долина Миколи" </h1>
                    <hr>
                </div>
                <div class="overlay"></div>
                <div class="text">
                    Розкішний змішаний ліс підступає прямо до котеджу, прекрасна панорама гір видно з будь-якого
                    вікна. Котедж розташований відокремлено, навколо немає скупчення будівель і людей. Ніщо і ніхто
                    не завадить вам насолоджуватися природою. Від затишного котеджу до гірськолижного – туристичного
                    комплексу Мигово ви можете дістатися за 10 хвилин.
                    <br><br>
                    Зняти будиночок в Мигово, це означає не просто отримати в користування кімнату із зручностями.
                    Ви також отримуєте можливість користуватися великою доглянутою територією, де є два озера.
                    Біля будинку є альтанка, мангал, зручна тераса, дитячий майданчик, лавочки, природні джерела,
                    парковка автомобілів і багато іншого. А також на території є лижна міні – траса довжиною 150
                    метрів
                </div>

            </div>
        </div>
        <div id="map"></div>

        <div class="map">
            <div class="content">
                <div class="ask-block">
                    <h3>Залишися питання?</h3>

                    <form action="">
                        <input type="text" class="text" placeholder="Ім'я">
                        <input type="tel" class="text" placeholder="Телефон"
                               pattern="\+38\(0[0-9]{2}\)-[0-9]{3}-[0-9]{2}-[0-9]{2})">
                        <input type="email" class="text" placeholder="Пошта">
                        <input type="submit" value="Надіслати">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="{{URL::asset('/assets_mygovo/js/map.js')}}"></script>
@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('a[data-rel^=lightcase]').lightcase({
//            iframe:{width : 'auto',
//                    height : 'auto',
//                    frameborder : 0
//                    },
                flash: {
                    width: 400,
                    height: 205,
                    wmode: 'transparent'
                },
                video: {
                    width: 400,
                    height: 225,
                    poster: '',
                    preload: 'auto',
                    controls: true,
                    autobuffer: true,
                    autoplay: true,
                    loop: false
                }
            });
        });
    </script>
    <script>
        $("#owl-example").owlCarousel({

            // Most important owl features
            items: 3,
            itemsCustom: false,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [981, 3],
            itemsTablet: [769, 2],
            itemsTabletSmall: false,
            itemsMobile: [481, 1],
            singleItem: false,
            itemsScaleUp: false,

            //Basic Speeds
            slideSpeed: 200,
            paginationSpeed: 800,
            rewindSpeed: 1000,

            //Autoplay
            autoPlay: false,
            stopOnHover: false,

            // Navigation
            navigation: false,
            navigationText: ["prev", "next"],
            rewindNav: true,
            scrollPerPage: false,

            //Pagination
            pagination: true,
            paginationNumbers: false,

            // Responsive
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,

            // CSS Styles
            baseClass: "owl-carousel",
            theme: "owl-theme",

            //Lazy load
            lazyLoad: false,
            lazyFollow: true,
            lazyEffect: "fade",

            //Auto height
            autoHeight: false,

            //JSON
            jsonPath: false,
            jsonSuccess: false,

            //Mouse Events
            dragBeforeAnimFinish: true,
            mouseDrag: true,
            touchDrag: true,

            //Transitions
            transitionStyle: false,

            // Other
            addClassActive: false,

            //Callbacks
            beforeUpdate: false,
            afterUpdate: false,
            beforeInit: false,
            afterInit: false,
            beforeMove: false,
            afterMove: false,
            afterAction: false,
            startDragging: false,
            afterLazyLoad: false

        })
    </script>

    <script>
        $("#owl-example1").owlCarousel({

            // Most important owl features
            items: 4,
            itemsCustom: false,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [981, 3],
            itemsTablet: [769, 2],
            itemsTabletSmall: false,
            itemsMobile: [481, 1],
            singleItem: false,
            itemsScaleUp: false,

            //Basic Speeds
            slideSpeed: 200,
            paginationSpeed: 800,
            rewindSpeed: 1000,

            //Autoplay
            autoPlay: false,
            stopOnHover: false,

            // Navigation
            navigation: false,
            navigationText: ["prev", "next"],
            rewindNav: true,
            scrollPerPage: false,

            //Pagination
            pagination: true,
            paginationNumbers: false,

            // Responsive
            responsive: true,
            responsiveRefreshRate: 200,
            responsiveBaseWidth: window,

            // CSS Styles
            baseClass: "owl-carousel",
            theme: "owl-theme",

            //Lazy load
            lazyLoad: false,
            lazyFollow: true,
            lazyEffect: "fade",

            //Auto height
            autoHeight: false,

            //JSON
            jsonPath: false,
            jsonSuccess: false,

            //Mouse Events
            dragBeforeAnimFinish: true,
            mouseDrag: true,
            touchDrag: true,

            //Transitions
            transitionStyle: false,

            // Other
            addClassActive: false,

            //Callbacks
            beforeUpdate: false,
            afterUpdate: false,
            beforeInit: false,
            afterInit: false,
            beforeMove: false,
            afterMove: false,
            afterAction: false,
            startDragging: false,
            afterLazyLoad: false

        })
    </script>
@endsection