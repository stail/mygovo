@extends('mygovo::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')

    <header>
        <div class="content">
            <div class="preheader">
                <div class="head-phone">
                    <p>+3 8 (050) 536-88-25</p></div>
                <div class="head-position"><a href="contacts.html">Як нас знайти?</a></div>
                <div class="menu-bars"></div>
                <div class="lang">
                    <ul>
                        @foreach($languages as $lang)
                            <li>
                                <a {{$lang == Lang::getLocale() ? 'class="active"' : ''}} href="{{URL::to($lang->key)}}">{{$lang->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="weather">
                    <img src="/assets_mygovo/img/cloud.png" alt="">

                    <div class="degrees">+30&deg;</div>
                </div>
            </div>
            @include('mygovo::templates.top-nav')

            <div class="main-title">
                <h1>На одинці з природою з домашнім комфортом</h1>
            </div>
            <div class="cottage-navigation">
                <a href="" title="Попередній котедж"><img src="/assets_mygovo/img/prev-arrow.png" alt="prev"></a>
                <a href="" title="Наступний котедж"><img src="/assets_mygovo/img/next-arrow.png" alt="next"></a>
            </div>
        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <!--
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>
            -->
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="overlay"></div>
                <div class="item active">
                    <div style="background:url(/assets_mygovo/img/winter.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="background:url(/assets_mygovo/img/smmmer.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left and right controls    -->
            <!--
                          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="lnr lnr-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
            -->
        </div>
    </header>
    <div id="container">
        <div class="content">
            <div class="left">
                <?php $first = $products[0];?>
                <div class="cottage-title">{{ $first->text->caption }}</div>
                <div id="owl-demo" class="owl-carousel owl-theme">
                    @foreach($first->gallery as $item)
                        <div class="item"><img src="{{$item->media ? $item->media->path : ''}}" alt=""></div>
                    @endforeach
                </div>
                <span class="cottage-discription">
                      {!! $first->text->description_detail !!}
                </span>


                <h1 class="other-cottages"> Перегляньте також</h1>
                <?php $one = true ?>
                @foreach($products as $item)
                    <div class="cottage-box {{ $one ? 'hidden' : '' }}">
                        <a class="cottage-inner" href="{{URL::to($item->link)}}">
                            <div class="cottage-overlay">
                                <div class="overlay-inner">
                                    <h1 class="overlay-title">{{ $item->text->caption }}</h1>
                                    <hr>
                                    <div class="discript">
                                        {{ $item->text->description }}
                                    </div>
                                    <div class="price">{{$item->price}}<span>{{Lang::get('mygovo::common.catalog.currency')}}
                                            /ніч</span></div>
                                </div>
                            </div>
                            <img src="{{$item->media ? $item->media->path : ''}}" alt="">
                        </a>
                    </div>
                    <?php $one = false ?>
                    @endforeach


            </div>
            <div class="right">
                <div class="right-inner">
                    <div class="cottage-price">
                        <div class="price">{{ $first->price }} <span>грн</span></div>
                        <hr>
                        <p>ніч</p>
                    </div>
                    <div class="book-block">
                        <h3>Знайти вільні місця</h3>

                        <form action="">
                            <input type="text" class="text" id="dpd1" placeholder="Дата заїзду">
                            <input type="text" class="text" id="dpd2" placeholder="Дата виїзду">
                            <input type="submit" value="Перевірити">
                        </form>
                    </div>
                    <div>
                        {!! $first->text->text !!}
                        {{--<ul>--}}
                            {{--<li>Кухня</li>--}}
                            {{--<li>WIFI</li>--}}
                            {{--<li>LCD панель</li>--}}
                            {{--<li>Тераса</li>--}}
                            {{--<li>Парковка</li>--}}
                            {{--<li>Бесідка і мангал</li>--}}
                        {{--</ul>--}}
                    </div>

                </div>
            </div>


        </div>

    </div>

@endsection
@section('scripts')

    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>
    <script>
        $(document).ready(function () {

            $("#owl-demo").owlCarousel({

                navigation: false, // Show next and prev buttons
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
                pagination: true


                // "singleItem:true" is a shortcut for:
//       items : 1,
//       itemsDesktop : 1,
//       itemsDesktopSmall : 1,
//       itemsTablet: 1,
//       itemsMobile : 1

            });

        });
    </script>
    <link href="{{URL::asset('/assets_mygovo/css/cottage.css')}}" rel="stylesheet">
@endsection