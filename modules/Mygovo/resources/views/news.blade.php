@extends('mygovo::layouts.base')
@section('meta-tags')
    <title>{{$data->text->caption}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    <link rel="stylesheet" href="/assets_mygovo/css/gallery.css">
    <header>
        <div class="content">
            <div class="preheader">
                <div class="head-phone">
                    <p>+3 8 (050) 536-88-25</p></div>
                <div class="head-position"><a href="/contacts">Як нас знайти?</a></div>
                <div class="menu-bars"></div>
                <div class="lang">
                    <ul>
                        @foreach($languages as $lang)
                            <li>
                                <a {{$lang == Lang::getLocale() ? 'class="active"' : ''}} href="{{URL::to($lang->key)}}">{{$lang->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="weather">
                    <img src="/assets_mygovo/img/cloud.png" alt="">
                    <div class="degrees">+30&deg;</div>
                </div>
            </div>
            @include('mygovo::templates.top-nav')

            <div class="main-title">
                <h1>На одинці з природою з домашнім комфортом</h1>
            </div>
            <div class="cottage-navigation">
                <a href="" title="Попередній котедж"><img src="/assets_mygovo/img/prev-arrow.png" alt="prev"></a>
                <a href="" title="Наступний котедж"><img src="/assets_mygovo/img/next-arrow.png" alt="next"></a>
            </div>
        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <!--
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>
            -->
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="overlay"></div>
                <div class="item active">
                    <div style="background:url(/assets_mygovo/img/winter.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="background:url(/assets_mygovo/img/smmmer.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left and right controls    -->
            <!--
                          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="lnr lnr-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
            -->
        </div>
    </header>
    <div id="container">
        <div class="content">
            <div class="title">Блог</div>
            <hr>
            @foreach($news as $item)
                <div class="blog-block">
                    <div class="disc-image"><img src="{{$item->media ? $item->media->path : ''}}" alt=""></div>
                    <div class="blog-discription">
                        <div class="discript-title">
                            {{$item->text->caption}}
                        </div>
                        <div class="blog-date">{{ $item->date }}</div>

                        <div class="text">
                            {!! $item->text->text !!}
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>

@endsection
@section('scripts')
    <link href="{{URL::asset('/assets_mygovo/css/blog.css')}}" rel="stylesheet">
@endsection
