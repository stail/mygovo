@extends('mygovo::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    <link rel="stylesheet" href="/assets_mygovo/css/service.css">
    <header>
        <div class="content">
            <div class="preheader">
                <div class="head-phone">
                    <p>+3 8 (050) 536-88-25</p></div>
                <div class="head-position"><a href="contacts.html">Як нас знайти?</a></div>
                <div class="menu-bars"></div>
                <div class="lang">
                    <ul>
                        @foreach($languages as $lang)
                            <li>
                                <a {{$lang == Lang::getLocale() ? 'class="active"' : ''}} href="{{URL::to($lang->key)}}">{{$lang->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="weather">
                    <img src="/assets_mygovo/img/cloud.png" alt="">
                    <div class="degrees">+30&deg;</div>
                </div>
            </div>
            @include('mygovo::templates.top-nav')
            <div class="main-title">
                <h1>На одинці з природою з домашнім комфортом</h1>
            </div>

        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->

            <div class="carousel-inner" role="listbox">
                <div class="overlay"> </div>
                <div class="item active">
                    <div style="background:url(/assets_mygovo/img/winter.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="background:url(/assets_mygovo/img/smmmer.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </header>
    <div id="container">
        <div class="content">
            <div class="title">Послуги та розваги</div>
            <hr>
            @foreach($services as $service)
                <div class="service-block">

                    <div class="servise-discription">
                        <div class="discript-title">
                            {{ $service->text->caption }}
                        </div>

                        <div class="text">
                            {{$service->text->meta_description}}
                        </div>
                    </div>
                    <div class="disc-image"><img src="{{$service->media ? $service->media->path : ''}}" alt=""></div>
                </div>
            @endforeach
        </div>

    </div>
@endsection
