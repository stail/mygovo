<?php
$_status = [
        1 => 'Новий',
        2 => 'В роботі',
        3 => 'В очікуванні',
        4 => 'Виконано',
        6 => 'Відправлено',
        5 => 'Відмова',
];

$new = \Modules\Mygovo\Models\Lead::query()
        ->where('status', '=', '1')
        ->limit(5)
        ->orderBy('created_at', 'desc')
        ->get();

$progress = \Modules\Mygovo\Models\Lead::query()
        ->where('status', '=', '2')
        ->limit(5)
        ->orderBy('created_at', 'desc')
        ->get();

$done = \Modules\Mygovo\Models\Lead::query()
        ->where('status', '=', '4')
        ->limit(5)
        ->orderBy('created_at', 'desc')
        ->get();
