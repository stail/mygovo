@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Досягнення</div>
                    <div class="actions"></div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="table-container">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr class="heading">
                                    <th>Досягнення</th>
                                    <th>Число</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $item)
                                    <tr>
                                        <td>
                                            <input class="form-control margin-bottom-5" type="text" name="content[{{$item->id}}][title_uk]"
                                                   value="{{$item->title_uk}}" placeholder="Українська">
                                            <input class="form-control margin-bottom-5" type="text" name="content[{{$item->id}}][title_ru]"
                                                   value="{{$item->title_ru}}" placeholder="Русский">
                                            <input class="form-control" type="text" name="content[{{$item->id}}][title_en]"
                                                   value="{{$item->title_en}}" placeholder="English">
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" name="content[{{$item->id}}][number]"
                                                   value="{{$item->number}}">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/backend/js/common.js')}}"></script>
    <script src="{{asset('modules/mygovo/backend/js/services.js')}}"></script>
@endsection