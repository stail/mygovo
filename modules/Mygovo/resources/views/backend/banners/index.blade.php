@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Банери</div>
                    <div class="actions"></div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="form-body">
                            @foreach($banners as $data)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Банер {{$data->id}}</label>

                                            <div class="input-group file-picker-container" id="file-picker-{{$data->id}}" data-input="media-id-{{$data->id}}">
                                                <input type="hidden" name="data[{{$data->id}}][media_id]" id="media-id-{{$data->id}}"
                                                       value="{{$data->media ? $data->media->id : ''}}">
                                                <input type="text" class="form-control file-name" readonly
                                                       placeholder="Файл не вибрано"
                                                       value="{{$data->media ? $data->media->caption : ''}}">
                                            <span class="input-group-btn">
                                                <a class="btn default" rel="popup" type="button">
                                                    <i class="icon-refresh"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn blue" rel="preview" type="button" target="_blank"
                                                   href="{{$data->media ? $data->media->path(): ''}}">
                                                    <i class="fa fa-eye"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn red" rel="delete" href="javascript:">
                                                    <i class="fa fa-trash"></i></a>
                                            </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Посилання</label>
                                            <input name="data[{{$data->id}}][link]" type="text" class="form-control"
                                                   value="{{$data ? $data->link : ''}}">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/mygovo/backend/js/banners.js')}}"></script>
@endsection