@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Додавання категорії</div>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.mygovo.categories.index')}}">
                            <i class="fa fa-arrow-left"></i> Назад</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Заголовок</label>
                                        <input type="text" class="form-control" name="text_uk[caption]">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Статус</label>
                                        <select class="form-control" name="content[active]">
                                            <option value="1">Опубліковано</option>
                                            <option value="0">Приховано</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Зображення</label>

                                        <div class="input-group" id="file-picker" data-input=".media-id">
                                            <input type="hidden" name="media_path" class="media-id">
                                            <input type="text" class="form-control file-name" readonly
                                                   placeholder="Файл не вибрано">
                                                <span class="input-group-btn">
                                                    <a class="btn default" rel="popup" type="button">
                                                        <i class="icon-refresh"></i></a>
                                                </span>
                                                <span class="input-group-btn">
                                                    <a class="btn blue" rel="preview" type="button" href="javascript:"
                                                       target="_blank">
                                                        <i class="fa fa-eye"></i></a>
                                                </span>
                                                <span class="input-group-btn">
                                                    <a class="btn red" rel="delete" href="javascript:">
                                                        <i class="fa fa-trash"></i></a>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">URL</label>
                                        <input type="text" class="form-control" name="content[slug]">
                                    </div>
                                </div>
                            </div>

                            <h3 class="form-section">Мета-теги</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Заголовок</label>
                                        <input type="text" class="form-control" name="text_uk[meta_title]">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Опис</label>
                                        <input type="text" class="form-control" name="text_uk[meta_description]">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Ключові слова</label>
                                        <input type="text" class="form-control" name="text_uk[meta_keywords]">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/mygovo/backend/js/services-category-form.js')}}"></script>
@endsection