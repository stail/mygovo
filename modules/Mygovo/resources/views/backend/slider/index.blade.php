@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Слайдер</div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover"
                               data-url="{{URL::route('backend.mygovo.slider.weight')}}">
                            <thead>
                            <tr class="heading">
                                <th>{{trans('backend::common.caption')}}</th>
                                <th width="40px">{{trans('backend::common.status')}}</th>
                                <th width="45px">{{trans('backend::common.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody class="sortable">
                            @foreach($data as $item)
                                <tr data-id="{{$item->id}}">
                                    <td>{{$item->text && !empty($item->text->caption) ? $item->text->caption : 'Слайд №'.$item->id}}</td>
                                    <td>
                                        <a href="{{URL::route('backend.mygovo.slider.status',['id' => $item->id])}}"
                                           rel="item-status" class="btn btn-sm {{$item->active ? 'green' : 'red'}}">
                                            <i class="fa {{$item->active ? 'fa-check' : 'fa-ban'}}"></i></a>
                                    </td>
                                    <td>
                                        <a href="{{URL::route('backend.mygovo.slider.edit', ['id' => $item->id])}}"
                                           rel="edit-portlet" class="btn btn-sm green" title="Редагувати">
                                            <i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(function ($) {
            'use strict';

            var _Body = $('body');

            $('.sortable').sortable({
                helper: function (event, ui) {
                    ui.children().each(function () {
                        $(this).width($(this).width());
                    });
                    return ui;
                },
                update: function () {
                    var _Link = $(this).parents('table').data('url');

                    $.ajax({
                        url: _Link,
                        method: 'POST',
                        data: {
                            _token: _TOKEN,
                            list: $(this).sortable('toArray', {attribute: 'data-id'})
                        }
                    });
                }
            });

            _Body.on('click', '[rel="item-status"]', function (e) {
                e.preventDefault();
                var _Link = $(this),
                        _Icon = $('i', this);

                UpdateStatus({
                    url: _Link.attr('href'),
                    success: function (data) {
                        var btnColor = (data == 1) ? 'green' : 'red';
                        var btnIcon = (data == 1) ? 'fa-check' : 'fa-ban';

                        _Link.removeClass('green red');
                        _Icon.removeClass('fa-check fa-ban');

                        _Link.addClass(btnColor);
                        _Icon.addClass(btnIcon);
                    }
                });
            });
        });
    </script>
@endsection