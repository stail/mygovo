@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Редагування відкгуку</div>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.mygovo.reviews.index')}}">
                            <i class="fa fa-arrow-left"></i> Назад</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Ім'я</label>
                                        <input type="text" class="form-control" name="text_uk[caption]"
                                               value="{{isset($data) ? $data->texts[0]->caption : ''}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Зображення</label>

                                        <div class="input-group" id="file-picker" data-input=".media-id">
                                            <input type="hidden" name="media_path" class="media-id"
                                                   value="{{isset($data) && $data->media ? $data->media->path() : ''}}">
                                            <input type="text" class="form-control file-name" readonly
                                                   placeholder="Файл не вибрано"
                                                   value="{{isset($data) && $data->media ? $data->media->caption : ''}}">
                                            <span class="input-group-btn">
                                                <a class="btn default" rel="popup" type="button">
                                                    <i class="icon-refresh"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn blue" rel="preview" type="button" target="_blank"
                                                   href="{{isset($data) && $data->media ? $data->media->path(): ''}}">
                                                    <i class="fa fa-eye"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn red" rel="delete" href="javascript:">
                                                    <i class="fa fa-trash"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Текст</label>
                                        <textarea class="form-control" style="height: 100px;"
                                                  name="text_uk[text]">{{isset($data) ? $data->texts[0]->text : ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/mygovo/backend/js/media-manager.js')}}"></script>
@endsection