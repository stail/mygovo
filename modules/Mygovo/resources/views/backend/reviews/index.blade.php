@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Відгуки</div>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.mygovo.reviews.create')}}">
                            <i class="fa fa-plus"></i> Додати</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr class="heading">
                                <th>{{trans('backend::common.caption')}}</th>
                                <th width="100px">{{trans('backend::common.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody class="sortable">
                            @foreach($data as $item)
                                <tr>
                                    <td>{{$item->text->caption}}</td>
                                    <td>
                                        <a href="{{URL::route('backend.mygovo.reviews.edit', ['id' => $item->id])}}"
                                           rel="edit-portlet" class="btn btn-sm green" title="Редагувати">
                                            <i class="fa fa-pencil"></i></a>
                                        <a href="{{URL::route('backend.mygovo.reviews.destroy', ['id' => $item->id])}}"
                                           rel="item-destroy" class="btn btn-sm red" title="Видалити">
                                            <i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection