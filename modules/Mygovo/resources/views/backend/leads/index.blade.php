@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="tabbable-line tabs-below">
                <ul class="nav nav-tabs">
                    <li {!!(Request::get('status') == 1 || empty(Request::get('status'))) ? 'class="active"' : ''!!}>
                        <a href="{{URL::route('backend.mygovo.leads.index')}}">Нові</a>
                        <span>{{\Modules\Mygovo\Models\Lead::query()->where('status', '=', '1')->count()}}</span>
                    </li>
                    <li {!!Request::get('status') == 2 ? 'class="active"' : ''!!}>
                        <a href="{{URL::route('backend.mygovo.leads.index', ['status' => 2])}}">В роботі</a>
                        <span>{{\Modules\Mygovo\Models\Lead::query()->where('status', '=', '2')->count()}}</span>
                    </li>
                    <li {!!Request::get('status') == 3 ? 'class="active"' : ''!!}>
                        <a href="{{URL::route('backend.mygovo.leads.index', ['status' => 3])}}">В очікуванні</a>
                        <span>{{\Modules\Mygovo\Models\Lead::query()->where('status', '=', '3')->count()}}</span>
                    </li>
                    <li {!!Request::get('status') == 6 ? 'class="active"' : ''!!}>
                        <a href="{{URL::route('backend.mygovo.leads.index', ['status' => 6])}}">Відправлено</a></li>
                    <li {!!Request::get('status') == 4 ? 'class="active"' : ''!!}>
                        <a href="{{URL::route('backend.mygovo.leads.index', ['status' => 4])}}">Виконано</a></li>
                    <li {!!Request::get('status') == 5 ? 'class="active"' : ''!!}>
                        <a href="{{URL::route('backend.mygovo.leads.index', ['status' => 5])}}">Відмова</a></li>
                </ul>
            </div>
            <div class="portlet box blue-hoki base-portlet margin-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold">Ліди</span></div>
                    <div class="actions"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper"><span></span></div>
                        <table class="table table-striped table-bordered table-hover data-table">
                            <thead>
                            <tr class="heading">
                                <th width="40px">ID</th>
                                <th width="110px">Сторінка</th>
                                <th width="110px">Форма</th>
                                <th width="130px">Дата</th>
                                <th>Ім'я</th>
                                <th width="125px">Телефон</th>
                                <th width="110px">Коментарі</th>
                                <th width="150px">Статус</th>
                                <th width="125px">Дії</th>
                            </tr>
                            <tr class="filter">
                                <td></td>
                                <td>
                                    {{--<select name="filter[page]" class="form-control form-filter input-sm">
                                        <option value="">Виберіть...</option>
                                        <option value="panels">Інфрачервоні обігрівачі</option>
                                        <option value="ceramic">Керамічні обігрівачі</option>
                                    </select>--}}
                                </td>
                                <td>
                                    <select name="filter[form]" class="form-control form-filter input-sm">
                                        <option value="">Виберіть...</option>
                                        @foreach($forms as $item)
                                            <option value="{{$item->form}}">{{$item->form}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group date date-picker margin-bottom-5">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="filter[date_from]" placeholder="Від">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button">
                                                <i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <div class="input-group date date-picker" >
                                        <input type="text" class="form-control form-filter input-sm" readonly name="filter[date_to]"  placeholder="До">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button">
                                                <i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-sm yellow filter-submit" title="Шукати">
                                        <i class="fa fa-search"></i></button>
                                    <button class="btn btn-sm red filter-cancel" title="Скасувати">
                                        <i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/mygovo/backend/js/leads-index.js')}}"></script>
@endsection