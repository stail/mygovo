@extends('backend::layouts.modal')
@section('modal')
    <div class="modal-body">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Замовлення №{{$data->id}}</div>
            </div>
            <div class="portlet-body form">
                <div class="form-horizontal form-bordered">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Сторінка</label>
                            <div class="col-md-9">
                                <p class="form-control-static">{{$data->page}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Статус</label>
                            <div class="col-md-9">
                                <p class="form-control-static">{{$status[$data->status]}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Форма</label>
                            <div class="col-md-9">
                                <p class="form-control-static">{{$data->form}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Ім'я</label>
                            <div class="col-md-9">
                                <p class="form-control-static">{{$data->name}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Телефон</label>
                            <div class="col-md-9">
                                <p class="form-control-static">{{$data->phone}}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">E-mail</label>
                            <div class="col-md-9">
                                <p class="form-control-static">{{$data->email}}</p>
                            </div>
                        </div>
                        @if($data->comments->count() > 0)
                            <div class="form-group">
                                <label class="control-label col-md-3">Коментарі</label>
                                <div class="col-md-9">
                                    @foreach($data->comments as $item)
                                        <p class="form-control-static">{{$item->text}}</p><br>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"> Закрити</button>
    </div>
@endsection