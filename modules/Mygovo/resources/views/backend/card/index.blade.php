@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Дисконтна картка</h3>

            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <ul class="nav nav-tabs pull-left">
                        @foreach($languages as $lang)
                            <li class="{{$lang->default ? 'active' : ''}}">
                                <a href="#portlet_tab_{{$lang->key}}" data-toggle="tab" aria-expanded="true">
                                    {{$lang->caption}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">


                        <div class="tab-content">
                            @foreach($languages as $lang)
                                <div class="tab-pane form-body {{$lang->default ? 'active' : ''}}" id="portlet_tab_{{$lang->key}}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Заголовок</label>
                                                <input type="text" class="form-control" name="text[{{$lang->key}}][caption]"
                                                       value="{{array_key_exists($lang->key, $text) ? $text[$lang->key]['caption'] : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Текст</label>
                                            <textarea class="form-control editor"
                                                      name="text[{{$lang->key}}][text]">{{array_key_exists($lang->key, $text) ? $text[$lang->key]['text'] : ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Зображення</label>

                                        <div class="input-group" id="file-picker" data-input=".media-id">
                                            <input type="hidden" name="media_path" class="media-id"
                                                   value="{{$data->media ? $data->media->path() : ''}}">
                                            <input type="text" class="form-control file-name" readonly
                                                   placeholder="Файл не вибрано"
                                                   value="{{$data->media ? $data->media->caption : ''}}">
                                            <span class="input-group-btn">
                                                <a class="btn default" rel="popup" type="button">
                                                    <i class="icon-refresh"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn blue" rel="preview" type="button" target="_blank"
                                                   href="{{$data->media ? $data->media->path(): ''}}">
                                                    <i class="fa fa-eye"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn red" rel="delete" href="javascript:">
                                                    <i class="fa fa-trash"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/mygovo/backend/js/services-form.js')}}"></script>
@endsection