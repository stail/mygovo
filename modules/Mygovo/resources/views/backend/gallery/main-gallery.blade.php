@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Галерея</div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="form-body">
                            <?php $i = 1; ?>
                            <?php $images = $data->images()->orderBy('id', 'asc')->get(); ?>
                            @foreach($images as $item)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Фото {{$i}}</label>

                                            <div class="input-group file-picker-container" id="file-picker-{{$i}}" data-input="media-id-{{$i}}">
                                                <input type="hidden" name="gallery[{{$i}}][media_id]" id="media-id-{{$i}}"
                                                       value="{{$item->media ? $item->media->id : ''}}">
                                                <input type="text" class="form-control file-name" readonly
                                                       placeholder="Файл не вибрано"
                                                       value="{{$item->media ? $item->media->caption : ''}}">
                                            <span class="input-group-btn">
                                                <a class="btn default" rel="popup" type="button">
                                                    <i class="icon-refresh"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn blue" rel="preview" type="button" target="_blank"
                                                   href="{{$item->media ? $item->media->path(): ''}}">
                                                    <i class="fa fa-eye"></i></a>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/mygovo/backend/js/gallery.js')}}"></script>
@endsection