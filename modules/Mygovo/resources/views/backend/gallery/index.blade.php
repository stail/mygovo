@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Галерея</div>
                    <div class="actions"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr class="heading">
                                <th>{{trans('backend::common.caption')}}</th>
                                <th width="40px">{{trans('backend::common.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr data-id="{{$item->id}}">
                                    <td>{{$item->caption}}</td>
                                    <td>
                                        <a href="{{URL::route('backend.mygovo.gallery.edit', ['id' => $item->id])}}"
                                           rel="edit-portlet" class="btn btn-sm green" title="Редагувати">
                                            <i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/backend/js/common.js')}}"></script>
    <script src="{{asset('modules/mygovo/backend/js/services.js')}}"></script>
@endsection