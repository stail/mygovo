<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">

    @yield('meta-tags')

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{URL::asset('assets_mygovo/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets_mygovo/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets_mygovo/css/datepicker.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets_mygovo/css/owl.carousel.css')}}" rel="stylesheet" >
    <link href="{{URL::asset('assets_mygovo/css/owl.theme.css')}}" rel="stylesheet" >
    <link rel="stylesheet" href="{{URL::asset('assets_mygovo/css/lightcase.css')}}">
    <link href="{{URL::asset('assets_mygovo/css/bootstrap-theme.css')}}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="http://www.youtube.com/iframe_api"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="wrapper">
@yield('main')
</div>

@include('mygovo::templates.footer')
<script src="{{URL::asset('/assets_mygovo/js/jquery-2.1.4.js')}}"></script>
<script src="{{URL::asset('/assets_mygovo/js/bootstrap.js')}}"></script>
<script src="{{URL::asset('/assets_mygovo/js/owl.carousel.js')}}"></script>
<script src="{{URL::asset('/assets_mygovo/js/bootstrap-datepicker.js')}}"></script>
<script src="{{URL::asset('/assets_mygovo/js/lightcase.js')}}"></script>
<script src="{{URL::asset('/assets_mygovo/js/script.js')}}"></script>


@yield('scripts')
</body>
</html>