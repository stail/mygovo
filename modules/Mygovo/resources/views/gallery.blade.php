@extends('mygovo::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    <link rel="stylesheet" href="/assets_mygovo/css/gallery.css">
    <header>
        <div class="content">
            <div class="preheader">
                <div class="head-phone">
                    <p>+3 8 (050) 536-88-25</p></div>
                <div class="head-position"><a href="/contacts">Як нас знайти?</a></div>
                <div class="menu-bars"></div>
                <div class="lang">
                    <ul>
                        @foreach($languages as $lang)
                            <li>
                                <a {{$lang == Lang::getLocale() ? 'class="active"' : ''}} href="{{URL::to($lang->key)}}">{{$lang->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="weather">
                    <img src="/assets_mygovo/img/cloud.png" alt="">
                    <div class="degrees">+30&deg;</div>
                </div>
            </div>
            @include('mygovo::templates.top-nav')

            <div class="main-title">
                <h1>На одинці з природою з домашнім комфортом</h1>
            </div>

        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="overlay"> </div>
                <div class="item active">
                    <div style="background:url(/assets_mygovo/img/winter.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">

                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="background:url(/assets_mygovo/img/smmmer.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="container">
        <div class="content">
            <div class="title">Фото</div>
            <hr>

            <div class="gallery-box">


                <div class="minor-gallery">
                    <div class="gallery-inner">
                        <a href="/assets_mygovo/img/cot1.png" data-rel="lightcase:myCollection">
                            <div class="gallery-overlay">
                                <div class="gallery-overlay">
                                    <div class="overlay-inner">
                                        <h1 class="overlay-title">Котедж</h1>
                                        <hr>
                                        <div class="discript">3 Спальні. Королівські ліжка.</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <img src="/assets_mygovo/img/cot1.png" alt="1">
                    </div>
                </div>
                <div class="minor-gallery">
                    <div class="gallery-inner">
                        <a href="/assets_mygovo/img/cot1.png" data-rel="lightcase:myCollection">
                            <div class="gallery-overlay">
                                <div class="gallery-overlay">
                                    <div class="overlay-inner">
                                        <h1 class="overlay-title">Котедж</h1>
                                        <hr>
                                        <div class="discript">3 Спальні. Королівські ліжка.</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <img src="/assets_mygovo/img/cot1.png" alt="1">
                    </div>
                </div>
                <div class="minor-gallery">
                    <div class="gallery-inner">
                        <a href="/assets_mygovo/img/cot1.png" data-rel="lightcase:myCollection">
                            <div class="gallery-overlay">
                                <div class="gallery-overlay">
                                    <div class="overlay-inner">
                                        <h1 class="overlay-title">Котедж</h1>
                                        <hr>
                                        <div class="discript">3 Спальні. Королівські ліжка.</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <img src="/assets_mygovo/img/cot1.png" alt="1">
                    </div>
                </div>
                <div class="minor-gallery">
                    <div class="gallery-inner">
                        <a href="/assets_mygovo/img/cot1.png" data-rel="lightcase:myCollection">
                            <div class="gallery-overlay">
                                <div class="gallery-overlay">
                                    <div class="overlay-inner">
                                        <h1 class="overlay-title">Котедж</h1>
                                        <hr>
                                        <div class="discript">3 Спальні. Королівські ліжка.</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <img src="/assets_mygovo/img/cot1.png" alt="1">
                    </div>
                </div>
                <div class="minor-gallery">
                    <div class="gallery-inner">
                        <a href="/assets_mygovo/img/cot1.png" data-rel="lightcase:myCollection">
                            <div class="gallery-overlay">
                                <div class="gallery-overlay">
                                    <div class="overlay-inner">
                                        <h1 class="overlay-title">Котедж</h1>
                                        <hr>
                                        <div class="discript">3 Спальні. Королівські ліжка.</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <img src="/assets_mygovo/img/cot1.png" alt="1">
                    </div>
                </div>
                <div class="minor-gallery">
                    <div class="gallery-inner">
                        <a href="/assets_mygovo/img/cot1.png" data-rel="lightcase:myCollection">
                            <div class="gallery-overlay">
                                <div class="gallery-overlay">
                                    <div class="overlay-inner">
                                        <h1 class="overlay-title">Котедж</h1>
                                        <hr>
                                        <div class="discript">3 Спальні. Королівські ліжка.</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <img src="/assets_mygovo/img/cot1.png" alt="1">
                    </div>
                </div>
                <div class="minor-gallery">
                    <div class="gallery-inner">
                        <a href="/assets_mygovo/img/cot1.png" data-rel="lightcase:myCollection">
                            <div class="gallery-overlay">
                                <div class="gallery-overlay">
                                    <div class="overlay-inner">
                                        <h1 class="overlay-title">Котедж</h1>
                                        <hr>
                                        <div class="discript">3 Спальні. Королівські ліжка.</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <img src="/assets_mygovo/img/cot1.png" alt="1">
                    </div>
                </div>


            </div>
            <div class="title">Відео</div>
            <hr>
            <div class="video"><iframe width="640" height="360" src="https://www.youtube.com/embed/uKZZWROQBFo?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>
            <div class="video"><iframe width="640" height="360" src="https://www.youtube.com/embed/uKZZWROQBFo?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>

        </div>
        <div class="instagram">
            <div class="insta_overlay"></div>
            <div class="insta_content">
                <a href="#hotel_migovo_dolina"></a>
                <div id="owl-example" class="owl-carousel">
                    <div class="insta_block">
                        <div class="insta-overlay">
                            <a href="" class="like"></a><div class="like-count">39<span>&nbsp;-</span></div>
                            <a href="" class="coment"></a><div class="coment-count">0<span>&nbsp;-</span></div>
                        </div>
                        <img src="/assets_mygovo/img/insta1.jpg" alt="">
                    </div>

                    <div class="insta_block">
                        <div class="insta-overlay">
                            <a href="" class="like"></a><div class="like-count">39<span>&nbsp;-</span></div>
                            <a href="" class="coment"></a><div class="coment-count">0<span>&nbsp;-</span></div>
                        </div>
                        <img src="/assets_mygovo/img/insta2.jpg" alt="">
                    </div>

                    <div class="insta_block">
                        <div class="insta-overlay">
                            <a href="" class="like"></a><div class="like-count">39<span>&nbsp;-</span></div>
                            <a href="" class="coment"></a><div class="coment-count">0<span>&nbsp;-</span></div>
                        </div>
                        <img src="/assets_mygovo/img/insta3.jpg" alt="">
                    </div>

                    <div class="insta_block">
                        <div class="insta-overlay">
                            <a href="" class="like"></a><div class="like-count">39<span>&nbsp;-</span></div>
                            <a href="" class="coment"></a><div class="coment-count">0<span>&nbsp;-</span></div>
                        </div>
                        <img src="/assets_mygovo/img/insta4.jpg" alt="">
                    </div>

                    <div class="insta_block">
                        <div class="insta-overlay">
                            <a href="" class="like"></a><div class="like-count">39<span>&nbsp;-</span></div>
                            <a href="" class="coment"></a><div class="coment-count">0<span>&nbsp;-</span></div>
                        </div>
                        <img src="/assets_mygovo/img/insta3.jpg" alt="">
                    </div>

                    <div class="insta_block">
                        <div class="insta-overlay">
                            <a href="" class="like"></a><div class="like-count">39<span>&nbsp;-</span></div>
                            <a href="" class="coment"></a><div class="coment-count">0<span>&nbsp;-</span></div>
                        </div>
                        <img src="/assets_mygovo/img/insta2.jpg" alt="">
                    </div>


                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')

    <script src="{{URL::asset('/assets_mygovo/js/jquery-2.1.4.js')}}"></script>

    <script src="{{URL::asset('/assets_mygovo/js/bootstrap.js')}}"></script>
    <script src="{{URL::asset('/assets_mygovo/js/owl.carousel.js')}}"></script>
    <script src="{{URL::asset('/assets_mygovo/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{URL::asset('/assets_mygovo/js/lightcase.js')}}"></script>

    <script src="{{URL::asset('/assets_mygovo/js/script.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('a[data-rel^=lightcase]').lightcase({
                swipe:true
            });
        });
    </script>

    <script>
        $("#owl-example").owlCarousel({

            // Most important owl features
            items : 4,
            itemsCustom : false,
            itemsDesktop : [1199,4],
            itemsDesktopSmall : [980,4],
            itemsTablet: [768,3],
            itemsTabletSmall: false,
            itemsMobile : [479,2],
            singleItem : false,
            itemsScaleUp : false,

            //Basic Speeds
            slideSpeed : 200,
            paginationSpeed : 800,
            rewindSpeed : 1000,

            //Autoplay
            autoPlay : false,
            stopOnHover : false,

            // Navigation
            navigation : false,
            navigationText : ["prev","next"],
            rewindNav : true,
            scrollPerPage : false,

            //Pagination
            pagination : true,
            paginationNumbers: false,

            // Responsive 
            responsive: true,
            responsiveRefreshRate : 200,
            responsiveBaseWidth: window,

            // CSS Styles
            baseClass : "owl-carousel",
            theme : "owl-theme",

            //Lazy load
            lazyLoad : false,
            lazyFollow : true,
            lazyEffect : "fade",

            //Auto height
            autoHeight : false,

            //JSON 
            jsonPath : false,
            jsonSuccess : false,

            //Mouse Events
            dragBeforeAnimFinish : true,
            mouseDrag : true,
            touchDrag : true,

            //Transitions
            transitionStyle : false,

            // Other
            addClassActive : false,

            //Callbacks
            beforeUpdate : false,
            afterUpdate : false,
            beforeInit: false,
            afterInit: false,
            beforeMove: false,
            afterMove: false,
            afterAction: false,
            startDragging : false,
            afterLazyLoad : false

        })
    </script>
@endsection