@extends('mygovo::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('mygovo::templates.top-nav')
    @include('mygovo::templates.top-header')

    <div class="row news-info-row">
        <div class="content">
            <h2>{{$data->text->caption}}</h2>
            <div class="breadcrumbs">
                <a href="{{URL::route('mygovo.index')}}">{{Lang::get('mygovo::common.main')}}</a><div class="breadcrumbs-circle"></div>
                <a href="{{URL::to('news')}}">{{Lang::get('mygovo::common.news.title')}}</a><div class="breadcrumbs-circle"></div>
            </div>
            <p class="news-date">{{$data->date}}</p>
        </div>
    </div>

    <div class="row one-page-discription-row">
        <div class="one-page-slider">
            <div class="swiper-container swiper-one-page-image">
                <div class="swiper-wrapper">
                    @foreach($data->gallery as $item)
                        <div class="swiper-slide one-page-image-slide">
                            <img src="{{$item->media ? $item->media->path : ''}}" alt="">
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination one-page-image-pagination"></div>
            </div>
            <div class="swiper-button-next one-page-image-button-next"></div>
            <div class="swiper-button-prev one-page-image-button-prev"></div>
        </div>

        <div class="content">
            {!! $data->text->text !!}
        </div>
    </div>

    @include('mygovo::templates.footer')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/swiper.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>

    <!-- Initialize Swiper -->
    <script type="text/javascript">
        var swiper = new Swiper('.swiper-one-page-image', {
            pagination: '.one-page-image-pagination',
            paginationClickable: true,
            nextButton: '.one-page-image-button-next',
            prevButton: '.one-page-image-button-prev',
            spaceBetween: 30,
            loop: true,
            effect: 'fade',
            autoplay: 4000
        });
    </script>
@endsection