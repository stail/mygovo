<div id="popup-info" class="popup-send-form popup-send-form-order">
    <div class="close"></div>
    <div class="popup-thanks">
        <img src="{{URL::asset('img/icons/path-16-copy-14.png')}}" alt="">
        <h2>{{Lang::get('mygovo::common.popup-info.thanks')}}</h2>
        <p>{{Lang::get('mygovo::common.popup-info.manager')}}</p>
        <p class="thanks-close">{{Lang::get('mygovo::common.popup-info.close')}}</p>
    </div>

    <h2>{{Lang::get('mygovo::common.popup-info.title')}}</h2>
    <form action="{{URL::route('mygovo.submit')}}" name="contact_form" id="popup-form-3">
        <input type="hidden" name="_token" value="{{Session::token()}}">
        <input type="hidden" name="popup[page]" value="{{$data->text->caption}}">
        <input type="hidden" name="popup[form]" value="{{Lang::get('mygovo::common.popup-info.title')}}">

        <input class="required" type="text" placeholder="{{Lang::get('mygovo::common.popup-info.name')}}" name="popup[name]" data-name="{{Lang::get('mygovo::common.popup-info.name')}}">
        <input class="phone-mask required" type="text" placeholder="{{Lang::get('mygovo::common.popup-info.phone')}}" name="popup[phone]" data-name="{{Lang::get('mygovo::common.popup-info.phone')}}">
        <input class="required" type="text" placeholder="{{Lang::get('mygovo::common.popup-info.email')}}" name="popup[email]" data-name="{{Lang::get('mygovo::common.popup-info.email')}}">

        <input class="green-btn no-active" type="submit" value="{{Lang::get('mygovo::common.popup-info.submit')}}" disabled>
    </form>
    <div class="help" id="for_dummies">{{Lang::get('mygovo::common.popup-info.left')}} «
        <div class="list"></div> »
    </div>
</div>