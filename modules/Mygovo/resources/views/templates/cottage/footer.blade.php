<footer>
    <div class="content">
        <div class="footer-logo">
            <img src="../img/%D0%B4%D0%BD3.png" alt="">
        </div>
        <div class="addres">Україна, Чернівецька обл.,
            Вижницький р-н, с. Мигово</div>

        <div class="tel">
            <p>+3 8 (050) 536-88-25</p>
            <p>+3 8 (050) 388-13-18</p>
        </div>
        <div class="mail"><a href="">hoteldolina@gmail.com</a></div>
        <div class="social">
            <div class="soc-inner">
                <a href=""><img src="../img/instagram.png" alt="Instagram"></a>
                <a href=""><img src="../img/facebook.png" alt="Facebook"></a>
                <a href=""><img src="../img/vk.png" alt="Vkontakte"></a>
            </div>
        </div>
    </div>
    <div class="subfooter">
        © Сopyright Долина Миколая 2015р
    </div>
</footer>