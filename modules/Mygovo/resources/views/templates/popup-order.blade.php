<div id="popup-send-form-order" class="popup-send-form popup-send-form-order">
    <div id="close-order" class="close"></div>
    <div class="popup-thanks">
        <img src="{{URL::asset('img/icons/path-16-copy-14.png')}}" alt="">
        <h2>{{Lang::get('mygovo::common.popup-order.thanks')}}</h2>
        <p>{{Lang::get('mygovo::common.popup-order.manager')}}</p>
        <p class="thanks-close">{{Lang::get('mygovo::common.popup-order.close')}}</p>
    </div>

    <h2>{{Lang::get('mygovo::common.popup-order.title')}}</h2>
    <form action="{{URL::route('mygovo.submit')}}" name="contact_form" id="popup-form-2">
        <input type="hidden" name="_token" value="{{Session::token()}}">
        <input type="hidden" name="popup[page]" value="{{$data->text->caption}}">
        <input type="hidden" name="popup[form]" value="{{Lang::get('mygovo::common.popup-order.title')}}">
        <input type="hidden" name="popup[title]" value="">

        <input class="required" type="text" placeholder="{{Lang::get('mygovo::common.popup-order.name')}}" name="popup[name]" data-name="{{Lang::get('mygovo::common.popup-form.name')}}">
        <input class="phone-mask required" type="text" placeholder="{{Lang::get('mygovo::common.popup-order.phone')}}" name="popup[phone]" data-name="{{Lang::get('mygovo::common.popup-form.phone')}}">
        <input class="required" type="text" placeholder="{{Lang::get('mygovo::common.popup-order.email')}}" name="popup[email]" data-name="{{Lang::get('mygovo::common.popup-form.email')}}">

        <input class="green-btn no-active" type="submit" value="{{Lang::get('mygovo::common.popup-order.submit')}}" disabled>
    </form>
    <div class="help" id="for_dummies">{{Lang::get('mygovo::common.popup-order.left')}} «
        <div class="list"></div> »
    </div>
</div>