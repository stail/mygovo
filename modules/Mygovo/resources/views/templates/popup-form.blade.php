<div id="popup-send-form" class="popup-send-form">
    <div id="close" class="close"></div>
    <div class="popup-thanks">
        <img src="{{URL::asset('img/icons/path-16-copy-14.png')}}" alt="">
        <h2>{{Lang::get('mygovo::common.popup-form.thanks')}}</h2>
        <p>{{Lang::get('mygovo::common.popup-form.manager')}}</p>
        <p class="thanks-close">{{Lang::get('mygovo::common.popup-form.close')}}</p>
    </div>

    <h2>{{Lang::get('mygovo::common.popup-form.title')}}</h2>
    <form action="{{URL::route('mygovo.submit')}}" name="contact_form" id="popup-form-1">
        <input type="hidden" name="_token" value="{{Session::token()}}">
        <input type="hidden" name="popup[page]" value="{{$data->text->caption}}">
        <input type="hidden" name="popup[form]" value="{{Lang::get('mygovo::common.popup-form.title')}}">

        <input class="required" type="text" placeholder="{{Lang::get('mygovo::common.popup-form.name')}}" name="popup[name]" data-name="{{Lang::get('mygovo::common.popup-form.name')}}">
        <input class="phone-mask required" type="text" placeholder="{{Lang::get('mygovo::common.popup-form.phone')}}" name="popup[phone]" data-name="{{Lang::get('mygovo::common.popup-form.phone')}}">
        <input class="required" type="text" placeholder="{{Lang::get('mygovo::common.popup-form.email')}}" name="popup[email]" data-name="{{Lang::get('mygovo::common.popup-form.email')}}">

        <input class="green-btn no-active" type="submit" value="{{Lang::get('mygovo::common.popup-form.submit')}}" disabled>
    </form>
    <div class="help" id="for_dummies">{{Lang::get('mygovo::common.popup-form.left')}} «
        <div class="list"></div> »
    </div>
</div>