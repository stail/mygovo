<div class="row row-top-header">
    <div class="content">
        <div class="logo">
            <a href="{{URL::route('mygovo.index')}}">
                <img src="{{URL::asset('img/logo-copy.png')}}" alt="Mygovo Logo">
            </a>
        </div>
    </div>
</div>