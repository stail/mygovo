<div class="nav-block">
    <div class="logo"><img src="/assets_mygovo/img/%D0%B4%D0%BD4.png" alt="Долина Миколая"></div>
    <div class="menu-bars"></div>
    <nav>
        <div class="close"></div>
        <ul>
            <li><a href="/">Головна</a></li>
            {{--<li><a href="/catalog/">Будиночки</a></li>--}}
            <?php $product_top_link = \Modules\Mygovo\Models\Product::all();?>
            <li><a href="{{ URL::to($product_top_link[0]->link) }}">Будиночки</a></li>
            <li><a href="/services/">Послуги</a></li>
            <li><a href="/gallery/">Галерея</a></li>
            <li><a href="/blog/">Блог</a></li>
            <li><a href="/assets_mygovo/html/contacts.html">Контакти</a></li>
        </ul>
    </nav>
</div>