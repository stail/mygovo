@extends('mygovo::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
    <link rel="stylesheet" href="/assets_mygovo/css/service.css">
@endsection
@section('main')
    <header>
        <div class="content">
            <div class="preheader">
                <div class="head-phone">
                    <p>+3 8 (050) 536-88-25</p></div>
                <div class="head-position"><a href="contacts.html">Як нас знайти?</a></div>
                <div class="menu-bars"></div>
                <div class="lang">
                    <ul>
                        @foreach($languages as $lang)
                            <li>
                                <a {{$lang == Lang::getLocale() ? 'class="active"' : ''}} href="{{URL::to($lang->key)}}">{{$lang->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="weather">
                    <img src="/assets_mygovo/img/cloud.png" alt="">

                    <div class="degrees">+30&deg;</div>
                </div>
            </div>
            @include('mygovo::templates.top-nav')

            <div class="main-title">
                <h1>На одинці з природою з домашнім комфортом</h1>
            </div>
            <div class="cottage-navigation">
                <a href="" title="Попередній котедж"><img src="/assets_mygovo/img/prev-arrow.png" alt="prev"></a>
                <a href="" title="Наступний котедж"><img src="/assets_mygovo/img/next-arrow.png" alt="next"></a>
            </div>
        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <!--
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
              </ol>
            -->
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="overlay"></div>
                <div class="item active">
                    <div style="background:url(/assets_mygovo/img/winter.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div style="background:url(/assets_mygovo/img/smmmer.jpg) center center;
          background-size:cover;" class="slider-size">
                        <div class="carousel-caption">
                            <!--                <h3</h3>-->
                            <!--                <p></p>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left and right controls    -->
            <!--
                          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="lnr lnr-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
            -->
        </div>
    </header>
    <div id="container">
        <div class="content">
            <div class="title">Послуги та розваги</div>
            <hr>
            <div class="service-block">

                <div class="servise-discription">
                    <div class="discript-title">Велосипеди
                        <!--                    <hr>-->
                    </div>

                    <div class="text">Любители экстремального спорта могут покататься по горным дорогам на велосипеде. Это весёлый и увлекательный способ транспортировки на открытом воздухе, при этом Вы сполна сможете насладиться чудесными пейзажами Карпат.</div>
                </div>
                <div class="disc-image"><img src="../img/bike.png" alt=""></div>
            </div>

            <div class="service-block">

                <div class="servise-discription">
                    <div class="discript-title">Лыжи и сноуборд</div>
                    <!--                    <hr>-->
                    <div class="text">
                        В нашем комплексе «Долина Николая» есть лыжная мини – трасса, длиною 150 метров на которой вы получите массу удовольствия, катаясь на лыжах, сноубордах, сноутюбах и санках. Для продвинутых лыжников в 10 минутах от нашего комплекса ДН расположен горнолыжный комплекс Мигово. Тут прекрасные лыжные трассы для разной сложности. Для начинающих есть лыжная школа, и тренировочные горки. Снег тут выпадает рано, и лежит он долго. Кстати, убедиться, что на склонах достаточно снега, можно через веб-камеру Мигово, установленную возле лыжной трассы.</div>
                </div>
                <div class="disc-image"><img src="../img/9.jpg" alt=""></div>
            </div>

            <div class="service-block">

                <div class="servise-discription">
                    <div class="discript-title">Баня
                        <!--                    <hr>-->
                    </div>

                    <div class="text">Рекомендуем Вам посетить нашу Баню на Дровах в коттеджном комплексе Долина Николая. Она сделана с натуральной горной липы по Финской технологии, которая придает неповторимый и натуральный аромат. Баня помогает отвлечься от всех проблем, снять длительною усталость и при этом дает мощный импульс положительных эмоций, прекрасного самочувствия, хорошего настроения. А это неиссякаемый источник здоровья</div>
                </div>
                <div class="disc-image"><img src="../img/4H-3_0mZJUY-1024x764.jpg" alt=""></div>
            </div>

            <div class="service-block">

                <div class="servise-discription">
                    <div class="discript-title">Басейн</div>
                    <!--                    <hr>-->
                    <div class="text">
                        Индивидуальный открытий бассейн для тех, кто проживает в нашем коттедже в Мигово. Вы получите массу эмоций и удовольствия. (7.3 на 3.6 м.)</div>
                </div>
                <div class="disc-image"><img src="../img/bike.png" alt=""></div>
            </div>


        </div>

    </div>
@endsection
