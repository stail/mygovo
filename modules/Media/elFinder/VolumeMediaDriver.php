<?php namespace Modules\Media\elFinder;

use Modules\Media\Models\Media as MediaModel;
use elFinder;
use Illuminate\Database\Eloquent\Builder;

class VolumeMediaDriver extends \elFinderVolumeDriver
{
    /**
     * Root directory path
     *
     * @var string
     **/
    protected $root = 'files';
    /**
     * Driver id
     * Must be started from letter and contains [a-z0-9]
     * Used as part of volume id
     *
     * @var string
     **/
    protected $driverId = 'md';
    protected $ownerId = null;

    public function __construct()
    {
        $this->ownerId = \Auth::user()->id;
    }

    /**
     * @return Builder
     */
    protected function query()
    {
        return MediaModel::query(); //->where('owner_id', '=', $this->ownerId);
    }

    public function mount(array $opts)
    {
        // If path is not set, use the root
        if (!isset($opts['path']) || $opts['path'] === '') {
            $opts['path'] = $this->root;
        }

        return parent::mount($opts);
    }

    /**
     * Recursive files search
     *
     * @param  string $path dir path
     * @param  string $q search string
     * @param  array $mimes
     *
     * @return array
     * @author Dmitry (dio) Levashov
     **/
    protected function doSearch($path, $q, $mimes)
    {
        $result = [];

        if (!empty($q)) {
            foreach (

                $this->query()
                    ->whereIn('type', ['file'])
                    ->where('caption', 'like', "%$q%")
                    ->get() as $image
            ) {
                $result[] = $this->stat('search/'.$image->getKey());
            }
        }

        return $result;
    }

    /**
     * Save uploaded file.
     * On success return array with new file stat and with removed file hash (if existed file was replaced)
     *
     * @param  Resource $fp file pointer
     * @param  string $dst destination folder hash
     * @param  string $src file name
     * @param  string $tmpname file tmp name - required to detect mime type
     *
     * @return array|false
     * @author Dmitry (dio) Levashov
     **/
    //    public function upload($fp, $dst, $name, $tmpname)
    //    {
    //        if ($this->commandDisabled('upload')) {
    //            return $this->setError(elFinder::ERROR_PERM_DENIED);
    //        }
    //
    //        if (($dir = $this->dir($dst)) == false) {
    //            return $this->setError(elFinder::ERROR_TRGDIR_NOT_FOUND, '#'.$dst);
    //        }
    //
    //        if (!$dir['write']) {
    //            return $this->setError(elFinder::ERROR_PERM_DENIED);
    //        }
    //
    //        if (!$this->nameAccepted($name)) {
    //            return $this->setError(elFinder::ERROR_INVALID_NAME);
    //        }
    //
    //        $mime = $this->mimetype($this->mimeDetect == 'internal' ? $name : $tmpname, $name);
    //        if ($mime == 'unknown' && $this->mimeDetect == 'internal') {
    //            $mime = \elFinderVolumeDriver::mimetypeInternalDetect($name);
    //        }
    //
    //        // logic based on http://httpd.apache.org/docs/2.2/mod/mod_authz_host.html#order
    //        $allow = $this->mimeAccepted($mime, $this->uploadAllow, null);
    //        $deny = $this->mimeAccepted($mime, $this->uploadDeny, null);
    //        $upload = true; // default to allow
    //        if (strtolower($this->uploadOrder[0]) == 'allow') { // array('allow', 'deny'), default is to 'deny'
    //            $upload = false; // default is deny
    //            if (!$deny && ($allow === true)) { // match only allow
    //                $upload = true;
    //            }// else (both match | no match | match only deny) { deny }
    //        } else { // array('deny', 'allow'), default is to 'allow' - this is the default rule
    //            $upload = true; // default is allow
    //            if (($deny === true) && !$allow) { // match only deny
    //                $upload = false;
    //            } // else (both match | no match | match only allow) { allow }
    //        }
    //        if (!$upload) {
    //            return $this->setError(elFinder::ERROR_UPLOAD_FILE_MIME);
    //        }
    //
    //        $tmpsize = sprintf('%u', filesize($tmpname));
    //        if ($this->uploadMaxSize > 0 && $tmpsize > $this->uploadMaxSize) {
    //            return $this->setError(elFinder::ERROR_UPLOAD_FILE_SIZE);
    //        }
    //
    //        $dstpath = $this->decode($dst);
    //
    //
    //        $stat = [
    //            'mime'   => $mime,
    //            'width'  => 0,
    //            'height' => 0,
    //            'size'   => $tmpsize
    //        ];
    //
    //        // $w = $h = 0;
    //        if (strpos($mime, 'image') === 0 && ($s = getimagesize($tmpname))) {
    //            $stat['width'] = $s[0];
    //            $stat['height'] = $s[1];
    //        }
    //        $image = MediaModel::fromFile($tmpname, $name, pathinfo($name, PATHINFO_EXTENSION));
    //        $this->clearcache();
    ////        if (($path = $this->saveCE($fp, $dstpath, $name, $stat)) == false) {
    ////            return false;
    ////        }
    //
    //
    //        return $this->stat($dstpath.'/'.basename($image->path()));
    //    }

    protected function getStat(MediaModel $media)
    {
        static $imgMimes = ['image/jpeg', 'image/png', 'image/gif'];
        $stat = [
            'size'   => 0,
            'ts'     => time(),
            'read'   => true,
            'write'  => true,
            'locked' => false,
            'hidden' => false,
            'mime'   => 'directory',
            'name'   => $media->caption,
            'media'  => $media->attributesToArray()
        ];
        if ($media->type !== 'dir') {
            $stat['path'] = $media->path();
            $stat['mime'] = \elFinderVolumeDriver::mimetypeInternalDetect($stat['path']);
            //            $file = storage_path('app'.$stat['path']);
            //                    if (!is_file($file)) {
            //                        $stat = [];
            //                        break;
            //                    }
            $stat['size'] = $media->size;
            $stat['width'] = $media->width;
            $stat['width'] = $media->height;
            $stat['ts'] = $media->created_at->timestamp;

            if (in_array($stat['mime'], $imgMimes)) {
                $stat['url'] = $stat['path'];
                $stat['tmb'] = $media->url(
                    $this->tmbSize,
                    $this->tmbSize,
                    $this->options['tmbCrop'] ? 'c' : 'r'
                );
            }
        };

        return $stat;
    }

    /**
     * Return stat for given path.
     * Stat contains following fields:
     * - (int)    size    file size in b. required
     * - (int)    ts      file modification time in unix time. required
     * - (string) mime    mimetype. required for folders, others - optionally
     * - (bool)   read    read permissions. required
     * - (bool)   write   write permissions. required
     * - (bool)   locked  is object locked. optionally
     * - (bool)   hidden  is object hidden. optionally
     * - (string) alias   for symlinks - link target path relative to root path. optionally
     * - (string) target  for symlinks - link target path. optionally
     *
     * If file does not exists - returns empty array or false.
     *
     * @param  string $path file path
     *
     * @return array|false
     **/
    protected function _stat($path)
    {
        /**
         * @var MediaModel $media
         */
        static $months
        = [
            1  => 'Січень',
            2  => 'Лютий',
            3  => 'Березень',
            4  => 'Квітень',
            5  => 'Травень',
            6  => 'Червень',
            7  => 'Липень',
            8  => 'Серпень',
            9  => 'Вересень',
            10 => 'Жовтень',
            11 => 'Листопад',
            12 => 'Грудень',
        ];

        $stat = [
            'size'   => 0,
            'ts'     => time(),
            'read'   => true,
            'write'  => true,
            'locked' => false,
            'hidden' => false,
            'mime'   => 'directory',
        ];


        $parts = explode('/', $path);

        switch ($parts[0]) {
            case 'search':
                $media = $this->query()
                    ->find($parts[1]);
                if (!$media) {
                    $stat = [];
                    break;
                }
                $stat = $this->getStat($media);
                $stat['alias'] = $path;
                break;
            case 'dates':
                $stat['locked'] = true;
                $stat['write'] = false;
                if (preg_match('/^m(\d+)$/', end($parts), $m)) {
                    $media = $this->query()
                        ->find($m[1]);
                    if (!$media) {
                        $stat = [];
                        break;
                    }
                    $stat = $this->getStat($media);
                    $stat['alias'] = $path;
                } elseif (count($parts) == 1) {
                    $stat['name'] = 'По даті';
                    $stat['path'] = $path;
                } elseif (count($parts) == 3) {
                    $stat['name'] = $months[$parts[2]];
                }
                break;
            case 'latest':
                if (count($parts) == 1) {
                    $stat['name'] = 'Останні';
                    $stat['path'] = $path;
                    $stat['locked'] = true;
                    $stat['write'] = false;
                } else {
                    $media = $this->query()
                        ->find($parts[1]);
                    if (!$media) {
                        $stat = [];
                        break;
                    }
                    $stat = $this->getStat($media);
                    $stat['alias'] = $path;
                }

                break;
            case $this->root:
                if (count($parts) == 1) {
                    $stat['name'] = 'Малюнки';
                    $stat['path'] = $path;
                } else {
                    $media = null;
                    $parent = null;
                    array_shift($parts);
                    do {
                        if ($media = $this->query()
                            ->where('parent_id', '=', $parent)
                            ->find(array_shift($parts))
                        ) {
                            $parent = $media->getKey();
                        }
                    } while (count($parts) && $media);
                    if ($media == null) {
                        $stat = [];
                        break;
                    }
                    $stat = $this->getStat($media);
                    $stat['alias'] = $path;
                }
                break;
            case 'dates':
                break;
            default:
                die($path);
        }

        return $stat;


        // If root, just return from above
        if ($this->root == $path) {
            //$stat['alias'] = $this->root;
            $stat['name'] = $this->root;

            return $stat;
        }

        $parts = explode('/', $path);
        if (count($parts) == 1) {
            return $stat;
        }
        $last = end($parts);
        if (preg_match('/^(?<id>\d+)-[0-9a-f]{32}\./', $last, $m)) {
            /**
             * @var Media $image
             */
            $image = Media::query()->findOrFail($m['id']);
            $file = storage_path('app'.$image->path());
            if (!is_file($file)) {
                return [];
            }

            $stat['mime'] = \elFinderVolumeDriver::mimetypeInternalDetect($image->path());
            $stat['size'] = filesize($file);
            $stat['alias'] = $path;
            $stat['name'] = $image->caption;
            $stat['ts'] = filemtime($file);
            //$stat['alias'] = $image->path();
            $stat['path'] = $image->path();
            $imgMimes = ['image/jpeg', 'image/png', 'image/gif'];
            if (in_array($stat['mime'], $imgMimes)) {
                $stat['url'] = $image->path();
                $stat['tmb'] = $image->url($this->tmbSize, $this->tmbSize, $this->options['tmbCrop'] ? 'c' : 'r');
            }

            return $stat;
        }
        switch ($parts[1]) {
            case 'tags':
                $stat['name'] = 'Теги';
                $stat['alias'] = $path;
                break;
            case 'dates':
                if (isset($parts[4])) {

                } elseif (isset($parts[3])) {
                    $stat['name'] = $months[(int)$parts[3]];
                    $stat['alias'] = $path;
                } elseif (isset($parts[2])) {

                } else {
                    $stat['name'] = 'По даті';
                    $stat['alias'] = $path;
                }
                break;
            case 'latest':
                $stat['name'] = 'Останні';
                $stat['alias'] = $path;
                break;
            default:
                die($path);

                return false;
        }

        return $stat;
    }

    /*********************** paths/urls *************************/

    /**
     * Return parent directory path
     *
     * @param  string $path file path
     *
     * @return string
     * @author Dmitry (dio) Levashov
     **/
    protected function _dirname($path)
    {
        $parts = explode('/', $path);
        array_pop($parts);

        return implode('/', $parts);

        return ($stat = $this->stat($path)) ? ($stat['phash'] ? $this->decode($stat['phash']) : $this->root) : false;
    }

    /**
     * Return file name
     *
     * @param  string $path file path
     *
     * @return string
     * @author Dmitry (dio) Levashov
     **/
    protected function _basename($path)
    {
        $parts = explode('/', $path);

        return array_pop($parts);

        return ($stat = $this->stat($path)) ? $stat['name'] : false;
    }

    /**
     * Join dir name and file name and return full path.
     * Some drivers (db) use int as path - so we give to concat path to driver itself
     *
     * @param  string $dir dir path
     * @param  string $name file name
     *
     * @return string
     * @author Dmitry (dio) Levashov
     **/
    protected function _joinPath($dir, $name)
    {
        return $dir.DIRECTORY_SEPARATOR.$name;
    }

    /**
     * Return normalized path
     *
     * @param  string $path file path
     *
     * @return string
     * @author Dmitry (dio) Levashov
     **/
    protected function _normpath($path)
    {
        return $path;
    }

    /**
     * Return file path related to root dir
     *
     * @param  string $path file path
     *
     * @return string
     * @author Dmitry (dio) Levashov
     **/
    protected function _relpath($path)
    {
        return $path;
    }

    /**
     * Convert path related to root dir into real path
     *
     * @param  string $path rel file path
     *
     * @return string
     * @author Dmitry (dio) Levashov
     **/
    protected function _abspath($path)
    {
        return $path;
    }

    /**
     * Return fake path started from root dir.
     * Required to show path on client side.
     *
     * @param  string $path file path
     *
     * @return string
     * @author Dmitry (dio) Levashov
     **/
    protected function _path($path)
    {
        return $path;
    }

    /**
     * Return true if $path is children of $parent
     *
     * @param  string $path path to check
     * @param  string $parent parent path
     *
     * @return bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _inpath($path, $parent)
    {
        return $path == $parent || strpos($path, $parent.'/') === 0;
    }

    /***************** file stat ********************/


    /**
     * Return true if path is dir and has at least one childs directory
     *
     * @param  string $path dir path
     *
     * @return bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _subdirs($path)
    {
        do {
            $parts = explode('/', $path);
            switch ($parts[0]) {
                case 'latest':
                    return false;
                    break;
                case 'dates';
                    return count($parts) < 4;
                case $this->root:
                    $parent = null;
                    $parents = [];
                    array_shift($parts);
                    while (count($parts)) {
                        $parents[] = $parent = $this->query()->findOrFail(array_shift($parts))->getKey();
                    }

                    return $this->query()
                        ->where('parent_id', '=', $parent)
                        ->whereIn('type', ['dir'])->count() > 0;
            }
        } while (false);

        return true;
    }

    /**
     * Return object width and height
     * Usualy used for images, but can be realize for video etc...
     *
     * @param  string $path file path
     * @param  string $mime file mime type
     *
     * @return string
     * @author Dmitry (dio) Levashov
     **/
    protected function _dimensions($path, $mime)
    {
        return ($stat = $this->stat($path)) && isset($stat['width']) && isset($stat['height']) ?
            $stat['width'].'x'.$stat['height'] : '';
    }

    /******************** file/dir content *********************/

    /**
     * Return files list in directory
     *
     * @param  string $path dir path
     *
     * @return array
     * @author Dmitry (dio) Levashov
     **/
    protected function _scandir($path)
    {
        $paths = [];
        $parts = explode('/', $path);
        switch ($parts[0]) {
            case 'latest':
                foreach (
                    $this->query()
                        ->whereIn('type', ['file'])
                        ->latest('created_at')
                        ->limit(20)
                        ->get() as $media
                ) {
                    $paths[] = 'latest/'.$media->getKey();
                }
                break;
            case $this->root:
                $parent = null;
                $parents = [$this->root];
                array_shift($parts);
                while (count($parts)) {
                    $parents[] = $parent = $this->query()
                        ->where('parent_id', '=', $parent)
                        ->findOrFail(array_shift($parts))->getKey();
                }
                foreach (
                    $this->query()
                        ->where('parent_id', '=', $parent)
                        ->whereIn('type', ['file', 'dir'])->get() as $media
                ) {
                    $paths[] = implode('/', $parents).'/'.$media->getKey();
                }
                break;
            /*case 'dates':
                if (isset($parts[3])) {
                    $files = MediaModel::day($parts[1], $parts[2], $parts[3]);
                } elseif (isset($parts[2])) {
                    $files = MediaModel::days($parts[1], $parts[2], true);
                } elseif (isset($parts[1])) {
                    $files = MediaModel::months($parts[1], false);
                } else {
                    $files = MediaModel::years(true);
                }
                $paths = array_map(
                    function ($file) use ($path) {
                        return $path.'/'.$file;
                    },
                    $files
                );
                break;*/
        }
        $parts = explode('/', $path);
        if (count($parts) == 1) {
            $paths[] = $this->root;
            //$paths[] = 'dates';
            $paths[] = 'latest';
        }

        return $paths;
    }

    /**
     * Open file and return file pointer
     *
     * @param  string $path file path
     * @param  bool $write open file for writing
     *
     * @return resource|false
     * @author Dmitry (dio) Levashov
     **/
    protected function _fopen($path, $mode = "rb")
    {
        dd(__FUNCTION__);
    }

    /**
     * Close opened file
     *
     * @param  resource $fp file pointer
     * @param  string $path file path
     *
     * @return bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _fclose($fp, $path = '')
    {
        dd(__FUNCTION__);
    }

    /********************  file/dir manipulations *************************/

    /**
     * Create dir and return created dir path or false on failed
     *
     * @param  string $path parent dir path
     * @param string $name new directory name
     *
     * @return string|bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _mkdir($path, $name)
    {
        $parts = explode('/', $path);

        switch ($parts[0]) {
            case $this->root:
                $parent = null;
                $media = null;
                $parents = [$this->root];
                array_shift($parts);
                while (count($parts)) {
                    $media = $this->query()
                        ->where('parent_id', '=', $parent)
                        ->find(array_shift($parts));
                    if ($media == null) {
                        return false;
                    }
                    $parents[] = $parent = $media->getKey();
                }

                $media = MediaModel::create(
                    [
                        'type'      => 'dir',
                        'caption'   => $name,
                        'parent_id' => $parent,
                        'owner_id'  => $this->ownerId
                    ]
                );
                $parents[] = $media->id;

                return implode('/', $parents);
            default:
                return false;
        }
    }

    /**
     * Create file and return it's path or false on failed
     *
     * @param  string $path parent dir path
     * @param string $name new file name
     *
     * @return string|bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _mkfile($path, $name)
    {
    }

    /**
     * Create symlink
     *
     * @param  string $source file to link to
     * @param  string $targetDir folder to create link in
     * @param  string $name symlink name
     *
     * @return bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _symlink($source, $targetDir, $name)
    {
        return false;
    }

    /**
     * Copy file into another file (only inside one volume)
     *
     * @param  string $source source file path
     * @param  string $target target dir path
     * @param  string $name file name
     *
     * @return bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _copy($source, $targetDir, $name)
    {

    }

    /**
     * Move file into another parent dir.
     * Return new file path or false.
     *
     * @param  string $source source file path
     * @param  string $target target dir path
     * @param  string $name file name
     *
     * @return string|bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _move($source, $targetDir, $name)
    {
        $sparts = explode('/', $source);
        $source = null;
        $parent = null;
        array_shift($sparts);
        do {
            if ($source = $this->query()->where('parent_id', '=', $parent)->find(array_shift($sparts))) {
                $parent = $source->getKey();
            }
        } while (count($sparts) && $source);
        if ($source == null) {
            return false;
        }

        $parent = null;
        $parents = [$this->root];
        $tparts = explode('/', $targetDir);
        array_shift($tparts);
        while (count($tparts)) {
            $target = $this->query()
                ->where('parent_id', '=', $parent)
                ->find(array_shift($tparts));
            if ($target == null) {
                return false;
            }
            $parents[] = $parent = $target->getKey();
        }

        $source->caption = $name;
        $source->parent_id = $parent;

        $source->save();
        $parents[] = $source->getKey();

        return implode('/', $parents);
    }

    /**
     * Remove file
     *
     * @param  string $path file path
     *
     * @return bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _unlink($path)
    {
        $parts = explode('/', $path);

        switch ($parts[0]) {
            case 'dates':
                if (preg_match('/^m(\d+)$/', end($parts), $m)
                    && $media = $this->query()
                        ->find($m[1])
                ) {
                    $media->delete();

                    return true;
                }
                break;
            case 'search':
            case 'latest':
                if ($media = $this->query()->find($parts[1])) {
                    $media->delete();

                    return true;
                }
                break;
            case $this->root:
                $parent = null;
                array_shift($parts);
                do {
                    if ($media = $this->query()
                        ->where('parent_id', '=', $parent)
                        ->find(array_shift($parts))
                    ) {
                        $parent = $media->getKey();
                    }
                } while (count($parts) && $media);
                if ($media) {
                    $media->delete();

                    return true;
                }
                break;
        }

        return false;
    }

    /**
     * Remove dir
     *
     * @param  string $path dir path
     *
     * @return bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _rmdir($path)
    {
        $parts = explode('/', $path);

        switch ($parts[0]) {
            case $this->root:
                $parent = null;
                array_shift($parts);
                do {
                    if ($media = $this->query()
                        ->where('parent_id', '=', $parent)
                        ->find(array_shift($parts))
                    ) {
                        $parent = $media->getKey();
                    }
                } while (count($parts) && $media);
                if ($media) {
                    $media->delete();

                    return true;
                }
                break;
        }

        return false;
    }

    /**
     * Create new file and write into it from file pointer.
     * Return new file path or false on error.
     *
     * @param  resource $fp file pointer
     * @param  string $dir target dir path
     * @param  string $name file name
     * @param  array $stat file stat (required by some virtual fs)
     *
     * @return bool|string
     * @author Dmitry (dio) Levashov
     **/
    protected function _save($fp, $dir, $name, $stat)
    {
        $parts = explode('/', $dir);
        if ($parts[0] != $this->root) {
            return false;
        }
        $parent = null;
        array_shift($parts);
        $parents = [$this->root];
        while (count($parts)) {
            if ($media = $this->query()
                ->where('parent_id', '=', $parent)
                ->find(array_shift($parts))
            ) {
                $parents[] = $parent = $media->getKey();
            } else {
                return false;
            }
        }
        $pathinfo = pathinfo($name);
        $media = MediaModel::fromStream(
            $fp,
            $pathinfo['filename'],
            empty($pathinfo['extension']) ? '' : $pathinfo['extension'],
            'file',
            null,
            $this->ownerId
        );
        if ($media) {
            $media->parent_id = $parent;
            $media->save();
            $parents[] = $media->getKey();

            return implode('/', $parents);
        }

        return false;
    }

    /**
     * Get file contents
     *
     * @param  string $path file path
     *
     * @return string|false
     * @author Dmitry (dio) Levashov
     **/
    protected function _getContents($path)
    {

    }

    /**
     * Write a string to a file
     *
     * @param  string $path file path
     * @param  string $content new file content
     *
     * @return bool
     * @author Dmitry (dio) Levashov
     **/
    protected function _filePutContents($path, $content)
    {

    }

    /**
     * Extract files from archive
     *
     * @param  string $path file path
     * @param  array $arc archiver options
     *
     * @return bool
     * @author Dmitry (dio) Levashov,
     * @author Alexey Sukhotin
     **/
    protected function _extract($path, $arc)
    {
    }

    /**
     * Create archive and return its path
     *
     * @param  string $dir target dir
     * @param  array $files files names list
     * @param  string $name archive name
     * @param  array $arc archiver options
     *
     * @return string|bool
     * @author Dmitry (dio) Levashov,
     * @author Alexey Sukhotin
     **/
    protected function _archive($dir, $files, $name, $arc)
    {
    }

    /**
     * Detect available archivers
     *
     * @return void
     * @author Dmitry (dio) Levashov,
     * @author Alexey Sukhotin
     **/
    protected function _checkArchivers()
    {
    }

    protected function move($src, $dst, $name)
    {
        $stat = $this->stat($src);
        $stat['realpath'] = $src;
        $this->rmTmb($stat); // can not do rmTmb() after _move()
        $this->clearcache();

        if ($result = $this->convEncOut(
            $this->_move(
                $this->convEncIn($src),
                $this->convEncIn($dst),
                $this->convEncIn($name)
            )
        )
        ) {
            $this->removed[] = $stat;

            return $result === true ? $this->joinPathCE($dst, $name) : $result;
        }

        return $this->setError(elFinder::ERROR_MOVE, $this->path($src));
    }

    public function _chmod($path, $mode)
    {

    }
}