<?php namespace Modules\Media\Models;

use PhpSpec\Exception\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use SoftDeletes;

    protected $table = 'media';
    protected $fillable = ['hash', 'type', 'caption', 'ext', 'parent_id', 'owner_id', 'options'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = ['options' => 'array'];

    public function path()
    {
        return "/media/{$this->hash[0]}{$this->hash[1]}/{$this->hash[2]}{$this->hash[3]}/{$this->id}-{$this->hash}.{$this->ext}";
    }

    public function url($width = '-', $height = '-', $type = 'r')
    {
        return "/media/{$this->hash[0]}{$this->hash[1]}/{$this->hash[2]}{$this->hash[3]}/{$this->id}-{$this->hash}_{$type}{$width}x{$height}.{$this->ext}";
    }

    public function getUrlAttribute()
    {
        return $this->url();
    }

    public function getPathAttribute()
    {
        return $this->path();
    }

    public function getSizeAttribute()
    {
        $filename = storage_path('app/'.$this->path());

        return is_file($filename) ? filesize($filename) : 0;
    }

    /**
     * @param resource $resource
     * @param string $caption
     * @param null $ext
     * @param string $type
     * @param null $hash
     *
     * @return null|static
     */
    static public function fromStream(
        $resource,
        $caption = '',
        $ext = null,
        $type = 'file',
        $hash = null,
        $owner_id = null,
        $size = 0
    ) {
        $image = self::create(
            [
                'hash'     => $hash ?: md5(uniqid()),
                'ext'      => $ext ?: pathinfo($caption, PATHINFO_EXTENSION),
                'type'     => $type,
                'caption'  => $caption,
                'owner_id' => $owner_id,
                'options'  => [
                    'size' => $size,
                    'mime' => '',
                ]
            ]
        );

        $filename = storage_path('app/'.$image->path());
        $dir = dirname($filename);
        if (is_dir($dir) === false) {
            mkdir($dir, 0755, true);
        }

        if (!$stream = fopen($filename, 'w+')) {
            $image->forceDelete();

            return null;
        }

        //$length = 0;
        while (!feof($resource)) {
            fwrite($stream, fread($resource, 1024), 1024);
        }

        if (!fclose($stream)) {
            $image->forceDelete();

            return null;
        }

        /*try {
            $size = getimagesize($filename, $imageinfo);
            $image->width = $size[0];
            $image->height = $size[1];
            $image->save();
        } catch (\Exception $e) {
            $image->forceDelete();

            return null;
        }*/

        return $image;
    }


    /**
     * @param        $filename
     * @param string $caption
     * @param null $ext
     * @param string $type
     * @param null $hash
     *
     * @return null|static
     */
    static public function fromFile(
        $filename,
        $caption = '',
        $ext = null,
        $type = 'file',
        $hash = null,
        $owner_id = null
    ) {
        $fp = @fopen($filename, 'r');
        $size = filesize($filename);

        return $fp ? static::fromStream(
            $fp,
            $caption,
            $ext ?: pathinfo($filename, PATHINFO_EXTENSION),
            $type,
            $hash,
            $owner_id,
            $size
        ) : null;
    }

    /**
     * @param        $code
     * @param string $caption
     *
     * @return Media|null
     */
    static public function fromYoutube($code)
    {
        $hash = md5('youtube_'.$code);
        $image = Media::query()
            ->where('media.type', '=', 'reserve')
            ->where('media.hash', '=', $hash)
            ->first();

        foreach (['maxresdefault', 'sddefault', 'hqdefault', 'mqdefault'] as $name) {
            $image = $image
                ?: static::fromFile(
                    "http://img.youtube.com/vi/{$code}/{$name}.jpg",
                    'youtube_'.$code,
                    null,
                    'reserve',
                    $hash
                );
        }

        return $image;
    }

    /**
     * @param        $code
     * @param string $caption
     *
     * @return Media|null
     */
    static public function fromSvoboda($code)
    {
        $hash = md5('svoboda_'.$code);
        $image = Media::query()
            ->where('media.type', '=', 'reserve')
            ->where('media.hash', '=', $hash)
            ->first();

        if ($image) {
            return $image;
        }

        try {
            $a
                = file_get_contents(
                "http://radiosvoboda.share.rferl.org/flashembed.aspx?t=vid&id={$code}&w=640&h=363&skin=embeded"
            );
            preg_match(
                '/poster="(?<image>http:\/\/gdb.rferl.org\/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}_tv)(?:_w\d+_h\d+).jpg"/',
                $a,
                $m
            );

            return static::fromFile(
                "{$m['image']}.jpg",
                'Svoboda_'.$code,
                null,
                'reserve',
                $hash
            );
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param        $code
     * @param string $caption
     *
     * @return Media|null
     */
    static public function fromVimeo($code)
    {
        $hash = md5('vimeo_'.$code);
        $image = Media::query()
            ->where('media.type', '=', 'reserve')
            ->where('media.hash', '=', $hash)
            ->first();

        if ($image) {
            return $image;
        }

        try {
            $a = file_get_contents("https://player.vimeo.com/video/{$code}");
            preg_match('/https:\/\/i\.vimeocdn\.com\/video\/(?<image>\d+)\D/', $a, $m);

            return $image
                ?: static::fromFile(
                    "https://i.vimeocdn.com/video/{$m['image']}.jpg",
                    'vimeo_'.$code,
                    null,
                    'reserve',
                    $hash
                );
        } catch (Exception $e) {
            return null;
        }
    }

    static protected function files($from, $to, &$result)
    {
        foreach (
            static::query()
                ->where('type', '=', 'file')
                ->where('created_at', '>=', $from)
                ->where('created_at', '<=', $to)->get() as $image
        ) {
            $result[] = 'm'.$image->getKey();
        }
    }

    static function years()
    {
        $result = array_map(
            function ($row) {
                return $row->y;
            },
            \DB::table((new static)->getTable())
                ->selectRaw('YEAR(created_at) as y')
                ->groupBy('y')
                ->get()
        );

        return $result;
    }

    static function months($year, $files = false)
    {
        $from = date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, $year));
        $to = date('Y-m-d H:i:s', mktime(0, 0, -1, 1, 1, $year + 1));
        $result = array_map(
            function ($row) {
                return $row->m;
            },
            \DB::table((new static)->getTable())
                ->where('created_at', '>=', $from)
                ->where('created_at', '<=', $to)
                ->selectRaw('MONTH(created_at) as m')
                ->groupBy('m')->get()
        );

        if ($files) {
            static::files($from, $to, $result);
        }

        return $result;
    }


    static function days($year, $month, $files = false)
    {
        $from = date('Y-m-d H:i:s', mktime(0, 0, 0, $month, 1, $year));
        $to = date('Y-m-d H:i:s', mktime(0, 0, -1, $month + 1, 1, $year));
        $result = array_map(
            function ($row) {
                return str_pad($row->d, 2, '0', STR_PAD_LEFT);
            },
            \DB::table((new static)->getTable())
                ->where('created_at', '>=', $from)
                ->where('created_at', '<=', $to)
                ->selectRaw('DAY(created_at) as d')
                ->groupBy('d')->get()
        );

        if ($files) {
            static::files($from, $to, $result);
        }

        return $result;
    }

    static function day($year, $month, $day)
    {
        $from = date('Y-m-d H:i:s', mktime(0, 0, 0, $month, $day, $year));
        $to = date('Y-m-d H:i:s', mktime(0, 0, -1, $month, $day + 1, $year));
        $result = [];
        static::files($from, $to, $result);

        return $result;
    }

    /**
     * @param $src
     *
     * @return Media|null
     */
    static function search($src)
    {
        if (preg_match('#(?<id>\d+)-(?<hash>[a-f0-9]{32})#', $src, $m)) {
            $media = Media::withTrashed()->find($m['id']);
            if ($media && $media->hash == $m['hash']) {
                return $media;
            }
        }

        return null;
    }
}
