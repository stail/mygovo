<?php

Route::group(
    [
        'namespace' => '\\Modules\\Media\\Http\\Controllers',
        'prefix'    => 'media',
        'where'     => [
            'h'      => '[a-f0-9]{2}',
            'h2'     => '[a-f0-9]{2}',
            'hash'   => '[a-f0-9]{32}',
            'id'     => '\d+',
            'type'   => 'r|c',
            'width'  => '(\d+|-)',
            'height' => '(\d+|-)',
            'ext'    => 'jpg|JPG|png|PNG|pdf|PDF|jpeg|JPEG'
        ],
    ],
    function (\Illuminate\Routing\Router $router) {
        $router->get('{h}/{h2}/{id}-{hash}.{ext}', 'MediaController@image');
        $router->get('{h}/{h2}/{id}-{hash}_{type}{width}x{height}.{ext}', 'MediaController@resize');
    }
);