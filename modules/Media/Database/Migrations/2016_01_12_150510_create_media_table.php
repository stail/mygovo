<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'media',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('parent_id', false, true)->index()->nullable()->default(null);
                $table->integer('owner_id', false, true)->nullable()->index();
                $table->string('hash', 32)->index();
                $table->string('ext', 5);
                $table->mediumInteger('width', false, true);
                $table->mediumInteger('height', false, true);
                $table->enum('type', ['file', 'dir']);
                $table->string('caption');
                $table->text('options');
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('parent_id')
                    ->references('id')
                    ->on('media')
                    ->onDelete('restrict');

                $table->foreign('owner_id')
                    ->references('id')
                    ->on('acl_users')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('media');
    }

}
