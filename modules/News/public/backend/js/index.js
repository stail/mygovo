jQuery(function ($) {
    'use strict';

    var _Table = new Datatable(),
        _Body = $('body');

    _Table.setAjaxParam('_token', _TOKEN);
    _Table.init({
        src: $('.data-table'),
        dataTable: {
            ordering: false
        }
    });
});