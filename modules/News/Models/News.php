<?php namespace Modules\News\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Media\Models\Media as MediaModel;
use Modules\News\Models\News\Gallery as NewsGallery;
use Modules\News\Models\News\Text as NewsText;

class News extends Model
{
    use SoftDeletes;

    protected $table = 'news';
    protected $fillable = ['slug', 'active', 'media_id'];
    protected $appends = ['link', 'date'];
    protected $dates = ['created_at'];

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(NewsText::class, 'id', 'news_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(NewsText::class, 'news_id', 'id');
    }

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }

    public function gallery()
    {
        return $this->hasMany(NewsGallery::class, 'news_id', 'id');
    }

    public function getLinkAttribute()
    {
        return 'news/'.$this->id.'-'.$this->slug;
    }

    public function getDateAttribute()
    {
        $month = \Lang::get('news::date.month_'.$this->created_at->format('m'));

        return $this->created_at->format('d').' '.$month.' '.$this->created_at->format('Y');
    }
}