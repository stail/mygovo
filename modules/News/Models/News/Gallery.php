<?php namespace Modules\News\Models\News;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media as MediaModel;

class Gallery extends Model
{
    protected $table = 'news_gallery';
    protected $fillable = ['news_id', 'media_id'];

    public $timestamps = false;

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}