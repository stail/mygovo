<?php namespace Modules\News\Models\News;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $table = 'news_texts';
    protected $fillable = [
        'news_id',
        'lang',
        'caption',
        'description',
        'text',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public $timestamps = false;
}