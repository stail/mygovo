<?php namespace Modules\News\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\News\Models\News;

class NewsController extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['datatable', 'ckeditor', 'colorbox']);
        Metronic::menu('news');
    }

    public function getIndex()
    {
        return \View::make('news::backend.index');
    }

    public function postIndex(Request $request)
    {
        $query = News::query();
        $recordsTotal = $recordsFiltered = $query->count();

        $query->limit($request->input('length', 25))
            ->offset($request->input('start', 0));

        $query->orderBy('created_at', 'desc');

        $news = $query->get();

        $data = [];
        foreach ($news as $item) {
            $data[] = [
                $item->text->caption,
                $item->date,
                '<a href="'.route('backend.news.edit', ['id' => $item->id]).'" rel="edit-portlet" class="btn btn-sm green" title="Редагувати"><i class="fa fa-pencil"></i></a> '.
                '<a href="'.route('backend.news.destroy', ['id' => $item->id]).'" class="btn btn-sm red" rel="item-destroy" title="Видалити"><i class="fa fa-trash-o"></i></a>'
            ];
        }

        return [
            'data'            => $data,
            'recordsTotal'    => $recordsTotal,
            'recordsFiltered' => $recordsFiltered
        ];
    }

    public function getCreate()
    {
        return \View::make('news::backend.form');
    }

    public function postCreate(Request $request)
    {
        $model = new News();

        return $this->save($model, $request);
    }

    public function getEdit($id)
    {
        $data = News::query()
            ->findOrFail($id);

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('news::backend.form', ['data' => $data, 'text' => $text]);
    }

    public function postEdit($id, Request $request)
    {
        /**
         * @var News $model
         */
        $model = News::query()
            ->findOrFail($id);

        return $this->save($model, $request);
    }

    public function anyDestroy($id)
    {
        News::query()
            ->findOrFail($id)
            ->delete();

        return \Redirect::route('backend.news.index');
    }

    private function save(News $model, Request $request)
    {
        return \DB::transaction(
            function () use ($model, $request) {
                $data = $request->input('content', []);

                $image = MediaModel::search($request->input('media_path'));
                $data['media_id'] = ($image) ? $image->id : null;
                $slug = empty($data['slug']) ? Str::slug($request->input('text.uk.caption')) : Str::slug($data['slug']);
                $data['slug'] = strtolower($slug);

                $model->fill($data);
                $model->save();

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = News\Text::firstOrNew(['news_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                $model->gallery()->delete();
                $model->gallery()->createMany($request->input('gallery', []));

                return \Redirect::route('backend.news.edit', ['id' => $model->id])
                    ->with('message', 'success');
            }
        );
    }
}