<?php

Route::group(
    ['namespace' => '\\Modules\\News\\Http\\Controllers\\Backend'],
    function (\Illuminate\Routing\Router $router) {

        $router->controller(
            '',
            'NewsController',
            [
                'getIndex'   => 'index',
                'getCreate'  => 'create',
                'getEdit'    => 'edit',
                'anyDestroy' => 'destroy'
            ]
        );
    }
);