<?php

Route::group(
    ['namespace' => '\\Modules\\Partners\\Http\\Controllers\\Backend'],
    function (\Illuminate\Routing\Router $router) {
        $router->controller(
            '',
            'Partners',
            [
                'getIndex'   => 'index',
                'getCreate'  => 'create',
                'getEdit'    => 'edit',
                'anyDestroy' => 'destroy',
            ]
        );
    }
);