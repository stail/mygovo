<?php namespace Modules\Partners\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\Partners\Models\Partner;

class Partners extends Controller
{
    public function __construct()
    {
        Metronic::module(['datatable', 'colorbox']);

        Metronic::menu('partners');
    }

    public function getIndex()
    {
        return \View::make('partners::backend.index');
    }

    public function postIndex(Request $request)
    {
        $model = Partner::query();
        $recordsTotal = $recordsFiltered = $model->count();

        $model->limit($request->input('length', 25))
            ->offset($request->input('start', 0));

        $model->orderBy('created_at', 'desc');

        $collection = $model->get();

        $data = [];
        foreach ($collection as $item) {
            $data[] = [
                $item->title,
                '<a href="'.route('backend.partners.edit', ['id' => $item->id]).'" class="btn btn-sm green" title="Редагувати"><i class="fa fa-pencil"></i></a> '.
                '<a href="'.route('backend.partners.destroy', ['id' => $item->id]).'" class="btn btn-sm red" rel="item-destroy" title="Видалити"><i class="fa fa-trash-o"></i></a>'
            ];
        }

        return [
            'data'            => $data,
            'recordsTotal'    => $recordsTotal,
            'recordsFiltered' => $recordsFiltered
        ];
    }

    public function getCreate()
    {
        return \View::make('partners::backend.form');
    }

    public function postCreate(Request $request)
    {
        $model = new Partner();

        return $this->save($model, $request);
    }

    public function getEdit($id)
    {
        $model = Partner::query()
            ->findOrFail($id);

        return \View::make('partners::backend.form', ['data' => $model]);
    }

    public function postEdit($id, Request $request)
    {
        /**
         * @var Partner $model
         */
        $model = Partner::query()
            ->findOrFail($id);

        return $this->save($model, $request);
    }

    public function anyDestroy($id)
    {
        Partner::query()
            ->findOrFail($id)
            ->delete();

        return \Redirect::route('backend.partners.index');
    }

    private function save(Partner $model, Request $request)
    {
        $data = $request->input('data', []);

        $image = MediaModel::search($request->input('media_path'));
        $data['media_id'] = ($image) ? $image->id : null;

        $model->fill($data);
        $model->save();

        return \Redirect::route('backend.partners.edit', ['id' => $model->id])
            ->with('message', 'success');
    }
}