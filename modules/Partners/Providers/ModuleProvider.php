<?php namespace Modules\Partners\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerConfig();
        $this->registerTranslations();
        $this->registerViews();

        \BackendMenu::addMenu('partners', 'Партнери', route('backend.partners.index'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes(
            [
                __DIR__.'/../config/config.php' => config_path('modules/partners.php'),
            ]
        );
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php',
            'modules.partners'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/partners');

        $sourcePath = __DIR__.'/../resources/views';

        $this->publishes(
            [
                $sourcePath => $viewPath
            ]
        );

        $this->loadViewsFrom([$viewPath, $sourcePath], 'partners');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/partners');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'partners');
        } else {
            $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'partners');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
    }

}
