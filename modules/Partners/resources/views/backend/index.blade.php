@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Партнери</div>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.partners.create')}}">
                            <i class="fa fa-plus"></i> Додати</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover data-table">
                            <thead>
                            <tr class="heading">
                                <th>{{trans('backend::common.caption')}}</th>
                                <th width="100px">{{trans('backend::common.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/partners/backend/js/index.js')}}"></script>
@endsection