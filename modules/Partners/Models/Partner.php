<?php namespace Modules\Partners\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Media\Models\Media as MediaModel;

class Partner extends Model
{
    use SoftDeletes;

    protected $table = 'partners';
    protected $fillable = ['title', 'link', 'media_id'];

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}