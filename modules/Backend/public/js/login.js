jQuery(function ($) {
    $('.login-form').validate({
        focusInvalid: false,
        rules: {
            "email": {
                required: true
            },
            "password": {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        errorPlacement: function (error, element) {
            return true;
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $('input', '.login-form').keypress(function (e) {
        var loginForm = $('.login-form');
        if (e.which == 13) {
            if (loginForm.validate().form()) {
                loginForm.submit();
            }
            return false;
        }
    });
});
