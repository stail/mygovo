jQuery(function ($) {
    'use strict';

    var _Body = $('body');

    _Body.on('click', '[rel="item-destroy"]', function () {
        if (!confirm('Видалити?'))
            return false;
    });
});



function ShowAjaxModal(options) {
    $('.ajax-modal').remove();
    options = $.extend(true, {
        url: "",
        method: 'GET',
        data: {},
        successCallback: function (data) {
            if (data.error) {
                unblockPage();
                /*ShowMessage('#form-' + type, 'danger', data.error, 'icon-warning-sign');*/
            } else {
                unblockPage();
                $(data).insertAfter('.base-portlet');
                $('.ajax-modal').modal();

                if ($.isFunction(options.success)) {
                    options.success();
                }
            }
        },
        errorCallback: function (data) {
            unblockPage();
            /*ShowMessage('#form-' + type, 'danger', 'Сталася помилка завантаження', 'icon-warning-sign');*/
        }
    }, options);

    blockPage();
    $.ajax({
        url: options.url,
        method: options.method,
        data: options.data,
        success: options.successCallback,
        error: options.errorCallback
    });
}

function SubmitAjaxModal(options) {
    options = $.extend(true, {
        url: '',
        method: 'POST',
        data: {},
        successCallback: function (data) {
            if (data.error) {
                unblockBox('.ajax-modal');
                /*ShowMessage(form, 'danger', data.error, 'icon-warning-sign');*/
            } else {
                $('.ajax-modal').modal('hide').remove();
                unblockBox('.ajax-modal');

                if ($.isFunction(options.success)) {
                    options.success();
                }
            }
        },
        errorCallback: function () {
            unblockBox('.ajax-modal');
            /*ShowMessage('#form-' + type, 'danger', 'Сталася помилка завантаження', 'icon-warning-sign');*/
        }
    }, options);

    blockBox('.ajax-modal');
    $.ajax({
        url: options.url,
        method: options.method,
        data: options.data,
        success: options.successCallback,
        error: options.errorCallback
    });
}

function UpdateStatus (options) {
    options = $.extend(true, {
        url: '',
        method: 'POST',
        data: {
            _token: _TOKEN
        },
        successCallback: function (data) {
            if (data.error) {
                unblockPage();
            } else {
                unblockPage();

                if ($.isFunction(options.success)) {
                    options.success(data);
                }
            }
        },
        errorCallback: function () {
            unblockPage();
        }
    }, options);

    blockPage();
    $.ajax({
        url: options.url,
        method: options.method,
        data: options.data,
        success: options.successCallback,
        error: options.errorCallback
    });
}

function blockPage() {
    Metronic.blockUI();
}

function unblockPage() {
    Metronic.unblockUI();
}

function blockBox(target) {
    Metronic.blockUI({
        target: target,
        boxed: true,
        message: 'Завантаження...'
    });
}

function unblockBox(target) {
    Metronic.unblockUI(target);
}

function ShowMessage(container, type, text, icon) {
    Metronic.alert({
        container: container,
        place: 'prepend',
        type: type,
        message: text,
        icon: icon
    });
}

function reorderTable(table) {
    $('tr', table).find('[rel="move-up"]').removeClass('v-hidden');
    $('tr', table).find('[rel="move-down"]').removeClass('v-hidden');
    $('tr:first-child', table).find('[rel="move-up"]').addClass('v-hidden');
    $('tr:last-child', table).find('[rel="move-down"]').addClass('v-hidden');
}







function form_show(name) {
    $('[data-form]').removeClass('show');
    return $('[data-form="' + name + '"]').addClass('show').length > 0;
}
