<?php namespace Modules\Backend\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Backend\Support\Metronic;

class Main extends Controller
{
    public function __construct()
    {
        Metronic::menu('main');
    }

    public function getIndex()
    {
        return \View::make('backend::index');
    }
}