<?php namespace Modules\Backend\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Backend\Models\Language;

class BaseController extends Controller
{
    public function __construct()
    {
        $languages = Language::query()
            ->orderBy('default', 'desc')
            ->orderBy('id', 'asc')
            ->get();

        \View::share('languages', $languages);
    }
}