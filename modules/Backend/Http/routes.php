<?php

use \Illuminate\Routing\Router;

Route::middleware('backend.auth', \Modules\Backend\Http\Middleware\Authenticate::class);

$backend = \Config::get('cms.backend');
if (array_key_exists('domain', $backend)) {
    $domain = config('app.domain');
    $backend['domain'] = str_replace('{$domain}', $domain, $backend['domain']);
}
$backend['as'] = 'backend.';

Route::group($backend, function (Router $router) {
    $router->controller('auth', \Modules\Backend\Http\Controllers\Auth::class, [
        'getLogin' => 'auth',
        'postLogin' => 'auth.login',
        'getLogout' => 'auth.logout'
    ]);

    $router->group(['middleware' => ['backend.auth']], function (Router $router) {
        $router->get('/', ['as' => 'index', 'uses' => 'Modules\Backend\Http\Controllers\Main@getIndex']);

        array_map(function (\Pingpong\Modules\Module $module) use ($router) {
            if (file_exists($path = $module->getExtraPath('Http/backend.php'))) {
                $group = [
                    'prefix' => $module->getAlias(),
                    'as' => $module->getAlias() . '.',
                ];
                $router->group($group, function (Router $router) use ($path) {
                    require($path);
                });
            }
        }, Module::getByStatus(1));
    });
});