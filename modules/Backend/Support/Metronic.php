<?php namespace Modules\Backend\Support;

class Metronic
{
    static protected $plugins = [
        'fonts'           => [
            [],
            [
                '/modules/backend/plugins/font-awesome/css/font-awesome.min.css',
                '/modules/backend/plugins/simple-line-icons/simple-line-icons.min.css',
                '/modules/backend/plugins/linear-icons/linear-icons.css',
                '/modules/backend/plugins/themify-icons/themify-icons.css'
            ]
        ],
        'jquery'          => [['/modules/backend/plugins/jquery.min.js'], []],
        'jquery.validate' => [
            [
                '/modules/backend/plugins/jquery-validation/js/jquery.validate.min.js',
                '/modules/backend/plugins/jquery-validation/js/additional-methods.min.js'
            ],
            []
        ],
        'jquery-ui'       => [
            ['/modules/backend/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js'],
            ['/modules/backend/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css']
        ],
        'bootstrap'       => [
            ['/modules/backend/plugins/bootstrap/js/bootstrap.min.js'],
            ['/modules/backend/plugins/bootstrap/css/bootstrap.min.css']
        ],
        'jquery.uniform'  => [
            ['/modules/backend/plugins/uniform/jquery.uniform.min.js'],
            ['/modules/backend/plugins/uniform/css/uniform.default.css']
        ],
        'theme'           => [
            [
                '/modules/backend/js/metronic.js',
                '/modules/backend/js/layout.js',
                '/modules/backend/js/common.js'
            ],
            [
                '/modules/backend/css/components.css',
                '/modules/backend/css/plugins.css',
                '/modules/backend/css/zet-components.css',
                '/modules/backend/css/zet-layout.css',
                '/modules/backend/css/zet-theme.css'
            ],
            'Metronic.init(); Layout.init();',
        ],
        'login'           => [
            ['/modules/backend/js/login.js'],
            ['/modules/backend/css/login.css']
        ],
        'datatable'       => [
            [
                '/modules/backend/plugins/datatable/media/js/jquery.dataTables.min.js',
                '/modules/backend/plugins/datatable/plugins/bootstrap/dataTables.bootstrap.js',
                '/modules/backend/plugins/datatable/extensions/Scroller/js/dataTables.scroller.min.js',
                '/modules/backend/plugins/datatable/extensions/ColReorder/js/dataTables.colReorder.min.js',
                '/modules/backend/plugins/datatable/extensions/RowReorder/js/dataTables.rowReorder.min.js',
                '/modules/backend/plugins/datatable/extensions/TableTools/js/dataTables.tableTools.min.js',
                '/modules/backend/js/datatable.js'
            ],
            [
                '/modules/backend/plugins/datatable/plugins/bootstrap/dataTables.bootstrap.css',
                '/modules/backend/plugins/datatable/extensions/Scroller/css/dataTables.scroller.min.css',
                '/modules/backend/plugins/datatable/extensions/ColReorder/css/dataTables.colReorder.min.css',
                '/modules/backend/plugins/datatable/extensions/RowReorder/css/dataTables.rowReorder.min.css'
            ]
        ],
        'jquery.blockui'  => [['/modules/backend/plugins/jquery.blockui.min.js'], []],
        'ckeditor'        => [['/modules/backend/plugins/ckeditor/ckeditor.js', '/modules/backend/plugins/ckeditor/adapters/jquery.js'], []],
        'colorbox'        => [['/modules/backend/plugins/colorbox/jquery.colorbox.js'], ['/modules/backend/plugins/colorbox/colorbox.css']],
        'datepicker'      => [
            ['/modules/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'],
            ['/modules/backend/plugins/bootstrap-datepicker/css/datepicker3.css']
        ],
        'fileupload'      => [
            [
                '/modules/backend/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js',
                '/modules/backend/plugins/jquery-file-upload/js/vendor/tmpl.min.js',
                '/modules/backend/plugins/jquery-file-upload/js/vendor/load-image.min.js',
                '/modules/backend/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js',
                '/modules/backend/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js',
                '/modules/backend/plugins/jquery-file-upload/js/jquery.iframe-transport.js',
                '/modules/backend/plugins/jquery-file-upload/js/jquery.fileupload.js',
                '/modules/backend/plugins/jquery-file-upload/js/jquery.fileupload-process.js',
                '/modules/backend/plugins/jquery-file-upload/js/jquery.fileupload-image.js',
                '/modules/backend/plugins/jquery-file-upload/js/jquery.fileupload-audio.js',
                '/modules/backend/plugins/jquery-file-upload/js/jquery.fileupload-video.js',
                '/modules/backend/plugins/jquery-file-upload/js/jquery.fileupload-validate.js',
                '/modules/backend/plugins/jquery-file-upload/js/jquery.fileupload-ui.js'
            ],
            [
                '/modules/backend/plugins/jquery-file-upload/css/jquery.fileupload.css',
                '/modules/backend/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css',
                '/modules/backend/plugins/jquery-file-upload/css/jquery.fileupload-ui.css',
                '/modules/backend/plugins/jquery-file-upload/css/portfolio.css'
            ]
        ]
    ];

    static protected $depends = [
        'login'           => ['jquery.validate'],
        'jquery-ui'       => ['jquery'],
        'bootstrap'       => ['jquery'],
        'jquery.uniform'  => ['jquery'],
        'jquery.validate' => ['jquery'],
        'datatable'       => ['jquery'],
        'theme'           => [
            'fonts',
            'jquery',
            'jquery-ui',
            'bootstrap',
            'jquery.uniform',
            'jquery.blockui',
        ],
    ];

    static protected $css = [];
    static protected $js = [];
    static protected $script = [];
    static protected $modules = [];
    static protected $menu = [null, null];
    static protected $breadcrumbs = [];

    static function module($module)
    {
        if (is_array($module)) {
            array_map([__CLASS__, __METHOD__], $module);
        } else {
            if (!isset(self::$modules[$module])) {
                self::$modules[$module] = $module;
                if (isset(self::$depends[$module])) {
                    foreach (self::$depends[$module] as $m) {
                        self::module($m);
                    }
                }

                if (isset(self::$plugins[$module])) {
                    self::$js = array_merge(self::$js, self::$plugins[$module][0]);
                    self::$css = array_merge(self::$css, self::$plugins[$module][1]);
                    if (isset(self::$plugins[$module][2])) {
                        self::$script[] = self::$plugins[$module][2];
                    }
                }
            }
        }
    }

    static function js()
    {
        return self::$js;
    }

    static function css()
    {
        return self::$css;
    }

    static function script()
    {
        return join(PHP_EOL, self::$script);
    }

    static function init()
    {
        self::module('theme');
    }

    static function menu($menu = null, $subItem = null)
    {
        self::$menu = [$menu, $subItem];
    }

    static function isMenu($menu, $subItem = null)
    {
        return self::$menu[0] == $menu && ($subItem === null || self::$menu[1] == $subItem);
    }

    static function isSubMenu($subItem)
    {
        return self::$menu[1] == $subItem;//&& ($subItem === null || self::$menu[1] == $subItem);
    }

    static function addPath($title, $url = null, array $attributes = [])
    {
        self::$breadcrumbs[] = [$title, $url, $attributes];
    }

    static function breadcrumbs()
    {
        return self::$breadcrumbs;
    }

    static function renderMenu()
    {
        $html = '';
        $menu = config('backend.sidebar');

        foreach ($menu as $item) {
            if ($item['name'] == 'heading') {
                $html .= self::renderMenuHeading($item);
            } else {
                $html .= self::renderMenuItem($item);
            }
        }

        return $html;
    }

    static function renderMenuItem($item)
    {
        $html = '<li'.(self::isMenu($item['name']) ? ' class="active open"' : '').'>'.
            '<a href="'.url($item['url']).'">'.
            '<i class="'.$item['icon'].'"></i>'.
            '<span class="title">'.$item['title'].'</span>'.
            '<span class="selected"></span>'.
            ((array_key_exists('items', $item)) ? '<span class="arrow"></span>' : '').
            '</a>'.
            ((array_key_exists('items', $item)) ? self::renderMenuSubItems($item['items']) : '').
            '</li>';

        return $html;
    }

    static function renderMenuHeading($item)
    {
        $html = '<li class="heading"><h3 class="uppercase">'.$item['title'].'</h3></li>';

        return $html;
    }

    static function renderMenuSubItem($item)
    {
        $html = '<li'.
            (self::isSubMenu($item['name']) ? ' class="active open"' : '').
            '><a href="'.$item['url'].'">'.$item['title'].
            ((array_key_exists('items', $item)) ? '<span class="arrow"></span>' : '').
            '</a>'.
            ((array_key_exists('items', $item)) ? self::renderMenuSubItems($item['items']) : '').
            '</li>';

        return $html;
    }

    static function renderMenuSubItems($items)
    {
        $html = '<ul class="sub-menu">';
        foreach ($items as $item) {
            $html .= self::renderMenuSubItem($item);
        }
        $html .= '</ul>';

        return $html;
    }
}

Metronic::init();