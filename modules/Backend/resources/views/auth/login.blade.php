@extends('backend::layouts.base')
@section('main')
    <body class="login">
    <div class="logo">
        <a href="{{URL::route('backend.index')}}">
            <img src="{{URL::asset('/modules/backend/img/logo.png')}}" alt="zetadmin"/></a>
    </div>
    <div class="content">
        <form class="login-form" action="{{URL::route('backend.auth.login')}}" method="post">
            <input type="hidden" name="_token" value="{{Session::token()}}">

            <h3 class="form-title text-center">{{Lang::get('backend::common.login.title')}}</h3>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control" type="text" name="email" value="{{Request::old('email')}}"
                           placeholder="{{Lang::get('backend::common.login.username')}}">
                </div>
            </div>
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control" type="password" name="password"
                           placeholder="{{Lang::get('backend::common.login.password')}}">
                </div>
            </div>
            <div class="form-actions">
                <label class="checkbox">
                    <input type="checkbox" name="remember" value="1"> {{Lang::get('backend::common.login.remember')}}
                </label>
                <button type="submit"
                        class="btn green-haze pull-right">{{Lang::get('backend::common.login.submit')}}</button>
            </div>
        </form>
    </div>
    <div class="copyright">{{date('Y')}} &copy; ZETADMIN. ZaycevStudio.</div>

    @include('backend::layouts.scripts')
    </body>
@endsection