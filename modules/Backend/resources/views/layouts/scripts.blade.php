@foreach(\Modules\Backend\Support\Metronic::js() as $js)
    <script src="{{$js}}" type="text/javascript"></script>
@endforeach

<script type="text/javascript">
    jQuery(document).ready(function () {
        {!!\Modules\Backend\Support\Metronic::script()!!}
    });
</script>

@yield('scripts')