<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <title>Адміністрування</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css">

    @foreach(\Modules\Backend\Support\Metronic::css() as $css)
        <link href="{{$css}}" rel="stylesheet" type="text/css">
    @endforeach

    <link rel="shortcut icon" type="image/png" href="{{URL::asset('/modules/backend/img/favicon.png')}}">

    <script type="text/javascript">
        var _TOKEN = '{{Session::token()}}';
    </script>
</head>
@yield('main')
</html>