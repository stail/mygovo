<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            @foreach(\BackendMenu::getMenu() as $menu => $item)
                <li @if(\Modules\Backend\Support\Metronic::isMenu($menu)) class="open active" @endif>
                    <a href="{{isset($item['link']) ? $item['link'] : 'javascript:'}}">
                        <i class="fa {{$item['icon']}}"></i>
                        <span class="title">{{$item['label']}}</span>
                        @if (!empty($item['items']))
                            <span class="arrow @if(\Modules\Backend\Support\Metronic::isMenu($menu)) open @endif"></span>
                        @endif
                    </a>
                    @if (!empty($item['items']))
                        <ul class="sub-menu">
                            @foreach($item['items'] as $subitem)
                                <li @if(\Modules\Backend\Support\Metronic::isMenu($menu, $subitem['menu'])) class="active" @endif>
                                    <a href="{{isset($subitem['link']) ? $subitem['link'] : 'javascript:'}}">
                                        <i class="fa {{$subitem['icon']}}"></i>
                                        <span class="title">{{$subitem['label']}}</span>
                                        @if (!empty($subitem['items']))
                                            <span class="arrow @if(\Modules\Backend\Support\Metronic::isMenu($menu)) open @endif"></span>
                                        @endif
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>