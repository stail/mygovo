@extends('backend::layouts.base')
@section('main')
    <body class="page-header-fixed">
    @include('backend::layouts.header')

    <div class="page-container">
        @include('backend::layouts.sidebar')
        @yield('content')
    </div>
    <div class="clearfix"></div>

    @include('backend::layouts.scripts')
    </body>
@stop
