<div class="modal fade ajax-modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            @yield('modal')
        </div>
    </div>
</div>