<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner">
        <div class="page-logo">
            <a href="{{URL::route('backend.index')}}">
                <img src="{{URL::asset('/modules/backend/img/logo.png')}}" alt="logo" class="logo-default"/></a>

            <div class="menu-toggler sidebar-toggler"></div>
            <a href="javascript:" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse"></a>
        </div>

        <div class="hor-menu hidden-xs">
            <ul class="nav navbar-nav">
                {{--
                <li>
                    <a href="{{URL::action('Backend\LeadsController@getIndex')}}">
                        <i class="lnr lnr-cart"></i>
                        <span class="badge badge-default">{{\App\Models\Landing\Lead::query()->where('status', '=', '1')->count()}}</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="icon-users"></i>
                        <span class="badge badge-default">22</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="icon-bubble"></i>
                        <span class="badge badge-default">7</span>
                    </a>
                </li>--}}
            </ul>
        </div>

        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-actions">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <i class="icon-settings"></i></a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{URL::to('/')}}" target="_blank">Перейти на сайт
                                <i class="ti ti-new-window pull-right"></i></a>
                        </li>
                        <li>
                            <a href="{{URL::route('backend.auth.logout')}}">Вийти
                                <i class="lnr lnr-arrow-right pull-right"></i></a>
                        </li>
                        {{--
                        <li>
                            <a href="{{URL::action('LandingPage\PanelsController@getIndex')}}" target="_blank">Інфрачервоні
                                Обігрівачі <i class="ti ti-new-window pull-right"></i></a>
                        </li>
                        <li>
                            <a href="{{URL::action('LandingPage\CeramicController@getIndex')}}" target="_blank">Керамічні
                                Обігрівачі <i class="ti ti-new-window pull-right"></i></a>
                        </li>
                        --}}
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="clearfix"></div>