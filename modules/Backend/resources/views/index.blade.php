@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="base-portlet">
                @include('mygovo::backend.index')
            </div>
        </div>
    </div>
@endsection