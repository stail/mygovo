<h3 class="form-section">Мета-теги</h3>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Заголовок</label>
            <input type="text" class="form-control" name="text[{{$lang->key}}][meta_title]"
                   value="{{isset($text) && array_key_exists($lang->key, $text) ? $text[$lang->key]['meta_title'] : ''}}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Опис</label>
            <input type="text" class="form-control" name="text[{{$lang->key}}][meta_description]"
                   value="{{isset($text) && array_key_exists($lang->key, $text) ? $text[$lang->key]['meta_description'] : ''}}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Ключові слова</label>
            <input type="text" class="form-control" name="text[{{$lang->key}}][meta_keywords]"
                   value="{{isset($text) && array_key_exists($lang->key, $text) ? $text[$lang->key]['meta_keywords'] : ''}}">
        </div>
    </div>
</div>