<?php namespace Modules\Backend\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RabbitCMS\Carrot\Support\PermissionsTrait;

/**
 * Class Group
 *
 * @property int $id
 */
class Group extends Model
{
    use SoftDeletes;
    use PermissionsTrait;

    protected $table = 'acl_groups';
    protected $fillable = ['caption', 'permissions'];
    protected $casts = ['permissions' => 'array'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'acl_groups_users', 'group_id', 'user_id');
    }

    public function getPermissions()
    {
        return $this->permissions;
    }

    protected function setPermissions(array $permissions)
    {
        $this->permissions = $permissions;
    }
}