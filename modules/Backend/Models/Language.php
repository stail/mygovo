<?php namespace Modules\Backend\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'lang';
    protected $fillable = ['key', 'active', 'default', 'caption'];

    public $timestamps = false;
}