<?php namespace Modules\Backend\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use RabbitCMS\Carrot\Support\PermissionsTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    use PermissionsTrait;
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $table = 'acl_users';

    /**
     * @inheritdoc
     */
    protected $fillable = ['email', 'password', 'name', 'active', 'permissions'];

    /**
     * @inheritdoc
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @inheritdoc
     */
    protected $casts = ['permissions' => 'array'];

    /**
     * Merged permissions
     * @var array
     */
    protected $mergedPermissions;

    /**
     * Get groups relation
     * @return BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'acl_groups_users', 'user_id', 'group_id');
    }

    /**
     * Set new user password
     * @param string $password
     * @throws \Exception
     */
    public function setAuthPassword($password)
    {
        if (empty($this->id)) {
            throw new \Exception('Don\'t set password to nonexistent user.');
        }

        $this->update(['password' => static::passwordEncode($password)]);
    }

    /**
     * Returns an array of merged permissions for each group the user is in.
     *
     * @return array
     */
    public function getPermissions()
    {
        /**
         * @var Group $group
         */
        if (!$this->mergedPermissions) {
            $permissions = [[]];

            foreach ($this->groups()->get() as $group) {
                if (!is_array($p = $group->getPermissions())) {
                    continue;
                }

                $permissions[] = $p;
            }
            $permissions[] = $this->permissions;
            $this->mergedPermissions = call_user_func_array('array_merge', $permissions);
        }

        return $this->mergedPermissions;
    }

    /**
     * Encode password
     *
     * @param string $password
     * @return string
     */
    public static function passwordEncode($password)
    {
        return bcrypt($password);
    }
}
