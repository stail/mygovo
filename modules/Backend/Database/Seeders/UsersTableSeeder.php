<?php namespace Modules\Backend\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Backend\Models\Group as GroupModel;
use Modules\Backend\Models\User as UserModel;

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$user = UserModel::create(
			[
				'name'     => 'Super User',
				'email'    => 'admin@domain.com',
				'active'   => 1,
				'password' => bcrypt('admin'),
			]
		);

		$group = GroupModel::create(
			[
				'caption'     => 'Администратор',
				'permissions' => [
					'backend.*' => 1
				]
			]
		);

		$user->groups()->sync([$group->id]);
	}

}