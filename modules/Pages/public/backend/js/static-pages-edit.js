jQuery(function ($) {
    'use strict';

    CKEDITOR.disableAutoInline = true;
    $('.editor').ckeditor({
        filebrowserBrowseUrl: '/elfinder/ckeditor'
    });
});
