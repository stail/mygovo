@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Редагування екрану</h3>

            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <ul class="nav nav-tabs pull-left">
                        @foreach($languages as $lang)
                            <li class="{{$lang->default ? 'active' : ''}}">
                                <a href="#portlet_tab_{{$lang->key}}" data-toggle="tab" aria-expanded="true">
                                    {{$lang->caption}}</a></li>
                        @endforeach
                    </ul>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.pages.screens.index')}}">
                            <i class="fa fa-arrow-left"></i> Назад</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="tab-content">
                            @foreach($languages as $lang)
                                <div class="tab-pane form-body {{$lang->default ? 'active' : ''}}" id="portlet_tab_{{$lang->key}}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Заголовок</label>
                                                <input type="text" class="form-control" name="text[{{$lang->key}}][caption]"
                                                       value="{{array_key_exists($lang->key, $text) ? $text[$lang->key]['caption'] : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Опис</label>
                                                <input type="text" class="form-control" name="text[{{$lang->key}}][description]"
                                                       value="{{array_key_exists($lang->key, $text) ? $text[$lang->key]['description'] : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    @if($data->key == 'contacts')
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Текст</label>
                                                    <textarea class="form-control editor"
                                                          name="text[{{$lang->key}}][text]">{{array_key_exists($lang->key, $text) ? $text[$lang->key]['text'] : ''}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach

                            @if($data->key == 'screen_1')
                                <div class="form-body">
                                    <h3 class="form-section">Додатково</h3>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Код відео</label>
                                                <textarea class="form-control" style="height: 100px;"
                                                          name="additional[video]">@if(array_key_exists('video', $data->additional)){{$data->additional['video']}}@endif</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/pages/backend/js/static-pages-edit.js')}}"></script>
@endsection