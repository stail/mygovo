<?php namespace Modules\Pages\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Pages\Models\Page\Text as TextModel;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = ['key', 'template', 'additional'];
    protected $casts = ['additional' => 'array'];

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(TextModel::class, 'id', 'page_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(TextModel::class, 'page_id', 'id');
    }
}