<?php namespace Modules\Pages\Models\Page;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $table = 'pages_texts';
    protected $fillable = [
        'page_id',
        'lang',
        'caption',
        'description',
        'text',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public $timestamps = false;
}