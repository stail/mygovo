<?php

Route::group(
    ['namespace' => '\\Modules\\Pages\\Http\\Controllers\\Backend'],
    function (\Illuminate\Routing\Router $router) {
        $router->controller(
            'screens',
            'Screens',
            [
                'getIndex' => 'screens.index',
                'getEdit'  => 'screens.edit',
                'postEdit' => 'screens.update'
            ]
        );

        $router->controller(
            'static',
            'StaticPages',
            [
                'getIndex' => 'static.index',
                'getEdit'  => 'static.edit',
                'postEdit' => 'static.update'
            ]
        );
    }
);