<?php namespace Modules\Pages\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Pages\Models\Page as PageModel;

class Screens extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['datatable', 'ckeditor']);
        Metronic::menu('pages', 'screens');
    }

    public function getIndex()
    {
        return \View::make('pages::backend.screens.index');
    }

    public function postIndex(Request $request)
    {
        $query = PageModel::query()
            ->where('type', '=', 'screen')
            ->with(['text']);
        $recordsTotal = $recordsFiltered = $query->count();

        $query->limit($request->input('length', 25))
            ->offset($request->input('start', 0));

        $query->orderBy('created_at', 'desc');

        $pages = $query->get();

        $data = [];
        foreach ($pages as $item) {
            $data[] = [
                $item->text->caption,
                '<a href="'.route('backend.pages.screens.edit', ['id' => $item->id]).'" rel="edit-portlet" class="btn btn-sm green" title="Редагувати"><i class="fa fa-pencil"></i></a>'
            ];
        }

        return [
            'data'            => $data,
            'recordsTotal'    => $recordsTotal,
            'recordsFiltered' => $recordsFiltered
        ];
    }

    public function getEdit($id)
    {
        $data = PageModel::query()
            ->findOrFail($id);

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('pages::backend.screens.edit', ['data' => $data, 'text' => $text]);
    }

    public function postEdit($id, Request $request)
    {
        return \DB::transaction(
            function () use ($id, $request) {
                $model = PageModel::query()
                    ->findOrFail($id);

                $additional = $request->input('additional', []);
                $model->additional = $additional;
                $model->save();

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = PageModel\Text::firstOrNew(['page_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                return \Redirect::route('backend.pages.screens.edit', ['id' => $model->id])
                    ->with('message', 'success');
            }
        );
    }
}