<?php

return [
    'video'          => 'Watch Video',
    'main'           => 'Home',
    'services'       => [
        'title'   => 'Services',
        'price'   => 'Cost of services',
        'type'    => 'SERVICE TYPE',
        'price_2' => 'PRICE'
    ],
    'detail'         => 'Read more',
    'consultation'   => 'Get advice',
    'trust'          => [
        'title'       => 'Why trust us',
        'approach'    => 'Individual approach',
        'personnel'   => 'Qualified staff',
        'equipment'   => 'Modern equipment',
        'problem'     => 'The solution to any problem',
        'reputation'  => 'Excellent reputation',
        'method'      => 'The only method in Ukraine',
        'appointment' => 'Appointment'
    ],
    'beauty'         => [
        'title'       => 'Do not hide your beauty.',
        'image'       => 'Wrap it in a perfect image,',
        'admiration'  => 'admiration all around.',
        'naturel'     => 'Embassy of Beauty and Health Naturel',
        'appointment' => 'Appointment'
    ],
    'achievements'   => 'Our achievements',
    'certificates'   => [
        'title'      => 'Discount cards, certificates',
        'tab_1'      => 'Gift Certificates',
        'tab_2'      => 'Discount card',
        'currency'   => 'UAH',
        'item-title' => 'GIFT CERTIFICATE',
        'buy'        => 'Order',
        'more'       => 'Learn more'
    ],
    'reviews'        => 'Feedback from our customers',
    'news'           => [
        'title' => 'News'
    ],
    'partners'       => [
        'title' => 'Trademarks and partners'
    ],
    'catalog'        => [
        'main-title' => 'Professional makeup',
        'navigate'   => 'To the catalog',
        'currency'   => 'UAH.',
        'title'      => 'Catalog',
        'filter'     => 'Filter',
        'all'        => 'All'
    ],
    'studio'         => [
        'title'    => 'Vadim Zaitsev Studio',
        'by'       => 'Developed by',
        'by-title' => 'Vadim Zaitsev Studio'
    ],
    'contacts-title' => 'Contacts',
    'popup-form'     => [
        'title'   => 'Record on reception',
        'name'    => 'Name',
        'phone'   => 'Phone',
        'email'   => 'E-mail',
        'submit'  => 'Send request',
        'left'    => 'Still specify',
        'thanks'  => 'Thank you, your application is submitted.',
        'manager' => 'In the near future You will contact our manager',
        'close'   => 'Close'
    ],
    'popup-order'    => [
        'title'   => 'Checkout',
        'name'    => 'Name',
        'phone'   => 'Phone',
        'email'   => 'E-mail',
        'submit'  => 'Send request',
        'left'    => 'Still specify',
        'thanks'  => 'Thank you, your application is submitted.',
        'manager' => 'In the near future You will contact our manager',
        'close'   => 'Close'
    ],
    'popup-info'     => [
        'title'   => 'Ask for information',
        'name'    => 'Name',
        'phone'   => 'Phone',
        'email'   => 'E-mail',
        'submit'  => 'Send request',
        'left'    => 'Still specify',
        'thanks'  => 'Thank you, your application is submitted.',
        'manager' => 'In the near future You will contact our manager',
        'close'   => 'Close'
    ]
];