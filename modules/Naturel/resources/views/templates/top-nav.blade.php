<div class="nav-fixed">
    <div class="content">
        <div class="nav-btn-open">
            <div class="opacity"></div>
            <div class="nav-btn-ico"></div>
        </div>
        <div class="lang">
            <ul>
                @foreach($languages as $lang)
                    <li><a {{$lang == Lang::getLocale() ? 'class="active"' : ''}} href="{{URL::to($lang->key)}}">{{$lang->code}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

<div class="nav-field">
    <div class="nav-blur"></div>
    <div class="nav-btn">
        <div class="nav-btn-close">
            <div class="opacity"></div>
            <div class="nav-btn-ico"></div>
        </div>
    </div>
    <nav>
        <ul>
            <li>
                <a class="first-link" href="{{URL::to('services')}}">{{Lang::get('naturel::common.services.title')}}</a>

                <div class="first-line"></div>
                <ul>
                    @foreach($services as $item)
                        <li>
                            <a class="second-link" href="{{URL::to($item->link)}}">{{$item->text->caption}}</a>

                            <div class="second-line"></div>
                        </li>
                    @endforeach
                </ul>
            </li>
            <li>
                <a class="first-link" href="{{URL::to('news')}}">{{Lang::get('naturel::common.news.title')}}</a>

                <div class="first-line"></div>
            </li>
            <li>
                <a class="first-link" href="{{URL::to('catalog')}}">{{Lang::get('naturel::common.catalog.title')}}</a>

                <div class="first-line"></div>
            </li>
            <li>
                <a class="first-link" href="{{URL::to('contacts')}}">{{Lang::get('naturel::common.contacts-title')}}</a>

                <div class="first-line"></div>
            </li>
        </ul>
    </nav>
</div>