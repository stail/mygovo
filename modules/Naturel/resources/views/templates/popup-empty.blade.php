<div class="popup-send-form" id="empty-popup">
    <div class="close"></div>

    <h2>{{Lang::get('naturel::common.certificates.item-title')}}</h2>
    <div class="content"></div>
</div>