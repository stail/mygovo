<div class="row row-top-header">
    <div class="content">
        <div class="logo">
            <a href="{{URL::route('naturel.index')}}">
                <img src="{{URL::asset('img/logo-copy.png')}}" alt="Naturel Logo">
            </a>
        </div>
    </div>
</div>