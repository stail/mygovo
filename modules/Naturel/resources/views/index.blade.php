@extends('naturel::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('naturel::templates.top-nav')


    <div class="row row-header">
        <div class="content-header-bg">
            <div class="blur-bg"></div>
        </div>
        <div class="row row-top-header">
            <div class="content">
                <div class="logo">
                    <img src="{{URL::asset('img/logo-copy.png')}}" alt="Naturel Logo">
                </div>
            </div>
        </div>

        <div class="swiper-container swiper-header">
            <div class="swiper-wrapper">
                <?php $first = true; ?>
                @foreach($slider as $item)
                    <div class="swiper-slide header-slide">
                        <div class="slide {{$first ? '' : 'dark-slide'}}">
                            <div class="content-header-bg" style="background-image: url('{{$item->media ? $item->media->path : ''}}');">
                                <div class="blur-bg"></div>
                            </div>
                            <div class="content">
                                <div class="header-content">
                                    <h1>{{$item->text->caption}}</h1>

                                    <h3 class="header-text">{{$item->text->description}}</h3>
                                    <a href="javascript:void(0);">
                                        <div class="btn header-btn" onclick="show();">{{Lang::get('naturel::common.trust.appointment')}}</div>
                                    </a>
                                    <div class="header-video" onclick="showVideo()">
                                        <div class="video-icon"></div>
                                        <div class="video-text">
                                            <p>{{Lang::get('naturel::common.video')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $first = false; ?>
                @endforeach
            </div>
            <div class="swiper-pagination header-pagination"></div>
            <div class="swiper-button-next header-button-next"></div>
            <div class="swiper-button-prev header-button-prev"></div>
        </div>

        {{--<div class="content">
            <div class="header-content">
                <h1>{{$screen_1->text->caption}}</h1>

                <h3 class="header-text">{{$screen_1->text->description}}</h3>

                <div class="header-video" onclick="showVideo()">
                    <div class="video-icon"></div>
                    <div class="video-text">
                        <p>{{Lang::get('naturel::common.video')}}</p>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>

    <div class="row row-service">
        <div class="content">
            <h2>{{Lang::get('naturel::common.services.title')}}</h2>

            <div class="service-items">
                @foreach($services as $item)
                    <a href="{{$item->link}}" class="service-link">
                        <div class="item">
                            <div class="item-img">
                                <img src="{{$item->media ? $item->media->path : ''}}" alt="item">

                                <div class="item-hover"></div>
                            </div>
                            <div class="item-title"><span>{{$item->text->caption}}</span></div>
                        </div>
                    </a>
                @endforeach
            </div>

            <div class="swiper-container swiper-service">
                <div class="swiper-wrapper">
                    @foreach($services as $item)
                    <div class="swiper-slide service-slide">
                        <a href="{{$item->link}}" class="service-link">
                            <div class="item">
                                <div class="item-img">
                                    <img src="{{$item->media ? $item->media->path : ''}}" alt="item">

                                    <div class="item-hover"></div>
                                </div>
                                <div class="item-title"><span>{{$item->text->caption}}</span></div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                <div class="swiper-pagination service-pagination"></div>
            </div>
        </div>
    </div>

    <div class="row row-boss">
        <div class="content">
            <div class="boss">
                <div class="boss-person">
                    <img src="{{$item->media ? $item->media->path : ''}}" alt="{{$manager->text->description}}">

                    <p class="boss-fio">{{$manager->text->caption}}</p>

                    <p class="boss-status">{{$manager->text->description}}</p>
                </div>
                <div class="boss-text">
                    <div class="text">
                        {!!$manager->text->text!!}
                    </div>
                    <a href="{{URL::to('certificates')}}">
                        <div class="btn yellow-btn boss-btn">{{Lang::get('naturel::common.detail')}}</div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row row-achievement">
        <div class="content">
            <h2>{{Lang::get('naturel::common.achievements')}}</h2>

            <div class="achievement-items">
                <div class="item">
                    <div class="item-graph">
                        <div class="item-num item-num1"><span>&gt;</span>{{$achievements[0]->number}}</div>
                        <input type="text" value="{{$achievements[0]->number}}" class="dial" data-readOnly="true"
                               data-linecap="round"
                               data-thickness="0.15" data-bgColor="#abe4da" data-fgColor="#71b3a8"
                               data-angleOffset="180" data-width="200" data-inputColor="#abe4da"
                               data-displayInput="false">
                    </div>
                    <div class="item-title">{{$achievements[0]->title_uk}}</div>
                </div>
                <div class="item">
                    <div class="item-graph">
                        <div class="item-num item-num2"><span>&gt;</span>{{$achievements[1]->number}}</div>
                        <input type="text" value="{{$achievements[1]->number}}" class="dial2" data-readOnly="true"
                               data-linecap="round"
                               data-thickness="0.15" data-bgColor="#e7e7e7" data-fgColor="#cecece" data-angleOffset="90"
                               data-width="200" data-inputColor="#cecece" data-displayInput="false">
                    </div>
                    <div class="item-title">{{$achievements[1]->title_uk}}</div>
                </div>
                <div class="item">
                    <div class="item-graph">
                        <div class="item-num item-num3"><span>&gt;</span>{{$achievements[2]->number}}</div>
                        <input type="text" value="{{$achievements[2]->number}}" class="dial3" data-readOnly="true"
                               data-linecap="round"
                               data-thickness="0.15" data-bgColor="#efb7c0" data-fgColor="#e28190"
                               data-angleOffset="180" data-width="200" data-inputColor="#efb7c0"
                               data-displayInput="false">
                    </div>
                    <div class="item-title">{{$achievements[2]->title_uk}}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row row-gallery">
        <!--    <div class="content">-->
        <div class="gallery-left" style="background-image: url('{{$mainGalleryImages[0]->media->path()}}');">
            <a rel="example_group" href="{{$mainGalleryImages[0]->media->path()}}" title="">
                <div class="hover-bg">
                    <div class="btn gallery-btn">Збільшити фото</div>
                </div>
            </a>
        </div>
        <div class="gallery-right">
            <div class="gallery-top">
                <div class="gallery-top-left" style="background-image: url('{{$mainGalleryImages[1]->media->path()}}');">
                    <a rel="example_group" href="{{$mainGalleryImages[1]->media->path()}}" title="">
                        <div class="hover-bg">
                            <div class="btn gallery-btn">Збільшити фото</div>
                        </div>
                    </a>
                </div>
                <div class="gallery-top-right" style="background-image: url('{{$mainGalleryImages[2]->media->path()}}');">
                    <a rel="example_group" href="{{$mainGalleryImages[2]->media->path()}}" title="">
                        <div class="hover-bg">
                            <div class="btn gallery-btn">Збільшити фото</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="gallery-bottom">
                <div class="gallery-bottom-left" style="background-image: url('{{$mainGalleryImages[3]->media->path()}}');">
                    <a rel="example_group" href="{{$mainGalleryImages[3]->media->path()}}" title="">
                        <div class="hover-bg">
                            <div class="btn gallery-btn">Збільшити фото</div>
                        </div>
                    </a>
                </div>
                <div class="gallery-bottom-right" style="background-image: url('{{$mainGalleryImages[4]->media->path()}}');">
                    <a rel="example_group" href="{{$mainGalleryImages[4]->media->path()}}" title="">
                        <div class="hover-bg">
                            <div class="btn gallery-btn">Збільшити фото</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!--    </div>-->
    </div>

    <div class="row row-why">
        <div class="content">
            <h2>{{Lang::get('naturel::common.trust.title')}}</h2>

            <div class="why-items">
                <div class="top">
                    <div class="item">
                        <img class="item-img" src="{{URL::asset('img/icons/indyvidual-oval-119-1-copy.png')}}"
                             alt="item">

                        <div class="item-title">{{Lang::get('naturel::common.trust.approach')}}</div>
                    </div>
                    <div class="item">
                        <img class="item-img" src="{{URL::asset('img/icons/personal-oval-119-1-copy-2.png')}}"
                             alt="item">

                        <div class="item-title">{{Lang::get('naturel::common.trust.personnel')}}</div>
                    </div>
                </div>
                <div class="middle">
                    <div class="item">
                        <img class="item-img" src="{{URL::asset('img/icons/obladnania-oval-119-1-copy-3.png')}}"
                             alt="item">

                        <div class="item-title">{{Lang::get('naturel::common.trust.equipment')}}</div>
                    </div>
                    <div class="item">
                        <img class="item-img" src="{{URL::asset('img/icons/vyrishenia-oval-119-1-copy-4.png')}}"
                             alt="item">

                        <div class="item-title">{{Lang::get('naturel::common.trust.problem')}}</div>
                    </div>
                </div>
                <div class="bottom">
                    <div class="item">
                        <img class="item-img" src="{{URL::asset('img/icons/reputacia-oval-119-1-copy-5.png')}}"
                             alt="item">

                        <div class="item-title">{{Lang::get('naturel::common.trust.reputation')}}</div>
                    </div>
                    <div class="item">
                        <img class="item-img" src="{{URL::asset('img/icons/metodyky-oval-119-1-copy-6.png')}}"
                             alt="item">

                        <div class="item-title">{{Lang::get('naturel::common.trust.method')}}</div>
                    </div>
                </div>
            </div>
            <div class="btn green-btn why-btn"
                 onclick="show();">{{Lang::get('naturel::common.trust.appointment')}}</div>
        </div>
    </div>

    <div class="row row-cards">
        <div class="content">
            <h2>{{Lang::get('naturel::common.certificates.title')}}</h2>

            <div class="tab-field">
                <div class="tabs">
                    <div id="tab1" class="tab tab-active">
                        <div class="tab-text-1">{{Lang::get('naturel::common.certificates.tab_1')}}</div>
                    </div>
                    <div id="tab2" class="tab">
                        <div class="tab-text-2">{{Lang::get('naturel::common.certificates.tab_2')}}</div>
                    </div>
                </div>
                <div class="tab-info">
                    <div class="tab-container-1">
                        <div class="tab-container">
                            <div class="gifts">
                                @foreach($certificates as $item)
                                    <div class="item">
                                        <h3 class="item-title">{{Lang::get('naturel::common.certificates.item-title')}}</h3>

                                        <div class="item-line"></div>
                                        <p class="price">{{$item->text->caption}}
                                            <span> {{Lang::get('naturel::common.certificates.currency')}}</span></p>

                                        <p class="count">{{$item->text->description}}</p>

                                        <div class="btn green-trans-btn card-btn"
                                             onclick="showOrder();">{{Lang::get('naturel::common.certificates.buy')}}</div>
                                        <a href="#" data-target="#empty-popup" data-text="{{$item->text->text}}"
                                           class="more show-modal">{{Lang::get('naturel::common.certificates.more')}}</a>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-container swiper-cards">
                                <div class="swiper-wrapper">
                                    @foreach($certificates as $item)
                                        <div class="swiper-slide cards-slide">
                                            <div class="item">
                                                <h3 class="item-title">{{Lang::get('naturel::common.certificates.item-title')}}</h3>

                                                <div class="item-line"></div>
                                                <p class="price">{{$item->text->caption}}
                                                    <span> {{Lang::get('naturel::common.certificates.currency')}}</span>
                                                </p>

                                                <p class="count">{{$item->text->description}}</p>

                                                <div class="btn green-trans-btn card-btn"
                                                     onclick="showOrder();">{{Lang::get('naturel::common.certificates.buy')}}</div>
                                                <a href="#" data-target="#empty-popup" data-text="{{$item->text->text}}"
                                                   class="more show-modal">{{Lang::get('naturel::common.certificates.more')}}</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="swiper-pagination cards-pagination"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-container-2">
                        <div class="tab-container">
                            <div class="card-left">
                                <p class="card-title">{{$card->text->caption}}</p>

                                <div class="card-line"></div>
                                <div class="card-text">
                                    <div class="card-text-field">
                                        {!! $card->text->text !!}
                                    </div>
                                </div>
                            </div>
                            <div class="card-right">
                                <div class="card-image"></div>
                                <a href="#" class="show-modal" data-target="#popup-info">
                                    <div class="btn green-trans-btn card-btn-2">{{Lang::get('naturel::common.certificates.more')}}</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row row-comments">
        <div class="content">
            <h2>{{Lang::get('naturel::common.reviews')}}</h2>

            <div class="swiper-container swiper-comment">
                <div class="swiper-wrapper">
                    @foreach($reviews as $item)
                        <div class="swiper-slide comment-slide">
                            <div class="comment">
                                <div class="costumer-person">
                                    <img src="{{$item->media ? $item->media->path : ''}}" alt="{{$item->text->caption}}">

                                    <p class="costumer-fio">{{$item->text->caption}}</p>
                                </div>
                                <div class="comment-text">
                                    <div class="text">
                                        <p>{{$item->text->text}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination comment-pagination"></div>
            </div>
            <div class="swiper-button-next comment-button-next"></div>
            <div class="swiper-button-prev comment-button-prev"></div>
        </div>
    </div>

    <div class="row row-blur">
        <div class="content">
            <p class="blur-text-1">{{Lang::get('naturel::common.beauty.title')}}</p>

            <p class="blur-text-2">{{Lang::get('naturel::common.beauty.image')}}</p>

            <p class="blur-text-3">{{Lang::get('naturel::common.beauty.admiration')}}</p>

            <p class="blur-text-4">{{Lang::get('naturel::common.beauty.naturel')}}</p>

            <div class="btn white-btn blur-btn"
                 onclick="show();">{{Lang::get('naturel::common.beauty.appointment')}}</div>
        </div>
    </div>

    <div class="row row-news">
        <div class="content">
            <div class="news-left">
                <h2>{{Lang::get('naturel::common.news.title')}}</h2>

                <div class="swiper-container swiper-news">
                    <div class="swiper-wrapper">
                        @foreach($news as $item)
                            <div class="swiper-slide news-slide">
                                <a href="{{$item->link}}">
                                    <div class="news-image"
                                         style="background-image: url('{{$item->media ? $item->media->path : ''}}');"></div>
                                </a>

                                <div class="news-discription">
                                    <a href="{{$item->link}}">
                                        <h4 class="news-title">{{$item->text->caption}}</h4></a>

                                    <p>{{\Illuminate\Support\Str::limit(strip_tags($item->text->text), 200)}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination news-pagination"></div>
                    <div class="swiper-button-next news-button-next"></div>
                    <div class="swiper-button-prev news-button-prev"></div>
                </div>
            </div>
            <div class="news-right">
                @foreach($banners as $item)
                    <a href="{{$item->link}}" target="_blank">
                        <img class="action-{{$item->id}}" src="{{$item->media ? $item->media->path : ''}}" alt="Action">
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row row-partners">
        <div class="content">
            <h2>{{Lang::get('naturel::common.partners.title')}}</h2>

            <div class="partners-items">
                @foreach($partners as $item)
                    <a class="item" target="_blank" href="{{$item->link}}">
                        <img class="item-img{{$item->id}}" src="{{$item->media ? $item->media->path : ''}}" alt="{{$item->title}}">
                    </a>
                @endforeach
            </div>
            <div class="swiper-container swiper-partners">
                <div class="swiper-wrapper">
                    @foreach($partners as $item)
                        <div class="swiper-slide partners-slide">
                            <a class="item" target="_blank" href="{{$item->link}}">
                                <img class="item-img{{$item->id}}" src="{{$item->media ? $item->media->path : ''}}" alt="{{$item->title}}">
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination partners-pagination"></div>
            </div>
        </div>
    </div>

    <div class="row row-makeup">
        <div class="content">
            <h2>{{Lang::get('naturel::common.catalog.main-title')}}</h2>

            <div class="makeup-items">
                <?php $even = true; ?>
                @foreach($products->chunk(2) as $chunk)
                    <div class="{{$even ? 'top' : 'bottom'}}">
                        @foreach ($chunk as $item)
                            <a href="{{$item->link}}">
                                <div class="item">
                                    <div class="item-img" style="background-image: url('{{$item->media ? $item->media->path : ''}}');"></div>
                                    <div class="makeup-price">
                                        <p class="num">{{$item->price}}</p>

                                        <p class="val">{{Lang::get('naturel::common.catalog.currency')}}</p>
                                    </div>
                                    <p class="makeup-title">{{$item->text->caption}}</p>

                                    <p class="makeup-discription">{{$item->text->description}}</p>
                                </div>
                            </a>
                        @endforeach
                    </div>
                    <?php $even = false; ?>
                @endforeach
            </div>
            <a href="{{URL::to('catalog')}}">
                <div class="btn yellow-btn makeup-btn">{{Lang::get('naturel::common.catalog.navigate')}}</div>
            </a>
        </div>
    </div>

    <div class="row row-instagram">
        <div class="content">
            <h2>Наші фото в Instagram</h2>

            <p>Розміщуйте фото з хештегом <span><a href="//www.instagram.com/naturel_cosmetology_center/" class="more"
                                                   target="_blank">#naturel</a></span></p>
            <div class="instagram-items">
                <?php $even = true; $i = 1; ?>
                @foreach ($instagram->images->chunk(2) as $chunk)
                    <div class="{{$even ? 'top' : 'bottom'}}">
                        @foreach ($chunk as $item)
                            <div class="item">
                                <a target="" href="//www.instagram.com/naturel_cosmetology_center/">
                                    <img class="item-img{{$i}}" src="{{$item->media ? $item->media->path : ''}}" alt="item">
                                </a>
                            </div>
                            <?php $i = ($i < 3) ? $i + 1 : 3; ?>
                        @endforeach
                    </div>
                    <?php $even = false; ?>
                @endforeach
            </div>
            <a href="//www.instagram.com/naturel_cosmetology_center/" class="more" target="_blank">Переглянути більше
                фото</a>
        </div>
    </div>

    <div id="map" class="row row-map-google"></div>
    <div class="row row-map">
        <div class="contacts">
            <div class="info">
                <h2>{{$contacts->text->caption}}</h2>

                {!! $contacts->text->text !!}
            </div>
            <svg class="fill" width="450" height="525">
                <polygon points="0,0 450,0 450,525 0,525 0,90 20,70 0,50"/>
            </svg>
            <svg class="border" width="26" height="525">
                <polygon points="0,0 6,0 6,50 26,70 6,90 6,525 0,525 0,88 18,70 0,52"/>
            </svg>
        </div>

        <a class="z-link" href="http://zaycevstudio.com/" target="_blank" title="{{Lang::get('naturel::common.studio.title')}}">
            <img class="z-logo" src="{{asset('img/logo/logo-zaycevstudio-copy.png')}}" alt="{{Lang::get('naturel::common.studio.title')}}">

            <div class="z-text">
                <span>{{Lang::get('naturel::common.studio.by')}}</span>
                <span>{{Lang::get('naturel::common.studio.by-title')}}</span>
            </div>
        </a>
    </div>

    @include('naturel::templates.popup-form')
    @include('naturel::templates.popup-order')
    @include('naturel::templates.popup-empty')
    @include('naturel::templates.popup-info')

    <div id="popup-video" class="popup-video">
        <div class="close-video">
            <div class="close-img"></div>
        </div>
        @if(array_key_exists('video', $screen_1->additional)) {!!$screen_1->additional['video']!!} @endif
    </div>
    <div id="wrap"></div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <script type="text/javascript" src="{{URL::asset('js/map.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/graf.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/swiper.min.js')}}"></script>

    <!-- Initialize Swiper -->
    <script type="text/javascript">
        new Swiper('.swiper-header', {
            loop: true,
            pagination: '.header-pagination',
            paginationClickable: true,
            nextButton: '.header-button-next',
            prevButton: '.header-button-prev',
            effect: 'fade',
            autoplay: 4000,
            speed: 1400,
            spaceBetween: 30
        });

        var swiper = new Swiper('.swiper-comment', {
            pagination: '.comment-pagination',
            paginationClickable: true,
            nextButton: '.comment-button-next',
            prevButton: '.comment-button-prev',
            spaceBetween: 30
        });
        var swiper = new Swiper('.swiper-news', {
            pagination: '.news-pagination',
            paginationClickable: true,
            nextButton: '.news-button-next',
            prevButton: '.news-button-prev',
            spaceBetween: 30
        });
        var swiper = new Swiper('.swiper-partners', {
            pagination: '.partners-pagination',
            paginationClickable: true,
            spaceBetween: 30
        });
        var swiper = new Swiper('.swiper-service', {
            pagination: '.service-pagination',
            paginationClickable: true,
            spaceBetween: 30
        });
        var swiper = new Swiper('.swiper-cards', {
            pagination: '.cards-pagination',
            paginationClickable: true,
            spaceBetween: 30
        });
    </script>
@endsection