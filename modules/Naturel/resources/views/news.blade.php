@extends('naturel::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('naturel::templates.top-nav')
    @include('naturel::templates.top-header')

    <div class="row news-info-row">
        <div class="content">
            <h2>{{$data->text->caption}}</h2>
            <div class="breadcrumbs">
                <a href="{{URL::route('naturel.index')}}">{{Lang::get('naturel::common.main')}}</a><div class="breadcrumbs-circle"></div>
                <a href="javascript:">{{$data->text->caption}}</a>
            </div>
        </div>
    </div>

    <div class="row news-row">
        <div class="content">
            <?php $even = true; ?>
            @foreach ($news->chunk(2) as $chunk)
                <div class="{{$even ? 'top' : 'bottom'}}">
                    @foreach ($chunk as $item)
                        <div class="news">
                            <a href="{{URL::to($item->link)}}">
                                <div class="news-image" style="background-image: url('{{$item->media ? $item->media->path : ''}}');"></div>
                            </a>
                            <div class="news-discription">
                                <a href="{{URL::to($item->link)}}">
                                    <h4 class="news-title">{{$item->text->caption}}</h4>
                                </a>
                                <p>{{\Illuminate\Support\Str::limit(strip_tags($item->text->text), 200)}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <?php $even = false; ?>
            @endforeach
        </div>
    </div>

    @include('naturel::templates.footer')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>
@endsection