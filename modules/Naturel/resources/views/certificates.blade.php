@extends('naturel::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('naturel::templates.top-nav')
    @include('naturel::templates.top-header')

    <div class="row news-info-row">
        <div class="content">
            <h2>{{$data->text->caption}}</h2>
            <div class="breadcrumbs">
                <a href="{{URL::route('naturel.index')}}">{{Lang::get('naturel::common.main')}}</a><div class="breadcrumbs-circle"></div>
                <a href="javascript:void(0);">{{$data->text->caption}}</a>
            </div>
        </div>
    </div>

    <div class="row sertificates-row">
        <div class="content">
            <div class="sertificates-discrpt">{!! $data->text->text !!}</div>
            @foreach($certificates->images->chunk(4) as $chunk)
                <?php $even = true; ?>
                <div class="sertificates-items">
                    @foreach($chunk->chunk(2) as $chunk_)
                        <div class="{{$even ? 'top' : 'bottom'}}">
                            @foreach($chunk_ as $item)
                                <div class="sertificates">
                                    <a rel="sert_group" href="{{$item->media ? $item->media->path : ''}}">
                                        <div class="sertificates-img">
                                            <img src="{{$item->media ? $item->media->url('225', '300', 'c') : ''}}" alt="">
                                        </div>
                                    </a>
                                    {{--<div class="sertificates-title">
                                        <h2>Ericson Laboratoire Enzymacid Intrazym cream </h2>
                                        <p>Поживний крем 50 мл</p>
                                    </div>--}}
                                </div>
                            @endforeach
                        </div>
                        <?php $even = false; ?>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>

    @include('naturel::templates.footer')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            $('a[rel="sert_group"]').fancybox({
                'transitionIn': 'none',
                'transitionOut': 'none',
                'titlePosition': 'over',
                'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
                    return '<span id="fancybox-title-over">' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
                }
            });
        });
    </script>
@endsection