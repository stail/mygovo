<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">

    @yield('meta-tags')

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{URL::asset('css/normalize.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">

    <link rel="stylesheet" href="{{URL::asset('plugins/fancybox/jquery.fancybox-1.3.4.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/swiper.min.css')}}">
</head>
<body>
@yield('main')

{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
<script type="text/javascript" src="{{URL::asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
{{--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>--}}

<script type="text/javascript" src="{{URL::asset('js/jquery.knob.min.js')}}"></script>

<script type="text/javascript" src="{{URL::asset('js/jquery.inputmask.js')}}"></script>

<script type="text/javascript" src="{{URL::asset('plugins/fancybox/jquery.mousewheel-3.0.4.pack.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('plugins/fancybox/jquery.fancybox-1.3.4.pack.js')}}"></script>

<script type="text/javascript" src="{{URL::asset('modules/backend/plugins/jquery-validation/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('modules/backend/plugins/jquery-validation/js/additional-methods.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("a[rel=example_group]").fancybox({
            'transitionIn': 'none',
            'transitionOut': 'none',
            'titlePosition': 'over',
            'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            }
        });
    });
</script>
@yield('scripts')

<script type="text/javascript" src="{{URL::asset('js/custom.js')}}"></script>
</body>
</html>