@extends('naturel::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('naturel::templates.top-nav')
    @include('naturel::templates.top-header')

    <div class="row news-info-row">
        <div class="content">
            <h2>{{$data->text->caption}}</h2>
            <div class="breadcrumbs">
                <a href="{{URL::route('naturel.index')}}">{{Lang::get('naturel::common.main')}}</a><div class="breadcrumbs-circle"></div>
                <a href="{{URL::to('services')}}">{{Lang::get('naturel::common.services.title')}}</a><div class="breadcrumbs-circle"></div>
                @if($data->parent)
                    <a href="{{URL::to($data->parent->category->link)}}">{{$data->parent->category->text->caption}}</a><div class="breadcrumbs-circle"></div>
                    <a href="{{URL::to($data->parent->link)}}">{{$data->parent->text->caption}}</a><div class="breadcrumbs-circle"></div>
                    <a href="javascript:void(0);">{{$data->text->caption}}</a>
                @else
                    <a href="{{URL::to($data->category->link)}}">{{$data->category->text->caption}}</a><div class="breadcrumbs-circle"></div>
                    <a href="javascript:void(0);">{{$data->text->caption}}</a>
                @endif
            </div>
        </div>
    </div>

    <div class="row one-page-discription-row">
        <div class="one-page-slider">
            <div class="swiper-container swiper-one-page-image">
                <div class="swiper-wrapper">
                    @foreach($data->gallery as $item)
                        <div class="swiper-slide one-page-image-slide">
                            <img src="{{$item->media ? $item->media->url('935', '630', 'c') : ''}}" alt="">
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination one-page-image-pagination"></div>
            </div>
            <div class="swiper-button-next one-page-image-button-next"></div>
            <div class="swiper-button-prev one-page-image-button-prev"></div>
        </div>

        <div class="content">
            {!! $data->text->text !!}

            @if(count($data->childs))
                <div class="one-page-items">
                    @foreach($data->childs as $item)
                        <a href="{{URL::to($item->link)}}" class="one-page-item-link">
                            <div class="item">
                                <div class="item-img">
                                    <img src="{{$item->media ? $item->media->url('225', '225', 'c') : ''}}" alt="">
                                </div>
                                <div class="item-title"><span>{{$item->text->caption}}</span></div>
                            </div>
                        </a>
                    @endforeach
                </div>
            @endif

            <h3>{{Lang::get('naturel::common.services.price')}}</h3>
            <div class="table">
                <div class="table-title">
                    <div class="left-title">{{Lang::get('naturel::common.services.type')}}</div>
                    <div class="right-title">{{Lang::get('naturel::common.services.price_2')}}</div>
                </div>
                @if(count($data->childs))
                    @foreach($data->childs as $item)
                        <div class="table-row">
                            <div class="left">
                                <a href="{{URL::to($item->link)}}">{{$item->text->caption}}</a></div>
                            <div class="right"><span class="price">{{$item->price}}</span></div>
                            <div class="table-btn">
                                <div class="btn green-trans-btn row-btn show-order"
                                     data-title="{{$item->text->caption}}">Замовити</div></div>
                        </div>
                    @endforeach
                @else
                    <div class="table-row">
                        <div class="left">{{$data->text->caption}}</div>
                        <div class="right"><span class="price">{{$data->price}}</span></div>
                        <div class="table-btn">
                            <div class="btn green-trans-btn row-btn show-order"
                                 data-title="{{$data->text->caption}}">Замовити</div></div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row same-proc-row">
        <div class="content">
            <h2 class="same-items-title">Інші процедури</h2>
            <!-- Swiper -->
            <div class="swiper-container same-swiper">
                <div class="swiper-wrapper">
                    @foreach($items as $item)
                        <div class="swiper-slide">
                            <a href="{{URL::to($item->link)}}">
                                <div class="item">
                                    <img src="{{$item->media ? $item->media->url('225', '225', 'c') : ''}}" alt="">
                                    <h3>{{$item->text->caption}}</h3>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination same-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next same-button-next"></div>
                <div class="swiper-button-prev same-button-prev"></div>
            </div>
        </div>
    </div>

    <div class="row one-page-btn-row">
        <div class="content">
            <div class="btn green-trans-btn one-page-btn" onclick="show();">{{Lang::get('naturel::common.consultation')}}</div>
        </div>
    </div>

    @include('naturel::templates.popup-order')
    @include('naturel::templates.popup-form')
    <div id="wrap"></div>

    @include('naturel::templates.footer')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/swiper.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>

    <!-- Initialize Swiper -->
    <script type="text/javascript">
        var swiper = new Swiper('.swiper-one-page-image', {
            pagination: '.one-page-image-pagination',
            paginationClickable: true,
            nextButton: '.one-page-image-button-next',
            prevButton: '.one-page-image-button-prev',
            spaceBetween: 30,
            loop: true,
            effect: 'fade',
            autoplay: 4000
        });

        new Swiper('.same-swiper', {
            pagination: '.same-pagination',
            nextButton: '.same-button-next',
            prevButton: '.same-button-prev',
            paginationClickable: true,
            slidesPerView: 3,
            spaceBetween: 20,
            breakpoints: {
                870: {
                    slidesPerView: 2,
                    spaceBetween: 15
                },
                600: {
                    slidesPerView: 1,
                    spaceBetween: 10
                }
            }
        });
    </script>
@endsection