@extends('naturel::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('naturel::templates.top-nav')
    @include('naturel::templates.top-header')

    <div class="row news-info-row">
        <div class="content">
            <h2>{{$data->text->caption}}</h2>
            <div class="breadcrumbs">
                <a href="{{URL::route('naturel.index')}}">{{Lang::get('naturel::common.main')}}</a><div class="breadcrumbs-circle"></div>
                <a href="{{URL::to('services')}}">{{Lang::get('naturel::common.services.title')}}</a><div class="breadcrumbs-circle"></div>
                <a href="javascript:">{{$data->text->caption}}</a>
            </div>
        </div>
    </div>

    <div class="row face-care-row">
        <div class="content">
            <?php $even = true; ?>
            @foreach ($data->services->chunk(3) as $chunk)
                <div class="{{$even ? 'top' : 'bottom'}}">
                    @foreach ($chunk as $item)
                        <div class="face-care">
                            <a href="{{URL::to($item->link)}}">
                                <img src="{{$item->media ? $item->media->url('254', '254', 'c') : ''}}" alt="{{$item->text->caption}}">
                            </a>
                            <a href="{{URL::to($item->link)}}">
                                <div class="face-care-title">
                                    <h2>{{$item->text->caption}}</h2>
                                </div>
                            </a>
                            @if(count($item->childs))
                                <ul>
                                @foreach($item->childs as $child)
                                    <li><a href="{{URL::to($child->link)}}">{{$child->text->caption}}</a></li>
                                @endforeach
                                </ul>
                            @endif
                        </div>
                    @endforeach
                </div>
                <?php $even = false; ?>
            @endforeach
        </div>
    </div>
    @include('naturel::templates.footer')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>
@endsection