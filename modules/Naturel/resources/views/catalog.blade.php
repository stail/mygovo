@extends('naturel::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('naturel::templates.top-nav')
    @include('naturel::templates.top-header')

    <div class="row news-info-row">
        <div class="content">
            <h2>{{Lang::get('naturel::common.catalog.title')}}</h2>
            <div class="breadcrumbs">
                <a href="{{URL::route('naturel.index')}}">{{Lang::get('naturel::common.main')}}</a><div class="breadcrumbs-circle"></div>
                <a href="javascript:">{{Lang::get('naturel::common.catalog.title')}}</a>
            </div>
        </div>
    </div>

    <div class="row filter-row">
        <div class="content">
            <div class="filter-btn">{{Lang::get('naturel::common.catalog.filter')}}</div>
            <ul class="filter-list">
                <li>
                    <a href="{{URL::to('catalog')}}" class="filter-link {{!isset($active) ? 'active' : ''}}">{{Lang::get('naturel::common.catalog.all')}}</a></li>
                @foreach($categories as $item)
                    <li>
                        <a href="{{URL::to($item->link)}}" class="filter-link {{isset($active) && $active == $item->id ? 'active' : ''}}">{{$item->text->caption}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="row catalog-row">
        <div class="content">
            <?php $even = true; ?>
            @foreach ($products->chunk(4) as $chunk_)
                <div class="makeup-items {{$even ? 'items-top' : ''}}">
                    <?php $even_ = true; ?>
                    @foreach ($chunk_->chunk(2) as $chunk)
                        <div class="{{$even_ ? 'top' : 'bottom'}}">
                        @foreach($chunk as $item)
                            <a href="{{URL::to($item->link)}}">
                                <div class="item">
                                    <div class="item-img" style="background-image: url('{{$item->media ? $item->media->path : ''}}');">
                                    </div>
                                    <div class="makeup-price">
                                        <p class="num">{{$item->price}}</p>
                                        <p class="val">{{Lang::get('naturel::common.catalog.currency')}}</p>
                                    </div>
                                    <p class="makeup-title">{{$item->text->caption}}</p>
                                    <p class="makeup-discription">{{$item->text->description}}</p>
                                </div>
                            </a>
                        @endforeach
                        </div>
                        <?php $even_ = false; ?>
                    @endforeach
                </div>
                <?php $even = false; ?>
            @endforeach
        </div>
    </div>

    @include('naturel::templates.footer')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>
@endsection