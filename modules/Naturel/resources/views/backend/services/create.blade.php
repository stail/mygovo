@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Додавання послуг</div>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.naturel.services.index')}}">
                            <i class="fa fa-arrow-left"></i> Назад</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="form-body">
                            <div class="tabbable-custom ">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#tab_1" data-toggle="tab">Дані</a>
                                    </li>
                                    <li>
                                        <a href="#tab_2" data-toggle="tab">Вартість послуг</a>
                                    </li>
                                    <li>
                                        <a href="#tab_3" data-toggle="tab">Галерея</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Заголовок</label>
                                                    <input type="text" class="form-control" name="text_uk[caption]">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Опис</label>
                                                    <input type="text" class="form-control" name="text_uk[description]">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Зображення</label>

                                                    <div class="input-group" id="file-picker" data-input=".media-id">
                                                        <input type="hidden" name="media_path" class="media-id">
                                                        <input type="text" class="form-control file-name" readonly
                                                               placeholder="Файл не вибрано">
                                                        <span class="input-group-btn">
                                                            <a class="btn default" rel="popup" type="button">
                                                                <i class="icon-refresh"></i></a>
                                                        </span>
                                                        <span class="input-group-btn">
                                                            <a class="btn blue" rel="preview" type="button" target="_blank"
                                                               href="javascript:">
                                                                <i class="fa fa-eye"></i></a>
                                                        </span>
                                                        <span class="input-group-btn">
                                                            <a class="btn red" rel="delete" href="javascript:">
                                                                <i class="fa fa-trash"></i></a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">URL</label>
                                                    <input type="text" class="form-control" name="content[slug]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Статус</label>
                                                    <select class="form-control" name="content[active]">
                                                        <option value="1">Опубліковано</option>
                                                        <option value="0">Приховано</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Категорія</label>
                                                    <select class="form-control" name="content[parent_id]">
                                                        @foreach($categories as $item)
                                                            <option value="{{$item->id}}">{{$item->text->caption}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Текст</label>
                                                    <textarea class="form-control" id="cke_uk" name="text_uk[text]"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <h3 class="form-section">Мета-теги</h3>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Заголовок</label>
                                                    <input type="text" class="form-control" name="text_uk[meta_title]">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Опис</label>
                                                    <input type="text" class="form-control" name="text_uk[meta_description]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Ключові слова</label>
                                                    <input type="text" class="form-control" name="text_uk[meta_keywords]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <a class="btn btn-default btn-sm" href="javascript:" rel="add-price">
                                            <i class="fa fa-plus"></i> Додати</a>

                                        <table class="table table-striped table-bordered table-hover margin-top-20">
                                            <thead>
                                            <tr class="heading">
                                                <th>{{trans('backend::common.caption')}}</th>
                                                <th>Ціна</th>
                                                <th width="40px">{{trans('backend::common.actions')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody id="prices-list"></tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="tab_3">
                                        <a class="btn btn-default btn-sm popup-selector" href="javascript:" data-input="gallery-items">
                                            <i class="fa fa-plus"></i> Додати</a>
                                        <div id="gallery-list" class="margin-top-20"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/naturel/backend/js/services-form.js')}}"></script>
@endsection