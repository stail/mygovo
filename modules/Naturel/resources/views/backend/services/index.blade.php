@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Послуги</div>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.naturel.services.create')}}">
                            <i class="fa fa-plus"></i> Додати</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover"
                               data-url="{{URL::route('backend.naturel.services.weight')}}">
                            <thead>
                            <tr class="heading">
                                <th>{{trans('backend::common.caption')}}</th>
                                <th width="40px">{{trans('backend::common.status')}}</th>
                                <th width="150px">{{trans('backend::common.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody class="sortable">
                            @foreach($data as $item)
                                <tr data-id="{{$item->id}}">
                                    <td>{{$item->text->caption}}</td>
                                    <td>
                                        <a href="{{URL::route('backend.naturel.services.status',['id' => $item->id])}}"
                                           rel="item-status" class="btn btn-sm {{$item->active ? 'green' : 'red'}}">
                                            <i class="fa {{$item->active ? 'fa-check' : 'fa-ban'}}"></i></a>
                                    </td>
                                    <td>
                                        <a href="{{URL::route('backend.naturel.services.create').'?service_id='.$item->id}}"
                                           class="btn btn-sm default" title="Створити">
                                            <i class="fa fa-plus"></i></a>
                                        <a href="{{URL::route('backend.naturel.services.edit', ['id' => $item->id])}}"
                                           rel="edit-portlet" class="btn btn-sm green" title="Редагувати">
                                            <i class="fa fa-pencil"></i></a>
                                        <a href="{{URL::route('backend.naturel.services.destroy', ['id' => $item->id])}}"
                                           rel="item-destroy" class="btn btn-sm red" title="Видалити">
                                            <i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                @if($item->childs)
                                    @foreach($item->childs as $child)
                                        <tr>
                                            <td style="padding-left: 35px;">{{$child->text->caption}}</td>
                                            <td>
                                                <a href="{{URL::route('backend.naturel.services.status',['id' => $child->id])}}"
                                                   rel="item-status" class="btn btn-sm {{$child->active ? 'green' : 'red'}}">
                                                    <i class="fa {{$child->active ? 'fa-check' : 'fa-ban'}}"></i></a>
                                            </td>
                                            <td>
                                                <a href="{{URL::route('backend.naturel.services.edit', ['id' => $child->id])}}"
                                                   rel="edit-portlet" class="btn btn-sm green" title="Редагувати">
                                                    <i class="fa fa-pencil"></i></a>
                                                <a href="{{URL::route('backend.naturel.services.destroy', ['id' => $child->id])}}"
                                                   rel="item-destroy" class="btn btn-sm red" title="Видалити">
                                                    <i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/naturel/backend/js/services.js')}}"></script>
@endsection