@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Галерея</div>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.naturel.gallery.index')}}">
                            <i class="fa fa-arrow-left"></i> Назад</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="form-body">
                            <div class="tabbable-custom">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_3">
                                        <a class="btn btn-default btn-sm popup-selector" href="javascript:" data-input="gallery-items">
                                            <i class="fa fa-plus"></i> Додати</a>
                                        <div id="gallery-list" class="margin-top-20">
                                            <?php $index = 0; ?>
                                            @foreach($data->images as $item)
                                                <div class="inline-block gallery-item" style="position: relative; margin: 5px;">
                                                    <a class="btn btn-sm red" href="javascript:" rel="destroy-gallery-item" style="position: absolute; right: 0; top: 0;">
                                                        <i class="fa fa-trash-o"></i></a>
                                                    <input type="hidden" name="gallery[{{$index}}][media_id]" value="{{$item->media->id}}">
                                                    <img style="max-height: 140px;" src="{{$item->media->path()}}" alt="" class="img-thumbnail">
                                                </div>
                                                <?php $index++; ?>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(function ($) {
            'use strict';

            var _Body = $('body');

            function processSelectedFile(filePath, requestingField, file) {
                if (requestingField == 'gallery-items') {
                    var _Index = $('.gallery-item', '#gallery-list').length;
                    $('#gallery-list').append('<div class="inline-block gallery-item" style="position: relative; margin: 5px;"><a class="btn btn-sm red" href="javascript:" rel="destroy-gallery-item" style="position: absolute; right: 0; top: 0;"><i class="fa fa-trash-o"></i></a><input type="hidden" name="gallery[' + _Index + '][media_id]" value="' + file.media.id + '"><img style="max-height: 140px;" src="' + file.path + '" alt="" class="img-thumbnail"></div>');
                } else {
                    $('.file-name', filePicker).val(file.name ? file.name : ' ');
                    $('[rel="preview"]', filePicker).attr('href', filePath);
                    $(requestingField, filePicker).val(filePath).change();
                }
            }

            window.processSelectedFile = processSelectedFile;

            _Body.on('click', '.popup-selector', function (e) {
                e.preventDefault();
                var _ID = $(this).attr('data-input');
                var elfinder = '/elfinder/popup/';

                var triggerUrl = elfinder + _ID;
                $.colorbox({
                    href: triggerUrl,
                    fastIframe: true,
                    iframe: true,
                    width: '70%',
                    height: '580'
                });
            });

            _Body.on('click', '[rel="destroy-gallery-item"]', function () {
                $(this).parents('.gallery-item').remove();
            });
        });
    </script>
@endsection