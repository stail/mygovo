@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Слайдер</h3>

            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <ul class="nav nav-tabs pull-left">
                        @foreach($languages as $lang)
                            <li class="{{$lang->default ? 'active' : ''}}">
                                <a href="#portlet_tab_{{$lang->key}}" data-toggle="tab" aria-expanded="true">
                                    {{$lang->caption}}</a></li>
                        @endforeach
                    </ul>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.naturel.slider.index')}}">
                            <i class="fa fa-arrow-left"></i> Назад</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="tab-content">
                            @foreach($languages as $lang)
                                <div class="tab-pane form-body {{$lang->default ? 'active' : ''}}" id="portlet_tab_{{$lang->key}}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Заголовок</label>
                                                <input type="text" class="form-control" name="text[{{$lang->key}}][caption]"
                                                       value="{{array_key_exists($lang->key, $text) ? $text[$lang->key]['caption'] : ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Опис</label>
                                                <input type="text" class="form-control" name="text[{{$lang->key}}][description]"
                                                       value="{{array_key_exists($lang->key, $text) ? $text[$lang->key]['description'] : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Зображення</label>

                                        <div class="input-group" id="file-picker" data-input=".media-id">
                                            <input type="hidden" name="media_path" class="media-id"
                                                   value="{{isset($data) && $data->media ? $data->media->path() : ''}}">
                                            <input type="text" class="form-control file-name" readonly
                                                   placeholder="Файл не вибрано"
                                                   value="{{isset($data) && $data->media ? $data->media->caption : ''}}">
                                            <span class="input-group-btn">
                                                <a class="btn default" rel="popup" type="button">
                                                    <i class="icon-refresh"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn blue" rel="preview" type="button" target="_blank"
                                                   href="{{isset($data) && $data->media ? $data->media->path(): ''}}">
                                                    <i class="fa fa-eye"></i></a>
                                            </span>
                                            <span class="input-group-btn">
                                                <a class="btn red" rel="delete" href="javascript:void(0);">
                                                    <i class="fa fa-trash"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Статус</label>
                                        <select class="form-control" name="content[active]">
                                            <option @if(isset($data) && $data->active == 1) selected @endif value="1">Опубліковано</option>
                                            <option @if(isset($data) && $data->active == 0) selected @endif value="0">Приховано</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(function ($) {
            'use strict';

            var _Body = $('body');

            var filePicker = $('#file-picker');
            var _Input = filePicker.data('input');

            filePicker.on('click', '[rel="popup"]', function (e) {
                e.preventDefault();
                var elFinder = '/elfinder/popup/';
                var triggerUrl = elFinder + _Input;

                $.colorbox({
                    href: triggerUrl,
                    fastIframe: true,
                    iframe: true,
                    width: '70%',
                    height: '580'
                });
            });

            filePicker.on('change', _Input, function () {
                if (!$(this).val()) {
                    $('[rel="preview"]', filePicker).parent('.input-group-btn').hide();
                    $('[rel="delete"]', filePicker).parent('.input-group-btn').hide();
                } else {
                    $('[rel="preview"]', filePicker).parent('.input-group-btn').show();
                    $('[rel="delete"]', filePicker).parent('.input-group-btn').show();
                }
            });
            $(_Input).change();

            filePicker.on('click', '[rel="delete"]', function () {
                if (confirm('Видалити?')) {
                    $(_Input, filePicker).val('').change();
                    $('.file-name', filePicker).val('');
                }
            });

            filePicker.on('click', '[rel="preview"]', function (e) {
                e.preventDefault();
                var _href = $(this).attr('href');

                $.colorbox({
                    href: _href,
                    photo: true,
                    width: '70%',
                    height: '580'
                });
            });

            function processSelectedFile(filePath, requestingField, file) {
                if (requestingField == 'gallery-items') {
                    var _Index = $('.gallery-item', '#gallery-list').length;
                    $('#gallery-list').append('<div class="inline-block gallery-item" style="position: relative; margin: 5px;"><a class="btn btn-sm red" href="javascript:" rel="destroy-gallery-item" style="position: absolute; right: 0; top: 0;"><i class="fa fa-trash-o"></i></a><input type="hidden" name="gallery[' + _Index + '][media_id]" value="' + file.media.id + '"><img style="max-height: 140px;" src="' + file.path + '" alt="" class="img-thumbnail"></div>');
                } else {
                    $('.file-name', filePicker).val(file.name ? file.name : ' ');
                    $('[rel="preview"]', filePicker).attr('href', filePath);
                    $(requestingField, filePicker).val(filePath).change();
                }
            }

            window.processSelectedFile = processSelectedFile;
        });
    </script>
@endsection