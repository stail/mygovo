@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Категорії</h3>

            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <ul class="nav nav-tabs pull-left">
                        <li class="active">
                            <a href="#portlet_tab_main" data-toggle="tab" aria-expanded="true">
                                Дані</a></li>
                        @foreach($languages as $lang)
                            <li class="">
                                <a href="#portlet_tab_{{$lang->key}}" data-toggle="tab" aria-expanded="true">
                                    {{$lang->caption}}</a></li>
                        @endforeach
                    </ul>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.naturel.catalog.index')}}">
                            <i class="fa fa-arrow-left"></i> Назад</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form role="form" class="form" method="post">
                        @if(Session::get('message') == 'success')
                            <div class="Metronic-alerts alert alert-success fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                Збережено
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="tab-content">
                            <div class="tab-pane form-body active" id="portlet_tab_main">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Статус</label>
                                            <select class="form-control" name="content[active]">
                                                <option @if(isset($data) && $data->active == 1) selected @endif value="1">Опубліковано</option>
                                                <option @if(isset($data) && $data->active == 0) selected @endif value="0">Приховано</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">URL</label>
                                            <input type="text" class="form-control" name="content[slug]"
                                                   value="{{isset($data) ? $data->slug : ''}}">
                                            <span class="help-block">Залиште порожнім для автоматичної генерації</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @foreach($languages as $lang)
                                <div class="tab-pane form-body" id="portlet_tab_{{$lang->key}}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Заголовок</label>
                                                <input type="text" class="form-control" name="text[{{$lang->key}}][caption]"
                                                       value="{{isset($text) && array_key_exists($lang->key, $text) ? $text[$lang->key]['caption'] : ''}}">
                                            </div>
                                        </div>
                                    </div>
                                    @include('backend::forms.metatags')
                                </div>
                            @endforeach
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection