@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Товари</div>
                    <div class="actions">
                        <a class="btn btn-default btn-sm" rel="to-index"
                           href="{{URL::route('backend.naturel.products.create')}}">
                            <i class="fa fa-plus"></i> Додати</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover data-table">
                            <thead>
                            <tr class="heading">
                                <th>{{trans('backend::common.caption')}}</th>
                                <th>Категорія</th>
                                <th width="40px">{{trans('backend::common.status')}}</th>
                                <th width="100px">{{trans('backend::common.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/naturel/backend/js/catalog-category.js')}}"></script>
@endsection