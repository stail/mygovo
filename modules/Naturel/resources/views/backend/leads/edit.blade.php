@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold">Замовлення №{{$data->id}}</span></div>
                    <div class="actions">
                        <a class="btn btn-default" href="{{URL::route('backend.naturel.leads.index')}}">
                            <i class="fa fa-arrow-left"></i> Назад</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal form" method="post">
                        <input type="hidden" name="_token" value="{{Session::token()}}">

                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Сторінка</label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">{{$data->page}}</p></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Статус</label>

                                        <div class="col-md-9">
                                            <select class="form-control" name="content[status]">
                                                <option {{$data->status == 1 ? 'selected="selected"' : ''}}
                                                        value="1">Новий</option>
                                                <option {{$data->status == 2 ? 'selected="selected"' : ''}}
                                                        value="2">В роботі</option>
                                                <option {{$data->status == 3 ? 'selected="selected"' : ''}}
                                                        value="3">В очікуванні</option>
                                                <option {{$data->status == 4 ? 'selected="selected"' : ''}}
                                                        value="4">Виконано</option>
                                                <option {{$data->status == 5 ? 'selected="selected"' : ''}}
                                                        value="5">Відмова</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Форма</label>

                                        <div class="col-md-9">
                                            <p class="form-control-static">{{$data->form}}</p></div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="form-section">Користувач</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Ім'я</label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="content[name]"
                                                   value="{{$data->name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Телефон</label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="content[phone]"
                                                   value="{{$data->phone}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">E-mail</label>

                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="content[email]"
                                                   value="{{$data->email}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="form-section">Коментарі</h4>

                            <div class="row">
                                <div class="col-md-6" id="comments-list">
                                    @foreach($data->comments as $item)
                                        <div class="media">
                                            <div class="media-body">
                                                <span>{{$item->text}}</span>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="hidden" id="lead_id" value="{{$data->id}}"
                                                   data-link="{{URL::route('backend.naturel.leads.comment')}}">
                                            <textarea class="form-control" rows="4" id="comment-text"></textarea>
                                        </div>
                                    </div>
                                    <button class="btn blue pull-right margin-bottom-20" id="comment-add">Залишити
                                        коментар
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn green">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('modules/naturel/backend/js/leads-edit.js')}}"></script>
@endsection