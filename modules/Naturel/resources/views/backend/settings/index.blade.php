@extends('backend::layouts.master')
@section('content')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet box blue-hoki base-portlet">
                <div class="portlet-title">
                    <div class="caption">Налаштування</div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr class="heading">
                                <th>{{trans('backend::common.caption')}}</th>
                                <th>{{trans('backend::common.status')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($languages as $item)
                                <tr>
                                    <td>{{$item->caption}}</td>
                                    <td>
                                        <a href="{{URL::route('backend.naturel.settings.status',['id' => $item->id])}}"
                                           rel="item-status" class="btn btn-sm {{$item->active ? 'green' : 'red'}}">
                                            <i class="fa {{$item->active ? 'fa-check' : 'fa-ban'}}"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(function ($) {
            'use strict';

            var _Body = $('body');

            _Body.on('click', '[rel="item-status"]', function (e) {
                e.preventDefault();
                var _Link = $(this),
                        _Icon = $('i', this);

                UpdateStatus({
                    url: _Link.attr('href'),
                    success: function (data) {
                        var btnColor = (data == 1) ? 'green' : 'red';
                        var btnIcon = (data == 1) ? 'fa-check' : 'fa-ban';

                        _Link.removeClass('green red');
                        _Icon.removeClass('fa-check fa-ban');

                        _Link.addClass(btnColor);
                        _Icon.addClass(btnIcon);
                    }
                });
            });
        });
    </script>
@endsection