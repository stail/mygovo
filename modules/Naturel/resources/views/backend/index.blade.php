<?php
$_status = [
        1 => 'Новий',
        2 => 'В роботі',
        3 => 'В очікуванні',
        4 => 'Виконано',
        6 => 'Відправлено',
        5 => 'Відмова',
];

$new = \Modules\Naturel\Models\Lead::query()
        ->where('status', '=', '1')
        ->limit(5)
        ->orderBy('created_at', 'desc')
        ->get();

$progress = \Modules\Naturel\Models\Lead::query()
        ->where('status', '=', '2')
        ->limit(5)
        ->orderBy('created_at', 'desc')
        ->get();

$done = \Modules\Naturel\Models\Lead::query()
        ->where('status', '=', '4')
        ->limit(5)
        ->orderBy('created_at', 'desc')
        ->get();
?>
<h3 class="page-title">Останні замовлення <small></small></h3>
<div class="tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#new" data-toggle="tab">
                Нові</a></li>
        <li>
            <a href="#progress" data-toggle="tab">
                В роботі</a></li>
        <li>
            <a href="#done" data-toggle="tab">
                Виконані</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="new">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr class="heading">
                        <th width="40px">ID</th>
                        <th width="130px">Дата</th>
                        <th>Ім'я</th>
                        <th width="130px">Тип</th>
                        <th width="150px">Статус</th>
                        <th width="125px">Дії</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($new as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->created_at->format('d.m.Y')}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->form}}</td>
                            <td>
                                <select class="status-change form-control input-sm" data-link="{{route('backend.naturel.leads.status', ['id' => $item->id])}}">
                                    @foreach($_status as $key => $val)
                                        <option value="{{$key }}" {{($key == $item->status) ? 'selected=""' : '' }}>{{$val}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <a class="btn default btn-sm"
                                   href="{{URL::route('backend.naturel.leads.view', ['id' => $item->id])}}"
                                   rel="order-view">
                                    <i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane" id="progress">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr class="heading">
                        <th width="40px">ID</th>
                        <th width="130px">Дата</th>
                        <th>Ім'я</th>
                        <th width="130px">Тип</th>
                        <th width="150px">Статус</th>
                        <th width="125px">Дії</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($progress as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->created_at->format('d.m.Y')}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->form}}</td>
                            <td>
                                <select class="status-change form-control input-sm" data-link="{{route('backend.naturel.leads.status', ['id' => $item->id])}}">
                                    @foreach($_status as $key => $val)
                                        <option value="{{$key }}" {{($key == $item->status) ? 'selected=""' : '' }}>{{$val}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <a class="btn default btn-sm"
                                   href="{{URL::route('backend.naturel.leads.view', ['id' => $item->id])}}"
                                   rel="order-view">
                                    <i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane" id="done">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr class="heading">
                        <th width="40px">ID</th>
                        <th width="130px">Дата</th>
                        <th>Ім'я</th>
                        <th width="130px">Тип</th>
                        <th width="150px">Статус</th>
                        <th width="125px">Дії</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($done as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->created_at->format('d.m.Y')}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->form}}</td>
                            <td>
                                <select class="status-change form-control input-sm" data-link="{{route('backend.naturel.leads.status', ['id' => $item->id])}}">
                                    @foreach($_status as $key => $val)
                                        <option value="{{$key}}" {{($key == $item->status) ? 'selected=""' : '' }}>{{$val}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <a class="btn default btn-sm"
                                   href="{{URL::route('backend.naturel.leads.view', ['id' => $item->id])}}"
                                   rel="order-view">
                                    <i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

@section('scripts')
    <script type="text/javascript">
        jQuery(function ($) {
            'use strict';

            var _Body = $('body');

            _Body.on('change', '.status-change', function () {
                var _Link = $(this).attr('data-link'),
                        _Status = $(this).val();

                $.ajax({
                    url: _Link,
                    method: 'POST',
                    data: {
                        _token: _TOKEN,
                        status: _Status
                    },
                    success: function (data) {}
                });
            });

            _Body.on('click', '[rel="order-view"]', function (e) {
                e.preventDefault();

                ShowAjaxModal({
                    url: $(this).attr('href')
                });
            });
        });
    </script>
@endsection