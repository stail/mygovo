@extends('naturel::layouts.base')
@section('meta-tags')
    <title>{{$data->text->meta_title}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('naturel::templates.top-nav')
    @include('naturel::templates.top-header')

    <div class="row news-info-row">
        <div class="content">
            <h2>{{$data->text->caption}}</h2>
            <div class="breadcrumbs">
                <a href="{{URL::route('naturel.index')}}">{{Lang::get('naturel::common.main')}}</a><div class="breadcrumbs-circle"></div>
                <a href="javascript:void(0);">{{$data->text->caption}}</a>
            </div>
        </div>
    </div>

    <div class="row contacts-row">
        <div id="map" class="row row-map-google"></div>
        <div class="row row-map">
            <div class="contacts">
                <div class="info">
                    <h2>{{$data->text->caption}}</h2>

                    {!! $data->text->text !!}
                </div>
                <svg class="fill" width="700" height="718">
                    <polygon points="0,0 700,0 700,718 0,718 0,90 20,70 0,50"/>
                </svg>
                <svg class="border" width="26" height="718">
                    <polygon points="0,0 6,0 6,50 26,70 6,90 6,718 0,718 0,88 18,70 0,52"/>
                </svg>
            </div>
        </div>
    </div>

    @include('naturel::templates.footer')
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/map.js')}}"></script>
@endsection