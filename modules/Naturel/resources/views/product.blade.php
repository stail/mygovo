@extends('naturel::layouts.base')
@section('meta-tags')
    <title>{{$data->text->caption}}</title>

    <meta name="description" content="{{$data->text->meta_description}}">
    <meta name="keywords" content="{{$data->text->meta_keywords}}">
@endsection
@section('main')
    @include('naturel::templates.top-nav')
    @include('naturel::templates.top-header')

    <div class="row news-info-row">
        <div class="content">
            <h2>{{$data->text->caption}}</h2>
            <div class="breadcrumbs">
                <a href="{{URL::route('naturel.index')}}">{{Lang::get('naturel::common.main')}}</a><div class="breadcrumbs-circle"></div>
                <a href="{{URL::to($data->category->link)}}">{{$data->category->text->caption}}</a><div class="breadcrumbs-circle"></div>
                <a href="javascript:void(0);">{{$data->text->caption}}</a>
            </div>
        </div>
    </div>

    <div class="row detail-buy-row">
        <div class="content">
            <div class="item-info">
                <div class="item-images">
                    <div class="swiper-container gallery-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide" style="background-image: url('{{$data->media ? $data->media->path : ''}}')"></div>
                            @foreach($data->gallery as $item)
                                <div class="swiper-slide" style="background-image: url('{{$item->media ? $item->media->path : ''}}')"></div>
                            @endforeach
                        </div>
                    </div>
                    <div class="swiper-container gallery-thumbs">
                        <div class="swiper-wrapper">
                            @foreach($data->gallery as $item)
                                <div class="swiper-slide" style="background-image: url('{{$data->media ? $data->media->path : ''}}')"></div>
                                <div class="swiper-slide" style="background-image: url('{{$item->media ? $item->media->path : ''}}')"></div>
                            @endforeach
                        </div>
                        <div class="swiper-button-next swiper-button-white mini-button-next"></div>
                        <div class="swiper-button-prev swiper-button-white mini-button-prev"></div>
                    </div>
                </div>
                <div class="item-discription">
                    <h2>{{$data->text->caption}}</h2>
                    <div class="item-buy-info">
                        <div class="price">
                            <p class="num">{{$data->price}}</p>
                            <p class="val">грн.</p>
                        </div>
                        <div class="articul">Артикл: {{$data->article}}</div>
                        <div class="in-stock">В наявності</div>
                    </div>
                    <div class="btn green-trans-btn item-buy-btn" onclick="showOrder();">Замовити</div>
                    <p class="anotation">{{$data->text->description}}</p>
                    <div class="item-values">
                        {!! $data->text->description_detail !!}
                    </div>
                </div>
            </div>
            <div class="line"></div>
        </div>
    </div>
    <div class="row discription-row">
        <div class="content">{!! $data->text->text !!}</div>
    </div>

    <div class="row same-items-row">
        <div class="content">
            <h2 class="same-items-title">Схожі товари</h2>
            <div class="swiper-container same-swiper">
                <div class="swiper-wrapper">
                    @foreach($products as $item)
                        <div class="swiper-slide">
                            <a href="{{URL::to($item->link)}}">
                                <div class="item">
                                    <div class="item-img" style="background-image: url('{{$item->media ? $item->media->path : ''}}');"></div>
                                    <div class="makeup-price">
                                        <p class="num">{{$item->price}}</p>
                                        <p class="val">грн.</p>
                                    </div>
                                    <p class="makeup-title">{{$item->text->caption}}</p>
                                    <p class="makeup-discription">{{$item->text->description}}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination same-pagination"></div>
                <div class="swiper-button-next same-button-next"></div>
                <div class="swiper-button-prev same-button-prev"></div>
            </div>
        </div>
    </div>

    @include('naturel::templates.popup-order')
    <div id="wrap"></div>
    @include('naturel::templates.footer')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{URL::asset('js/swiper.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popup.js')}}"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            var galleryTop = new Swiper('.gallery-top', {
                nextButton: '.mini-button-next',
                prevButton: '.mini-button-prev',
                spaceBetween: 10
            });
            var galleryThumbs = new Swiper('.gallery-thumbs', {
                spaceBetween: 10,
                centeredSlides: true,
                slidesPerView: 'auto',
                touchRatio: 0.2,
                slideToClickedSlide: true
            });
            galleryTop.params.control = galleryThumbs;
            galleryThumbs.params.control = galleryTop;

            var swiper = new Swiper('.same-swiper', {
                pagination: '.same-pagination',
                nextButton: '.same-button-next',
                prevButton: '.same-button-prev',
                paginationClickable: true,
                slidesPerView: 3,
                spaceBetween: 20,
                breakpoints: {
                    850: {
                        slidesPerView: 2,
                        spaceBetween: 15
                    },
                    600: {
                        slidesPerView: 1,
                        spaceBetween: 10
                    }
                }
            });
        });
    </script>
@endsection