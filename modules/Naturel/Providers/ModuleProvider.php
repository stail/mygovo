<?php namespace Modules\Naturel\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerConfig();
        $this->registerTranslations();
        $this->registerViews();

        \BackendMenu::addMenu('leads', 'Ліди', route('backend.naturel.leads.index'), 'lnr lnr-cart');

        \BackendMenu::addMenu('settings', 'Налаштування', route('backend.naturel.settings.index'), 'icon-settings');

        \BackendMenu::addMenu('catalog', 'Каталог');
        \BackendMenu::addItem('catalog', 'categories', 'Категорії', route('backend.naturel.catalog.index'));
        \BackendMenu::addItem('catalog', 'products', 'Товари', route('backend.naturel.products.index'));

        \BackendMenu::addMenu('services', 'Послуги');
        \BackendMenu::addItem('services', 'categories', 'Категорії', route('backend.naturel.categories.index'));
        \BackendMenu::addItem('services', 'items', 'Послуги', route('backend.naturel.services.index'));
        \BackendMenu::addItem('services', 'manager', 'Керівник', route('backend.naturel.manager.index'));

        \BackendMenu::addMenu('achievements', 'Досягнення', route('backend.naturel.achievements.index'));

        \BackendMenu::addMenu('certificates', 'Сертифікати');
        \BackendMenu::addItem('certificates', 'card', 'Дисконтна картка', route('backend.naturel.card.index'));
        \BackendMenu::addItem('certificates', 'items', 'Сертифікати', route('backend.naturel.certificates.index'));

        \BackendMenu::addMenu('reviews', 'Відгуки', route('backend.naturel.reviews.index'));
        \BackendMenu::addMenu('banners', 'Банери', route('backend.naturel.banners.index'));

        \BackendMenu::addMenu('gallery', 'Галерея', route('backend.naturel.gallery.index'));

        \BackendMenu::addMenu('slider', 'Слайдер', route('backend.naturel.slider.index'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes(
            [
                __DIR__.'/../config/config.php' => config_path('modules/naturel.php'),
            ]
        );
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php',
            'modules.naturel'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/naturel');

        $sourcePath = __DIR__.'/../resources/views';

        $this->publishes(
            [
                $sourcePath => $viewPath
            ]
        );

        $this->loadViewsFrom([$viewPath, $sourcePath], 'naturel');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/naturel');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'naturel');
        } else {
            $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'naturel');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
    }

}
