<?php namespace Modules\Naturel\Models\Certificate;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $table = 'certificates_texts';
    protected $fillable = ['certificate_id', 'lang', 'caption', 'description', 'text'];
    public $timestamps = false;
}