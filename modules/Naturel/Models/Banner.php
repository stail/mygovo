<?php namespace Modules\Naturel\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media as MediaModel;

class Banner extends Model
{
    protected $table = 'banners';
    protected $fillable = ['media_id', 'link'];

    public $timestamps = false;

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}