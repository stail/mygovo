<?php namespace Modules\Naturel\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media as MediaModel;

class GalleryImages extends Model
{
    protected $table = 'gallery_images';
    protected $fillable = ['gallery_id', 'media_id'];

    public $timestamps = false;

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}