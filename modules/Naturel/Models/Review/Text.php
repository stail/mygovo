<?php namespace Modules\Naturel\Models\Review;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $table = 'reviews_texts';
    protected $fillable = ['review_id', 'lang', 'caption', 'description', 'text'];
    public $timestamps = false;
}