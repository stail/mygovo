<?php namespace Modules\Naturel\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media as MediaModel;
use Modules\Naturel\Models\Certificate\Text as TextModel;

class Certificate extends Model
{
    protected $table = 'certificates';
    protected $fillable = ['type', 'media_id'];

    public $timestamps = false;

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(TextModel::class, 'id', 'certificate_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(TextModel::class, 'certificate_id', 'id');
    }

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}