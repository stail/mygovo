<?php namespace Modules\Naturel\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media as MediaModel;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['category_id', 'media_id', 'slug', 'active', 'price', 'article'];
    protected $appends = ['link'];

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(ProductText::class, 'id', 'product_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(ProductText::class, 'product_id', 'id');
    }

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function gallery()
    {
        return $this->hasMany(ProductGallery::class, 'product_id', 'id');
    }

    public function getLinkAttribute()
    {
        $slug = $this->slug ? '-'.$this->slug : '';

        return $this->category->link.'/'.$this->id.$slug;
    }
}