<?php namespace Modules\Naturel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Media\Models\Media as MediaModel;
use Modules\Naturel\Models\Review\Text as TextModel;

class Review extends Model
{
    use SoftDeletes;

    protected $table = 'reviews';
    protected $fillable = ['main', 'active', 'media_id'];

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(TextModel::class, 'id', 'review_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(TextModel::class, 'review_id', 'id');
    }

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}