<?php namespace Modules\Naturel\Models\Service;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Models\Media as MediaModel;

class Gallery extends Model
{
    protected $table = 'services_gallery';
    protected $fillable = ['service_id', 'media_id'];

    public $timestamps = false;

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }
}