<?php namespace Modules\Naturel\Models\Service;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Media\Models\Media as MediaModel;
use Modules\Naturel\Models\Service as ServiceModel;
use Modules\Naturel\Models\Service\Text as TextModel;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'services_categories';
    protected $fillable = ['slug', 'weight', 'active', 'media_id'];
    protected $appends = ['link'];

    public function text()
    {
        $lang = \Lang::getLocale();
        $lang = (in_array($lang, config('app.locales'))) ? $lang : 'uk';

        return $this->belongsTo(TextModel::class, 'id', 'category_id')
            ->where('lang', '=', $lang);
    }

    public function texts()
    {
        return $this->hasMany(TextModel::class, 'category_id', 'id');
    }

    public function media()
    {
        return $this->hasOne(MediaModel::class, 'id', 'media_id');
    }

    public function services()
    {
        return $this->hasMany(ServiceModel::class, 'parent_id', 'id');
    }

    public function getLinkAttribute()
    {
        return 'services/'.$this->id.'-'.$this->slug;
    }
}