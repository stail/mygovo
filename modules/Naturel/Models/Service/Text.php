<?php namespace Modules\Naturel\Models\Service;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $table = 'services_texts';
    protected $fillable = [
        'category_id',
        'service_id',
        'lang',
        'caption',
        'description',
        'text',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public $timestamps = false;
}