<?php namespace Modules\Naturel\Models;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $table = 'achievements';
    protected $fillable = ['title_uk', 'title_ru', 'title_en', 'number'];
}