<?php namespace Modules\Naturel\Models;

use Illuminate\Database\Eloquent\Model;

class LeadComment extends Model
{
    protected $table = 'leads_comments';
    protected $fillable = ['lead_id', 'text'];

    public $timestamps = false;
}