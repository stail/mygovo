jQuery(function ($) {
    'use strict';

    var _Table = new Datatable(),
        _Body = $('body');

    _Table.setAjaxParam('_token', _TOKEN);
    _Table.init({
        src: $('.data-table'),
        dataTable: {
            ordering: false
        }
    });

    _Body.on('click', '[rel="item-status"]', function (e) {
        e.preventDefault();
        var _Link = $(this),
            _Icon = $('i', this);

        UpdateStatus({
            url: _Link.attr('href'),
            success: function (data) {
                var btnColor = (data == 1) ? 'green' : 'red';
                var btnIcon = (data == 1) ? 'fa-check' : 'fa-ban';

                _Link.removeClass('green red');
                _Icon.removeClass('fa-check fa-ban');

                _Link.addClass(btnColor);
                _Icon.addClass(btnIcon);
            }
        });
    });
});