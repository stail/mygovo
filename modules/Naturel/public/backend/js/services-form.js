jQuery(function ($) {
    'use strict';

    var _Body = $('body');

    CKEDITOR.disableAutoInline = true;
    $('.editor').ckeditor({
        filebrowserBrowseUrl: '/elfinder/ckeditor'
    });

    _Body.on('click', '[rel="add-price"]', function () {
        var _Index = $('tr', '#prices-list').length;
        $('#prices-list').append('<tr><td><input type="text" class="form-control margin-bottom-5" name="prices[' + _Index + '][title_uk]" placeholder="Українська"><input type="text" class="form-control margin-bottom-5" name="prices[' + _Index + '][title_ru]" placeholder="Русский"><input type="text" class="form-control" name="prices[' + _Index + '][title_en]" placeholder="English"></td><td><input type="text" class="form-control" name="prices[' + _Index + '][price]"></td><td><a href="javascript:" rel="price-destroy" class="btn btn-sm red" title="Видалити"><i class="fa fa-trash-o"></i></a></td></tr>');
    });

    _Body.on('click', '[rel="price-destroy"]', function () {
        $(this).parents('tr').remove();
    });

    var filePicker = $('#file-picker');
    var _Input = filePicker.data('input');

    filePicker.on('click', '[rel="popup"]', function (e) {
        e.preventDefault();
        var elFinder = '/elfinder/popup/';
        var triggerUrl = elFinder + _Input;

        $.colorbox({
            href: triggerUrl,
            fastIframe: true,
            iframe: true,
            width: '70%',
            height: '580'
        });
    });

    filePicker.on('change', _Input, function () {
        if (!$(this).val()) {
            $('[rel="preview"]', filePicker).parent('.input-group-btn').hide();
            $('[rel="delete"]', filePicker).parent('.input-group-btn').hide();
        } else {
            $('[rel="preview"]', filePicker).parent('.input-group-btn').show();
            $('[rel="delete"]', filePicker).parent('.input-group-btn').show();
        }
    });
    $(_Input).change();

    filePicker.on('click', '[rel="delete"]', function () {
        if (confirm('Видалити?')) {
            $(_Input, filePicker).val('').change();
            $('.file-name', filePicker).val('');
        }
    });

    filePicker.on('click', '[rel="preview"]', function (e) {
        e.preventDefault();
        var _href = $(this).attr('href');

        $.colorbox({
            href: _href,
            photo: true,
            width: '70%',
            height: '580'
        });
    });

    function processSelectedFile(filePath, requestingField, file) {
        if (requestingField == 'gallery-items') {
            var _Index = $('.gallery-item', '#gallery-list').length;
            $('#gallery-list').append('<div class="inline-block gallery-item" style="position: relative; margin: 5px;"><a class="btn btn-sm red" href="javascript:" rel="destroy-gallery-item" style="position: absolute; right: 0; top: 0;"><i class="fa fa-trash-o"></i></a><input type="hidden" name="gallery[' + _Index + '][media_id]" value="' + file.media.id + '"><img style="max-height: 140px;" src="' + file.path + '" alt="" class="img-thumbnail"></div>');
        } else {
            $('.file-name', filePicker).val(file.name ? file.name : ' ');
            $('[rel="preview"]', filePicker).attr('href', filePath);
            $(requestingField, filePicker).val(filePath).change();
        }
    }

    window.processSelectedFile = processSelectedFile;

    _Body.on('click', '.popup-selector', function (e) {
        e.preventDefault();
        var _ID = $(this).attr('data-input');
        var elfinder = '/elfinder/popup/';

        var triggerUrl = elfinder + _ID;
        $.colorbox({
            href: triggerUrl,
            fastIframe: true,
            iframe: true,
            width: '70%',
            height: '580'
        });
    });

    _Body.on('click', '[rel="destroy-gallery-item"]', function () {
        $(this).parents('.gallery-item').remove();
    });
});