jQuery(function ($) {
    'use strict';

    var _Body = $('body');

    _Body.on('click', '#comment-add', function (e) {
        e.preventDefault();
        var textArea = $('#comment-text');
        var _Text = $.trim(textArea.val());
        var _LeadInput = $('#lead_id'),
            _Lead = _LeadInput.val(),
            _Link = _LeadInput.data('link');

        if (_Text != '') {
            $.ajax({
                url: _Link,
                method: 'POST',
                data: {
                    _token: _TOKEN,
                    lead_id: _Lead,
                    text: _Text
                },
                success: function (data) {
                    $('#comments-list').append('<div class="media"><div class="media-body"><span>' + _Text + '</span></div></div><hr>');
                    textArea.val('');
                }
            });
        }
    });
});