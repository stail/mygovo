<?php

$lang = null;
$locale = Request::segment(1);
if (in_array($locale, Config::get('app.locales'))) {
    \Lang::setLocale($locale);
    \Carbon\Carbon::setLocale($locale);
    $lang = $locale;
}

Route::group(
    ['prefix' => $lang, 'namespace' => 'Modules\\Naturel\\Http\\Controllers'],
    function (\Illuminate\Routing\Router $router) {

        $router->group(
            [
                'prefix' => 'services',
                'where'  => [
                    'id'           => '\d+',
                    'service'      => '\d+',
                    'slug'         => '[a-z0-9-_]+',
                    'service_slug' => '[a-z0-9-_]+',
                    'category'     => '[a-z0-9-_]+'
                ]
            ],
            function (\Illuminate\Routing\Router $router) {
                $router->get('{category}/{service}{service_slug?}', 'NaturelController@service');
                $router->get('{id}{slug?}', 'NaturelController@services');
                $router->get('/', 'NaturelController@servicesMain');
            }
        );


        $router->group(
            [
                'prefix' => 'news',
                'where'  => [
                    'id'   => '\d+',
                    'slug' => '[a-z0-9-_]+',
                ]
            ],
            function (\Illuminate\Routing\Router $router) {
                $router->get('{id}{slug?}', 'NaturelController@newsItem');
                $router->get('', 'NaturelController@news');
            }
        );

        $router->group(
            [
                'prefix' => 'catalog',
                'where'  => [
                    'id'           => '\d+',
                    'slug'         => '[a-z0-9-_]+',
                    'product_id'   => '\d+',
                    'product_slug' => '[a-z0-9-_]+'
                ]
            ],
            function (\Illuminate\Routing\Router $router) {
                $router->get('{id}{slug?}/{product_id}{product_slug?}', 'NaturelController@catalogProduct');
                $router->get('{id}{slug?}', 'NaturelController@catalogCategory');
                $router->get('', 'NaturelController@catalog');
            }
        );

        $router->get('certificates', ['as' => 'naturel.certificates', 'uses' => 'NaturelController@certificates']);
        $router->get('contacts', ['as' => 'naturel.contacts', 'uses' => 'NaturelController@contacts']);
        $router->post('submit', ['as' => 'naturel.submit', 'uses' => 'NaturelController@submitForm']);
        $router->get('/', ['as' => 'naturel.index', 'uses' => 'NaturelController@index']);
    }
);