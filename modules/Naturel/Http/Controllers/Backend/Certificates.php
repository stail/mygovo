<?php namespace Modules\Naturel\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Naturel\Models\Certificate;

class Certificates extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module('ckeditor');
        Metronic::menu('certificates', 'items');
    }

    public function getIndex()
    {
        $data = Certificate::query()
            ->where('type', '=', 'certificate')
            ->get();

        return \View::make('naturel::backend.certificates.index', ['data' => $data]);
    }

    public function getEdit($id)
    {
        $data = Certificate::query()
            ->where('type', '=', 'certificate')
            ->findOrFail($id);

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('naturel::backend.certificates.edit', ['data' => $data, 'text' => $text]);
    }

    public function postEdit($id, Request $request)
    {
        return \DB::transaction(
            function () use ($id, $request) {
                $model = Certificate::query()
                    ->where('type', '=', 'certificate')
                    ->findOrFail($id);

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = Certificate\Text::firstOrNew(['certificate_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                return \Redirect::route('backend.naturel.certificates.edit', ['id' => $id])
                    ->with('message', 'success');
            }
        );
    }
}