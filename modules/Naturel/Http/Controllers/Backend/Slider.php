<?php namespace Modules\Naturel\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\Naturel\Models\Slide;
use Modules\Naturel\Models\SlideText;

class Slider extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['colorbox']);
        Metronic::menu('slider');
    }

    public function getIndex()
    {
        $data = Slide::query()
            ->orderBy('weight', 'asc')
            ->get();

        return \View::make('naturel::backend.slider.index', ['data' => $data]);
    }

    public function getEdit($id)
    {
        $data = Slide::query()
            ->findOrFail($id);

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('naturel::backend.slider.edit', ['data' => $data, 'text' => $text]);
    }

    public function postEdit($id, Request $request)
    {
        $model = Slide::query()
            ->findOrFail($id);

        $data = $request->input('content', []);

        $image = MediaModel::search($request->input('media_path'));
        $data['media_id'] = ($image) ? $image->id : null;

        $model->fill($data);
        $model->save();

        foreach ($request->input('text', []) as $lang => $data) {
            $text = SlideText::firstOrNew(['slide_id' => $model->id, 'lang' => $lang]);
            $text->fill($data);
            $text->save();
        }

        return \Redirect::route('backend.naturel.slider.edit', ['id' => $model->id])
            ->with('message', 'success');
    }

    public function anyStatus($id)
    {
        $model = Slide::query()
            ->findOrFail($id);

        $model->active = ($model->active == 1) ? 0 : 1;
        $model->save();

        return $model->active;
    }

    public function anyWeight(Request $request)
    {
        $list = $request->input('list', []);

        foreach ($list as $weight => $id) {
            Slide::query()
                ->where('id', '=', $id)
                ->update(['weight' => $weight]);
        }
    }
}