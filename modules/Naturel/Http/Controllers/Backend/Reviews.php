<?php namespace Modules\Naturel\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\Naturel\Models\Review;

class Reviews extends Controller
{
    public function __construct()
    {
        Metronic::module(['colorbox']);

        Metronic::menu('reviews');
    }

    public function getIndex()
    {
        $data = Review::query()
            ->where('main', '=', null)
            ->get();

        return \View::make('naturel::backend.reviews.index', ['data' => $data]);
    }

    public function getCreate()
    {
        return \View::make('naturel::backend.reviews.form');
    }

    public function postCreate(Request $request)
    {
        $model = new Review();

        return $this->save($model, $request);
    }

    public function getEdit($id)
    {
        $data = Review::query()
            ->where('main', '=', null)
            ->findOrFail($id);

        return \View::make('naturel::backend.reviews.form', ['data' => $data]);
    }

    public function postEdit($id, Request $request)
    {
        /**
         * @var Review $model
         */
        $model = Review::query()
            ->where('main', '=', null)
            ->findOrFail($id);

        return $this->save($model, $request);
    }

    public function anyDestroy($id)
    {
        Review::query()
            ->where('main', '=', null)
            ->findOrFail($id)
            ->delete();

        return \Redirect::route('backend.naturel.reviews.index');
    }

    private function save(Review $model, Request $request)
    {
        return \DB::transaction(
            function () use ($model, $request) {

                $image = MediaModel::search($request->input('media_path'));
                $data['media_id'] = ($image) ? $image->id : null;

                $model->fill($data);
                $model->save();

                foreach (['uk', 'ru', 'en'] as $lang) {
                    $text = Review\Text::firstOrNew(['review_id' => $model->id, 'lang' => $lang]);
                    $text->fill($request->input('text_uk', []));
                    $text->save();
                }

                return \Redirect::route('backend.naturel.reviews.edit', ['id' => $model->id])
                    ->with('message', 'success');
            }
        );

    }
}