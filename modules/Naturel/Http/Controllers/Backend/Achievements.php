<?php namespace Modules\Naturel\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Naturel\Models\Achievement as AchievementModel;

class Achievements extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['datatable']);
        Metronic::menu('achievements');
    }

    public function getIndex()
    {
        $data = AchievementModel::query()
            ->get();

        return \View::make('naturel::backend.achievements.index', ['data' => $data]);
    }

    public function postIndex(Request $request)
    {
        foreach ($request->input('content', []) as $id => $data) {
            $model = AchievementModel::query()
                ->findOrFail($id);

            $model->fill($data);
            $model->save();
        }

        return \Redirect::route('backend.naturel.achievements.index')
            ->with('message', 'success');
    }
}