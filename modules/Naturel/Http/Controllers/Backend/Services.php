<?php namespace Modules\Naturel\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\Naturel\Models\Service as ServiceModel;

class Services extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['datatable', 'ckeditor', 'colorbox']);
        Metronic::menu('services', 'items');
    }

    public function getIndex()
    {
        $data = ServiceModel::query()
            ->whereNull('service_id')
            ->with(['text'])
            ->orderBy('weight', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        return \View::make('naturel::backend.services.index', ['data' => $data]);
    }

    public function getCreate()
    {
        $services = ServiceModel::query()
            ->whereNull('service_id')
            ->orderBy('weight', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        $categories = ServiceModel\Category::query()
            ->where('active', '=', 1)
            ->get();

        return \View::make('naturel::backend.services.edit', ['categories' => $categories, 'services' => $services]);
    }

    public function postCreate(Request $request)
    {
        $model = new ServiceModel();

        return $this->save($model, $request);
    }

    public function getEdit($id)
    {
        $services = ServiceModel::query()
            ->whereNull('service_id')
            ->orderBy('weight', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        $categories = ServiceModel\Category::query()
            ->where('active', '=', 1)
            ->get();

        $data = ServiceModel::query()
            ->with(['text', 'texts', 'media', 'prices', 'gallery'])
            ->findOrFail($id);

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('naturel::backend.services.edit', ['data' => $data, 'categories' => $categories, 'text' => $text, 'services' => $services]);
    }

    public function postEdit($id, Request $request)
    {
        /**
         * @var ServiceModel $model
         */
        $model = ServiceModel::query()
            ->findOrFail($id);

        return $this->save($model, $request);
    }

    public function anyDestroy($id)
    {
        ServiceModel::query()
            ->findOrFail($id)
            ->delete();

        return \Redirect::route('backend.naturel.services.index');
    }

    public function anyStatus($id)
    {
        $model = ServiceModel::query()
            ->findOrFail($id);

        $model->active = ($model->active == 1) ? 0 : 1;
        $model->save();

        return $model->active;
    }

    public function anyWeight(Request $request)
    {
        $list = $request->input('list', []);

        foreach ($list as $weight => $id) {
            ServiceModel::query()
                ->where('id', '=', $id)
                ->update(['weight' => $weight]);
        }
    }

    private function save(ServiceModel $model, Request $request)
    {
        return \DB::transaction(
            function () use ($model, $request) {
                $data = $request->input('content', []);

                $image = MediaModel::search($request->input('media_path'));
                $data['media_id'] = ($image) ? $image->id : null;
                $slug = empty($data['slug']) ? Str::slug($request->input('text.uk.caption')) : Str::slug($data['slug']);
                $data['slug'] = strtolower($slug);

                $model->fill($data);
                $model->save();

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = ServiceModel\Text::firstOrNew(['service_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                $model->prices()->delete();
                $model->prices()->createMany($request->input('prices', []));

                $model->gallery()->delete();
                $model->gallery()->createMany($request->input('gallery', []));

                return \Redirect::route('backend.naturel.services.edit', ['id' => $model->id])
                    ->with('message', 'success');
            }
        );
    }
}