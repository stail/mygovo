<?php namespace Modules\Naturel\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Support\Metronic;
use Modules\Naturel\Models\Banner;

class Banners extends Controller
{
    public function __construct()
    {
        Metronic::module('colorbox');

        Metronic::menu('banners');
    }

    public function getIndex()
    {
        $data = Banner::query()
            ->orderBy('id', 'asc')
            ->get();

        return \View::make('naturel::backend.banners.index', ['banners' => $data]);
    }

    public function postIndex(Request $request)
    {
        foreach ($request->input('data', []) as $id => $data) {
            $model = Banner::query()
                ->findOrFail($id);

            $model->fill($data);
            $model->save();
        }

        return \Redirect::route('backend.naturel.banners.index');
    }
}