<?php namespace Modules\Naturel\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Modules\Backend\Http\Controllers\BaseController;
use Modules\Backend\Support\Metronic;
use Modules\Media\Models\Media as MediaModel;
use Modules\Naturel\Models\Service\Category as ServiceCategoryModel;
use Modules\Naturel\Models\Service\Text as ServiceTextModel;

class Categories extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        Metronic::module(['datatable', 'colorbox']);
        Metronic::menu('services', 'categories');
    }

    public function getIndex()
    {
        $data = ServiceCategoryModel::query()
            ->with(['text'])
            ->orderBy('weight', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();

        return \View::make('naturel::backend.services.categories.index', ['data' => $data]);
    }

    public function getCreate()
    {
        return \View::make('naturel::backend.services.categories.edit');
    }

    public function postCreate(Request $request)
    {
        $model = new ServiceCategoryModel();

        return $this->save($model, $request);
    }

    public function getEdit($id)
    {
        $data = ServiceCategoryModel::query()
            ->findOrFail($id);

        $text = $data->texts->keyBy('lang')->toArray();

        return \View::make('naturel::backend.services.categories.edit', ['data' => $data, 'text' => $text]);
    }

    public function postEdit($id, Request $request)
    {
        /**
         * @var ServiceCategoryModel $model
         */
        $model = ServiceCategoryModel::query()
            ->findOrFail($id);

        return $this->save($model, $request);
    }

    public function anyDestroy($id)
    {
        ServiceCategoryModel::query()
            ->findOrFail($id)
            ->delete();

        return \Redirect::route('backend.naturel.categories.index');
    }

    public function anyStatus($id)
    {
        $model = ServiceCategoryModel::query()
            ->findOrFail($id);

        $model->active = ($model->active == 1) ? 0 : 1;
        $model->save();

        return $model->active;
    }

    public function anyWeight(Request $request)
    {
        $list = $request->input('list', []);

        foreach ($list as $weight => $id) {
            ServiceCategoryModel::query()
                ->where('id', '=', $id)
                ->update(['weight' => $weight]);
        }
    }

    private function save(ServiceCategoryModel $model, Request $request)
    {
        return \DB::transaction(
            function () use ($model, $request) {
                $data = $request->input('content', []);

                $image = MediaModel::search($request->input('media_path'));
                $data['media_id'] = ($image) ? $image->id : null;
                $slug = empty($data['slug']) ? Str::slug($request->input('text.uk.caption')) : Str::slug($data['slug']);
                $data['slug'] = strtolower($slug);

                $model->fill($data);
                $model->save();

                foreach ($request->input('text', []) as $lang => $data) {
                    $text = ServiceTextModel::firstOrNew(['category_id' => $model->id, 'lang' => $lang]);
                    $text->fill($data);
                    $text->save();
                }

                return \Redirect::route('backend.naturel.categories.edit', ['id' => $model->id])
                    ->with('message', 'success');
            }
        );
    }
}